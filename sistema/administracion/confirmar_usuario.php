<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');



if(isset($_POST['enviar']) and $_POST['enviar']=='Agregar'){
	$_POST['uname'] = trim($_POST['uname']); //eliminamos los espacios en blanco
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_usuario = new usuario($con);	
	$errores=verificar_errores($t_usuario);
	
	$t_bombero = new bombero($con);
	if($t_bombero->existe_bombero($_POST['rut'])==false)
		$errores[]='Rut ingresado no se encuentra registrado';

	if($errores==array()){
		
		$t_tipo_usuario = new tipo_usuario($con);
		$form['uname'] = $_POST['uname'];
		$form['descripcion_tipo_usuario'] = $t_tipo_usuario->obtener_descripcion($_POST['tipo_usuario']);
		$datos_basic_b = $t_bombero->obtener_datos_basicos($_POST['rut']);
		$form['rut'] = $_POST['rut'];
		$form['rut_dv'] = $datos_basic_b['rut_dv'];
		
		$form['nombre']= $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];

		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_usuario.php';
	}
	else{	
		$enlace_volver='agregar_usuario.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else {
	header('location: usuarios.php');
}

function verificar_errores($t_usuario) {
	$errores=array();
	
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='<strong>Solo un usuario administrador puede agregar usuarios</strong>';
		
	if(trim($_POST['uname'])=='')
		$errores[]='Campo nombre de usuario es requerido';
	else if(comprobar_nombre_usuario($_POST['uname'])==false)
		$errores[]='Campo nombre de usuario tiene caracteres no permitidos. Usar solo letras, numeros o "-" "_"';
		
	if(trim($_POST['upass'])=='') {
		$errores[]='Campo password es requerido';	
	}		
	if(trim($_POST['tipo_usuario'])=='') {
		$errores[]='Campo tipo usuario es requerido';	
	}		
	if(trim($_POST['rut'])==''){
		$errores[]='Campo Rut está vacío';
	}
	else{
		$dv=obtener_digito_verificador($_POST['rut']);
		$_POST['rut_dv']=strtoupper($_POST['rut_dv']);
		if($dv!=$_POST['rut_dv'])
			$errores[]='Digito verificador del Rut incorrecto';
	}
	return $errores;
}

function obtener_digito_verificador($rut){
	$d=false;
	$rut = strrev($rut);
	$aux = 1;
	$s = 0;
	for($i=0;$i<strlen($rut);$i++){
		$aux++;
		$s += intval($rut[$i])*$aux;
		if($aux == 7){ $aux=1; }
	}
	$digit = 11-$s%11;
	if($digit == 11){$d = 0;}
	elseif($digit == 10){$d = "K";}
	else{$d = $digit;}
	return $d;
}

function comprobar_nombre_usuario($nombre_usuario){
   //compruebo que el tamaño del string sea válido.
   if (strlen($nombre_usuario)<3 || strlen($nombre_usuario)>20){
      echo $nombre_usuario . " no es válido<br>";
      return false;
   }

   //compruebo que los caracteres sean los permitidos
   $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
   for ($i=0; $i<strlen($nombre_usuario); $i++){
      if (strpos($permitidos, substr($nombre_usuario,$i,1))===false){
         //echo $nombre_usuario . " no es válido<br>";
         return false;
      }
   }
   //echo $nombre_usuario . " es válido<br>";
   return true;
} 