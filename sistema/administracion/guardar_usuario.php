<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');


if(isset($_POST['confirmar']) and $_POST['confirmar']=='Confirmar'){
	
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_bombero = new bombero($con);
	$t_usuario = new usuario($con);
	$errores=verificar_errores($t_usuario);
	if($t_bombero->existe_bombero($_POST['rut'])==false)
		$errores[]='Rut ingresado no se encuentra registrado';

	if($errores==array()){
		//$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
		//
		
		$habilitado=0;
		if(isset($_POST['habilitado']))
			$habilitado=1;
			
		$t_usuario->guardar_usuario($_POST['rut'], $_POST['uname'], $_POST['upass'], $_POST['tipo_usuario'], $habilitado);			
		header('location: usuarios.php');
		
	}
	else{	
		$enlace_volver='agregar_usuario.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else if(isset($_POST['confirmar']) and $_POST['confirmar']=='Actualizar'){
	//el usuario que se esta actualizando esta guardado en variable de session
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_usuario = new usuario($con);
	$rut =$t_usuario->obtener_rut($_SESSION['uname']);
	
	$errores=verificar_errores_actualizar($t_usuario);	
	//$t_bombero = new bombero($con);
	//if($t_bombero->existe_bombero($rut)==false)
		//$errores[]='Rut ingresado no se encuentra registrado';

	if($errores==array()){
		//$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
		//$t_usuario = new usuario($con);
		
		$habilitado=0;
		if(isset($_POST['habilitado']))
			$habilitado=1;
			
		$t_usuario->actualizar_usuario($rut, $_SESSION['uname'], $_POST['upass'], $_POST['tipo_usuario'], $habilitado);
			
		header('location: usuarios.php');
		
	}
	else{	
		$enlace_volver='modificar_usuario.php?uname='.$_SESSION['uname'];
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else {
	header('location: usuarios.php');
}

function verificar_errores($t_usuario) {
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='<strong>Solo un usuario administrador puede agregar usuarios</strong>';
		
	if(trim($_POST['uname'])=='') {
		$errores[]='Campo nombre de usuario es requerido';	
	}		
	if(trim($_POST['upass'])=='') {
		$errores[]='Campo password es requerido';	
	}		
	if(trim($_POST['tipo_usuario'])=='') {
		$errores[]='Campo tipo usuario es requerido';	
	}		
	if(trim($_POST['rut'])==''){
		$errores[]='Campo Rut está vacío';
	}
	else{
		$dv=obtener_digito_verificador($_POST['rut']);
		$_POST['rut_dv']=strtoupper($_POST['rut_dv']);
		if($dv!=$_POST['rut_dv'])
			$errores[]='Digito verificador del Rut incorrecto';
	}
	return $errores;
}

function verificar_errores_actualizar($t_usuario) {
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='<strong>Solo un usuario administrador puede modificar usuarios</strong>';
		
	//if(trim($_POST['uname'])=='') {
		//$errores[]='Campo nombre de usuario es requerido';	
	//}		
	//if(trim($_POST['upass'])=='') {
		//$errores[]='Campo password es requerido';	
	//}		
	if(trim($_POST['tipo_usuario'])=='') {
		$errores[]='Campo tipo usuario es requerido';	
	}
	return $errores;
}

function obtener_digito_verificador($rut){
	$d=false;
	$rut = strrev($rut);
	$aux = 1;
	$s = 0;
	for($i=0;$i<strlen($rut);$i++){
		$aux++;
		$s += intval($rut[$i])*$aux;
		if($aux == 7){ $aux=1; }
	}
	$digit = 11-$s%11;
	if($digit == 11){$d = 0;}
	elseif($digit == 10){$d = "K";}
	else{$d = $digit;}
	return $d;
}