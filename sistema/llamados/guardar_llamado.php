<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_llamado.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_llamado_bomberos.php');
$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_cod_region = new cod_region($con);
$t_bombero = new bombero($con);
$t_llamado = new llamado($con);
$t_llamado_bomberos = new llamado_bomberos($con);

$bomberos = $t_bombero->obtener_checkbox_bomberos(array(1));
$form['fecha_ini'] = date('Y-m-d');
$form['hora_ini'] = '';
$form['fecha_fin'] = date('Y-m-d');
$form['hora_fin'] = '';
$form['dir_region'] = '13';
$form['dir_comuna'] = '';
$form['dir_calle'] = '';
$form['dir_numero'] = '';
$form['observaciones'] = '';
$form['rut'] = '';
$form['rut_dv'] = '';

//if($_POST['cod_tipo_llamado']=='')
	//$cod_tipo_llamado = 0;

$id_llamado=$t_llamado->guardar_llamado($_POST['cod_tipo_llamado'], $_POST['fecha_ini'], $_POST['hora_ini'], $_POST['fecha_fin'], $_POST['hora_fin'], $_POST['dir_region'], $_POST['dir_comuna'], $_POST['dir_calle'], $_POST['dir_numero'], $_POST['dir_calle2'], $_POST['observaciones'], $_POST['rut_acargo']);

foreach($_POST['rut_asistentes'] as $rut){
	if($rut!=$_POST['rut_acargo']) //para no guardar el mismo dato en 2 tablas
	$t_llamado_bomberos->guardar_bombero($rut, $id_llamado);
}


// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
//$regiones = $t_cod_region->obtener_regiones();

//include('../../vista/llamados/v_llamado_guardado.php');

header('location: consulta_llamados.php');