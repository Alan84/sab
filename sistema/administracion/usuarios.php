<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

$_SESSION['uname']='';
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_usuario = new usuario($con);
$t_tipo_usuario = new tipo_usuario($con);
$usuarios = $t_usuario->obtener_lista_usuarios();



include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_usuarios.php';