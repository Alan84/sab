<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_pagos.php');


include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/PHPMailer.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/SMTP.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/OAuth.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/Exception.php";




if(isset($_POST['confirmar']) and $_POST['confirmar']!=''){
	//ya pasó por confirmar, ahora se envian los emails
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_bombero = new bombero($con);
	$t_parametro = new parametro($con);
	$t_pago = new pago($con);
	$m_pagos = new m_pagos($con);
	$bomberos =array();
	$errores=verificar_errores();
	if($errores==array()){
		//$i_bomberos=0;
		$email_bombero='';
		foreach($_POST['sel'] as $rut) {
			$email_bombero = $t_bombero->obtener_email($rut);
			if($email_bombero!=''){ // solo se enviarán emails a bomberos q registren uno 
				$valor_cuota=$t_parametro->obtener_parametro('valor_cuota');
				$morosidad_limite=$t_parametro->obtener_parametro('morosidad_limite');
				$cantidad_cuotas_impagas=$m_pagos->cantidad_cuotas_impagas($rut);
				$fecha_vencimiento=$m_pagos->obtener_ultima_fecha_pago($rut);
				$bomberos[] = $t_bombero->obtener_datos_basicos($rut);
			
				if($fecha_vencimiento===false)
					$mostrar_fecha_v = 'No registra';
				else 
					$mostrar_fecha_v = obtener_formato_cuota(substr($fecha_vencimiento, 0, 7));
				
				if($cantidad_cuotas_impagas===false)
					$mostrar_cuotas_impagas=1;
				else
					$mostrar_cuotas_impagas=$cantidad_cuotas_impagas;
				
					//print_r($cantidad_cuotas_impagas);
				
				$cuerpo = file_get_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/correo/cuotas_atrazadas.htm');
				$cuerpo = str_replace('[rut]', number_format($rut, 0, ',', '.').'-'.$t_bombero->obtener_rut_dv($rut), $cuerpo);
				$cuerpo = str_replace('[nombre]', $t_bombero->obtener_nombre_completo($rut), $cuerpo);
				$cuerpo = str_replace('[u_cuota]', $mostrar_fecha_v, $cuerpo);
				$cuerpo = str_replace('[valor_cuota]', '$'.number_format($valor_cuota, 0, ',','.'), $cuerpo);
				$cuerpo = str_replace('[cuotas_impagas]', $mostrar_cuotas_impagas, $cuerpo);
				$cuerpo = str_replace('[deuda]', '$'.number_format($mostrar_cuotas_impagas*$valor_cuota, 0, ',','.'), $cuerpo);
				if($morosidad_limite>0 and $mostrar_cuotas_impagas>=$morosidad_limite)
					$cuerpo = str_replace('[comentario_extra]', sprintf(COMENTARIO_EXTRA_MOROSIDAD , $morosidad_limite), $cuerpo);
				else
					$cuerpo = str_replace('[comentario_extra]', '', $cuerpo);
			
				enviar_correos($email_bombero, $cuerpo);
				$t_bombero->update_fecha_mail_alerta($rut);
			}
			//$i_bomberos++;
		}
		//foreach($parametros as $param)
		//	$t_parametro->actualizar_parametro($param['nombre'], trim($_POST[$param['nombre']]));
		//include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_enviar_emails.php';
		header('location: bomberos_morosos.php');
	}	
	else{
		$enlace_volver='bomberos_morosos.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
//else if(isset($_POST['enviar']) and $_POST['enviar']=='Eliminar'){
//	include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_eliminar_usuario.php';
//}
else if(isset($_POST['enviar']) and $_POST['enviar']!=''){
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_bombero = new bombero($con);
	$t_parametro = new parametro($con);
	$m_pagos = new m_pagos($con);
	$bomberos =array();
	$errores=verificar_errores();
	if($errores==array()){
		$i_bomberos=0;
		foreach($_POST['sel'] as $rut) {
			$bomberos[$i_bomberos] = $t_bombero->obtener_datos_basicos($rut);
			$bomberos[$i_bomberos]['email'] = $t_bombero->obtener_email($rut);
			if($bomberos[$i_bomberos]['email'] =='') $bomberos[$i_bomberos]['email'] = '<strong style="text-decoration: underline;"><em>No registra Email</em></strong>';
			$bomberos[$i_bomberos]['rut'] = $rut;
			$bomberos[$i_bomberos]['rut_dv'] = $t_bombero->obtener_rut_dv($rut);
			$i_bomberos++;
		}
		$valor_cuota=$t_parametro->obtener_parametro('valor_cuota');
		$morosidad_limite=$t_parametro->obtener_parametro('morosidad_limite');
		$cantidad_cuotas_impagas=$m_pagos->cantidad_cuotas_impagas($bomberos[0]['rut']);
		$fecha_vencimiento=$m_pagos->obtener_ultima_fecha_pago($bomberos[0]['rut']);
		if($fecha_vencimiento===false)
			$mostrar_fecha_v = 'No registra';	
		else 
			$mostrar_fecha_v = obtener_formato_cuota(substr($fecha_vencimiento, 0, 7));
				
		if($cantidad_cuotas_impagas===false)
			$mostrar_cuotas_impagas=1;
		else
			$mostrar_cuotas_impagas=$cantidad_cuotas_impagas;
				
				
		$cuerpo = file_get_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/correo/cuotas_atrazadas.htm');
		$cuerpo = str_replace('[rut]', number_format($bomberos[0]['rut'], 0, ',', '.').'-'.$t_bombero->obtener_rut_dv($bomberos[0]['rut']), $cuerpo);
		$cuerpo = str_replace('[nombre]', $t_bombero->obtener_nombre_completo($bomberos[0]['rut']), $cuerpo);
		$cuerpo = str_replace('[u_cuota]', $mostrar_fecha_v, $cuerpo);
		$cuerpo = str_replace('[valor_cuota]', '$'.number_format($valor_cuota, 0, ',','.'), $cuerpo);
		$cuerpo = str_replace('[cuotas_impagas]', $mostrar_cuotas_impagas, $cuerpo);
		$cuerpo = str_replace('[deuda]', '$'.number_format($mostrar_cuotas_impagas*$valor_cuota, 0, ',','.'), $cuerpo);
		
		if($morosidad_limite>0 and $mostrar_cuotas_impagas>=$morosidad_limite)
			$cuerpo = str_replace('[comentario_extra]', sprintf(COMENTARIO_EXTRA_MOROSIDAD , $morosidad_limite), $cuerpo);
		else
			$cuerpo = str_replace('[comentario_extra]', '', $cuerpo);
		//foreach($parametros as $param)
		//	$t_parametro->actualizar_parametro($param['nombre'], trim($_POST[$param['nombre']]));
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_enviar_emails.php';
	}
	else{
		$enlace_volver='bomberos_morosos.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else {
	header('location: bomberos_morosos.php');
}
//print_r($_POST);
function verificar_errores(){
	$errores=array();
	
	if(!isset($_POST['sel']) or count($_POST['sel'])==0)
			$errores[]='No ha seleccionado ningún bombero';
	
	return $errores;
}

function enviar_correos($email_destino, $cuerpo) {

	//require '/usr/share/php/libphp-phpmailer/autoload.php';
	//namespace PHPMailer\PHPMailer
	//echo DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/PHPMailer.php";
	//$emails = ;
	
	

	$email_user = CORREO_ENVIO_MAILS;
	$email_password = PASSWORD_ENVIO_MAILS;
	$the_subject = SUBJECT_EMAILS_CUOTAS_ATRAZADAS;
	$address_to = $email_destino;
	
	//echo $email_destino. ' '.$cuerpo."\r ";
		
	$from_name = FROM_EMAILS_SISTEMA;
	$phpmailer = new PHPMailer();

	// ---------- datos de la cuenta de Gmail -------------------------------
	$phpmailer->Username = $email_user;
	$phpmailer->Password = $email_password; 
	//-----------------------------------------------------------------------
	// $phpmailer->SMTPDebug = 1;
	$phpmailer->SMTPSecure = 'ssl';
	$phpmailer->Host = "smtp.gmail.com"; // GMail
	$phpmailer->Port = 465;
	$phpmailer->IsSMTP(); // use SMTP
	$phpmailer->SMTPAuth = true;
	$phpmailer->setFrom($phpmailer->Username,$from_name);
	$phpmailer->AddAddress($address_to); // recipients email
	$phpmailer->Subject = $the_subject;	
	$phpmailer->Body = $cuerpo;
	$phpmailer->IsHTML(true);
	$phpmailer->Send();
}

function obtener_formato_cuota($cuota) {
	$trazada = explode('-', $cuota);
	
	return obtener_mes($trazada[1]).' de '.$trazada[0];
}

function obtener_mes($mes) {
	if ($mes!=''){ 
		$meses=array('01'=> 'Enero', '02'=> 'Febrero', '03'=>'Marzo', '04'=> 'Abril', '05'=> 'Mayo', '06'=> 'Junio',
		'07'=> 'Julio' , '08'=> 'Agosto', '09'=> 'Septiembre', '10'=> 'Octubre', '11'=> 'Noviembre', '12'=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>Sin información</em>';
	}
}
