<?php

class premio_anios{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function existe_premio($rut, $id) {
		$sql="select 1 from premio_anios where rut=? and id_premio_anios=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if($query->rowCount())
			return true;
		else
			return false;
	}
	
	function cuenta_premios($rut) {
		$sql="select count(is_premio_anios) from premio_anios where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
	
	function guardar_premio($rut, $id,$anios, $anio_premio_cia, $anio_premio_cuerpo) {
		$sql="insert into premio_anios (rut, id_premio_anios, anios, anio_premio_cia, anio_premio_cuerpo) values
						(?, ?, ?, ?, ?)";
		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id, $anios, $anio_premio_cia, $anio_premio_cuerpo));
	}
	
	function actualizar_premio($rut, $id,$anios, $anio_premio_cia, $anio_premio_cuerpo) {
		$sql="update premio_anios set anios=?, anio_premio_cia=?, anio_premio_cuerpo=?
						where rut=? and id_premio_anios=?";
		$q=$this->con->prepare($sql);
		$q->execute(array($anios, $anio_premio_cia, $anio_premio_cuerpo, $rut, $id));
	}
	
	function borrar_premio($rut, $id) {
		$sql="delete from premio_anios where rut=? and id_premio_anios=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
	}
	
	function obtener_ultimo_id($rut) {
		$sql="select id_premio_anios from premio_anios where rut=? order by id_premio_anios desc limit 1";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
}
