<?php


session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_est_bombero = new est_bombero($con);
$m_bomberos = new m_bomberos($con);

$estados=$t_est_bombero->obtener_estados_bomberos();
/*
$sql="select * from est_bombero";
foreach($con->query($sql) as $row)
	$estados[$row['id_est_bombero']]=$row['descripcion'];
*/

$filtro_buscar='';
$filtro_estados=array(1,2,3,4,5, 6); //el estado uno es el por defecto
//obteniendo las variables de busqueda
$filtro_sql=' and b.borrado=1 ';
//ordenado por defecto por fecha de actualizacion, la mas nueva primero
$orden_sql=" b.fecha_actualiza desc ";
$orden_link_cuota=$orden_link_rut=$orden_link_nom=$orden_link_edad=$orden_link_est=$orden_link_act='&tipo_orden=asc';
$simbolo_orden_cuota=$simbolo_orden_rut=$simbolo_orden_nom=$simbolo_orden_edad=$simbolo_orden_est=$simbolo_orden_act='';
$link_busqueda='';

if($_GET!==array()){
	if(isset($_GET['buscar']))
		$filtro_buscar=trim($_GET['buscar']);
	$filtro_sql=' and b.borrado=1 ';
	if($filtro_buscar!=''){
		$filtro_buscar=strtoupper($filtro_buscar);
		$filtro_sql.=" and (UPPER(b.nombre) like '%$filtro_buscar%' or UPPER(b.apellido) like '%$filtro_buscar%' or b.rut like '$filtro_buscar') ";
		$link_busqueda.='&buscar='.urlencode($filtro_buscar);
	}


	$estados_sql='';
	$hay_filtro_estado=0;
	foreach($estados as $id=>$est)
		if(isset($_GET['estado'.$id]) and $_GET['estado'.$id]=='s'){
			if($hay_filtro_estado==0)
				$filtro_estados=array();
			if($estados_sql!='')
				$estados_sql.=', ';
			$filtro_estados[]=$id;
			$estados_sql.=$id;
			$hay_filtro_estado=1;
			$link_busqueda.='&estado'.$id.'=s';
		}
	if($estados_sql!='')
		$filtro_sql.=" and b.id_est_bombero in ($estados_sql) ";

	//verificar orden
	if(isset($_GET['orden'])){
		$orden='b.fecha_actualiza';
		$tipo_orden='desc';
		switch($_GET['orden']){
			case 'rut':
				$orden='b.rut';
				$simbolo_orden_rut=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_rut='&tipo_orden=desc';
					$simbolo_orden_rut=' ▲';
				}
			break;
			case 'nombre':
				$orden='b.nombre';
				$simbolo_orden_nom=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_nom='&tipo_orden=desc';
					$simbolo_orden_nom=' ▲';
				}
			break;
			case 'edad':
				$orden='b.fecha_nac';
				$simbolo_orden_edad=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_edad='&tipo_orden=desc';
					$simbolo_orden_edad=' ▲';
				}
			break;
			case 'estado':
				$orden='est_bombero';
				$simbolo_orden_est=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_est='&tipo_orden=desc';
					$simbolo_orden_est=' ▲';
				}
			break;
			case 'cuota':
				$orden='cuota';
				$simbolo_orden_cuota=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_cuota='&tipo_orden=desc';
					$simbolo_orden_cuota=' ▲';
				}
			break;
			case 'actualiza':
				$orden='b.fecha_actualiza';
				$simbolo_orden_act=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_act='&tipo_orden=desc';
					$simbolo_orden_act=' ▲';
				}
			break;

		}
		$orden_sql=" $orden $tipo_orden ";
	}
}

$result=$m_bomberos->obtener_bomberos($filtro_sql, $orden_sql);

$est_bombero=new tabla($con, 'est_bombero', 'descripcion', 'id_est_bombero');



include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_bomberos_borrados.php');

class tabla{
	public $con;
	public $nombre;
	public $dato;
	public $campow;

	public function __construct($con, $nombre, $dato, $campow){
		$this->con=$con;
		$this->nombre=$nombre;
		$this->dato=$dato;
		$this->campow=$campow;
	}

	public function obtener_dato($id){
		$sql="select ".$this->dato." from ".$this->nombre." where ".$this->campo."='".$id."'";
		$query=$this->con->query($sql);
		if($query!==false){
			$result=$query->fetchAll(PDO::FETCH_ASSOC);
			return $result[0][$this->dato];
		}
		else{
			return '';
		}
	}
}

function obtener_mes($mes) {
	if ($mes!=''){ 
		$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
		7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>Sin información</em>';
	}
}
