<?php

class tipo_cargo{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_tipos_cargos() {
		$datos=array();
		$sql="select * from cod_cargo";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_cod_cargo']]=$row['descripcion'];

		return $datos;
	}
}
