<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_pago = new pago($con);
$t_parametro = new parametro($con);
$t_bombero = new bombero($con);

//print_r($_POST['fechas']);
$primer_pago=1; //asumimos que es primer pago
if($t_pago->existe_pago_para_rut($_POST['rut']))
	$primer_pago=0; //ya existe pago, por no es su primer pago

$errores=check_errores($primer_pago);
if(isset($_POST['enviar'])){
	if($errores==array()){
	$fechas_pago=array();
	$cant_meses_pagando=0;
//	$fecha_pago=$_POST['fecha_pago_a'].'-'.$_POST['fecha_pago_m'].'-01';
	$bombero = $t_bombero->obtener_datos_basicos($_POST['rut']);
	$suma_anio=0;
	$suma_mes=0;

	//ordenamos las fechas a partir de la seleccionada hasta el últmo pago
//	$mes=$_POST['fecha_pago_m'];
//	$anio=$_POST['fecha_pago_a'];
	$x=0;
	foreach($_POST['fechas'] as $fecha){
		$fecha_separada=preg_split('/\-/', $fecha);
		$fechas_pago[$x++]=obtener_mes($fecha_separada[1]).' '.$fecha_separada[0];
	}

	//se puede modificar el monto
	if($_POST['otro_monto']!=''){
		$monto=$_POST['otro_monto'];
	}
	else{
		$monto=$t_parametro->obtener_parametro('valor_cuota');
		$monto=$monto*count($_POST['fechas']);
	}

	$email='';
	if(isset($_POST['enviar_mail']) and $_POST['enviar_mail']==1)
		$email=$_POST['email'];


	$ultimo_indice=$t_pago->obtener_ultimo_indice();
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_revisar_pago.php');
	}
	else{
		$enlace_volver='ingresar_pago_nuevo.php?rut='.$_POST['rut'];
		include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
	}
}
else {	
	header('location: ver_pagos.php?rut='.$_POST['rut']);	
}
function check_errores($primer_pago=0){
	//$t_pago = new pago($con);
	//$fecha_ultimo=$t_pago->obtener_ultima_fecha_vencimiento($dats['rut']);
	$errors=array();
	if(trim($_POST['otro_monto'])!='' and trim($_POST['comentario'])=='')
			$errors[]='Si modifica el monto, debe agregar un comentario.';



//	if(trim($dats['fecha_pago_a'])=='')
//		$errors[]='Campo año de fecha de pago está vacío';
//	if(trim($dats['fecha_pago_m'])=='')
//		$errors[]='Campo mes de fecha de pago está vacío';

//	$x=0;
//	foreach($_POST['fechas'] as $fecha){
//		$fecha_separada=preg_split('/\-/', $fecha);
//		$fechas_pago[$x++]=obtener_mes($fecha_separada[1]).' '.$fecha_separada[0];
//	}

/*	print_r('u:'.$fecha_ultimo);
	if($fecha_ultimo!=FALSE){
		if(!($fecha_ultimo['fecha_siguente_a']==$_POST['fechas'][0]['a'] and
			$fecha_ultimo['fecha_siguente_m']==$_POST['fechas'][0]['m']))
		$errors[]='Si se salta el primer mes, debe justificarlo con un comentario.';
	}
	else{
*/
	$existenTodos=TRUE;
	$errorMostrado=FALSE;
	if(isset($_POST['fechas'])){
		$primerCiclo=FALSE;
		foreach($_POST['fechas'] as $indice => $dato){
			if($primerCiclo==FALSE){ //con la ayuda de una bandera guardamos el primer indice
				$primerCiclo=TRUE;
				$indicePrimero=$indice;
			}
		}
		$errorMostrado=FALSE;
		for ($x=$indicePrimero;$x<count($_POST['fechas']); $x++){
			if(!isset($_POST['fechas'][$x]) and trim($_POST['comentario'])=='' and !$errorMostrado){
				$errors[]='Si las fechas de pago no son seguidas, debe justificarlo con un comentario.';
				$errorMostrado=TRUE;
			}
		}
	}
	
	

	if(!isset($_POST['fechas']) or count($_POST['fechas'])==0)
		$errors[]='Debe ingresar al menos una fecha de pago.';
	if(!isset($_POST['fechas'][0]) and trim($_POST['comentario'])=='' and $primer_pago==0)
		$errors[]='Si se salta el primer mes, debe justificarlo con un comentario.';


//	}
	return $errors;
}

function obtener_mes($mes) {
	if ($mes!=''){
		$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
		7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>Sin información</em>';
	}
}
