<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php'); //se usa en la vista
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cargo_actual.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_cargo.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_est_bombero = new est_bombero($con);
$t_cargo = new cod_cargo($con);
$t_cargo_actual = new cargo_actual($con);
$m_cargos = new m_cargos($con); //se usa en la vista
$t_bombero = new bombero($con);

// seteamos los meses para usarlos en el combo
$meses=array(''=>'', 1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$estados=$t_est_bombero->obtener_estados_bomberos();
$cargos=$t_cargo->obtener_cargos();
$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','No cambiar'), array('0', 'Sólo eliminar cargo actual')));

/*
foreach($cargos as $cargo){
	$form['fecha_inicio_d'][$cargo['id_cod_cargo']]=date('d');
	$form['fecha_inicio_m'][$cargo['id_cod_cargo']]=date('m');
	$form['fecha_inicio_a'][$cargo['id_cod_cargo']]=date('Y');
} */

//obtenemos los valores devueltos por post
foreach($_POST as $indice=> $dato){
	$form[$indice]=$dato;
}

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_cargos.php');
