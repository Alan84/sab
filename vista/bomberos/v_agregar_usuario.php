<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Agregar usuario') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T22:38:52-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div style="width: 15em; float: left">
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>
<div class="tabla" style="float: left">
<h1 style="color: white; background-color: black">Agregar Bombero</h1>
<form method="post" action="guardar_bombero.php">
	<table><tr>
	<td>nombres:</td><td><input type="text" name="nombre" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>apellidos:</td><td><input type="text" name="apellido" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>rut:</td><td><input type="number" name="rut" class="amarillo" style="width: 9em" /> - <input type="number" name="dv" class="amarillo" style="width: 3em" /></td></tr>
	<tr><td>profesión:</td><td><input type="text" name="profesion" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>estado civil:</td><td>
	<select>
		<option>soltero</option>
		<option>casado</option>
	</select></td></tr>
	<tr><td>grupo sanguineo:</td><td><input type="text" name="gruposan" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>dirección:</td><td><input type="text" name="direccion" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>telefono movil:</td><td><input type="tel" name="movil" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>telefono fijo:</td><td><input type="search" name="telefono" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>email:</td><td><input type="email" name="email" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>fecha de nacimiento:</td><td><input type="date" name="fechanac" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>activo:</td><td><input type="checkbox" name="act" class="amarillo" /></td></tr>
	</table>
	<p style="text-align: right; margin: 0.5em"><input type="submit" value="guardar" style="" /> </p>
</form>
</div>
</div>
</body>
</html>
