<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Subir respaldo base de datos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T21:26:18-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>



</div>


<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">

<h1 style="color: white; background-color: black">Subir respaldo de la base de datos</h1>
<p><strong><em>Importante: </em></strong> La acción tomará efecto de forma <em style="text-decoration: underline;">inmediata</em>. Los datos se resetearán al 
	estado en los que estaban al momento de hacer el respaldo, esto quiere decir que si se agregó información posterior, esta será <em style="text-decoration: underline;">eliminada</em>. </p>
	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/subir_respaldo_db.php" method="post" enctype="multipart/form-data">
			Archivo: <input type="file" name="sql_file" value="" />
			<p><input type="submit" name="cancelar" value="Cancelar" /> <input type="submit" name="enviar" value="Subir" /></p>
	</form>
</div>
</body>
</html>