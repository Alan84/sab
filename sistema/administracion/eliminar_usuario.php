<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');


if(isset($_POST['confirmar'])){
//print_r($_POST);
//print_r($_SESSION);
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_usuario = new usuario($con);
	$t_bombero = new bombero($con);
	$t_pago = new pago($con);
	$t_bombero->set_usuario_actualiza($_SESSION['uname'], '');
	$t_pago->set_usuario($_SESSION['uname'],'');
	$t_usuario->eliminar_usuario($_SESSION['uname']);
	$_SESSION['uname']='';
}

header('location: usuarios.php');