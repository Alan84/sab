<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Bomberos morosos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:49-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<form method="get" action="bomberos_morosos.php">
<h1 style="color: white; background-color: black">Lista bomberos morosos</h1>
<p>Meses de morosidad mayor o igual a: 
<input type="number" name="meses" value="<?php echo $form['meses'] ?>" min="1" onchange="resaltar_envio()" /></p>
<!-- <p>Bomberos sin registro de pagos: <input type="checkbox" name="bom_sin_registro" value="1" checked="checked" /></p> -->

<ul style="margin-left: 0; padding-left: 0">
<?php foreach($estados as $id=>$est){ $check=''; if(in_array($id, $filtro_estados)) $check='checked="checked" '; ?>
	<li style="display: inline; margin: 0 0 0 1.2em; padding: 0; white-space:nowrap;"><input type="checkbox" id="check<?php echo $id ?>" onclick="resaltar_envio()" name="estado<?php echo $id; ?>" value="s" <?php echo $check ?>/><label for="check<?php echo $id ?>"><?php echo $est ?></label></li>
<?php } ?>
</ul>
<input type="submit" id="envio" value="¡filtrar!" />
</form>
<form method="post" action="confirmar_enviar_emails.php">
<table class="tabla" style="margin-top: 1em">
<thead>
<tr>
	<!-- <th>selecionar</th> -->
	<th><a href="bomberos_morosos.php?orden=rut<?php echo $orden_link_rut.$link_busqueda ?>" style="color: white">rut<?php echo $simbolo_orden_rut ?></a></th>
	<th><a href="bomberos_morosos.php?orden=nombre<?php echo $orden_link_nom.$link_busqueda ?>" style="color: white">nombre<?php echo $simbolo_orden_nom ?></a></th>
	<th><a href="bomberos_morosos.php?orden=edad<?php echo $orden_link_edad.$link_busqueda ?>" style="color: white">email<?php echo $simbolo_orden_edad ?></a></th>
	<th><a href="bomberos_morosos.php?orden=estado<?php echo $orden_link_est.$link_busqueda ?>" style="color: white">estado<?php echo $simbolo_orden_est ?></a></th>
	<th><a href="bomberos_morosos.php?orden=cuota<?php echo $orden_link_cuota.$link_busqueda ?>" style="color: white">última cuota<?php echo $simbolo_orden_cuota ?></a></th>
	<th><a href="bomberos_morosos.php?orden=actualiza<?php echo $orden_link_act.$link_busqueda ?>" style="color : white">email alerta</a></th>
	<th>Sel.</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($result as $dat){ ?>
	<tr>
		<!--  <td style="text-align: center"><input type="checkbox" name="rut_<?php echo $i++;?>" value="<?php echo $dat['rut'] ?>" /></td> -->
		<td style="text-align: right"><a href="../bomberos/ver_bombero.php?rut=<?php echo $dat['rut'] ?>"><?php echo number_format($dat['rut'], 0, ',', '.').'-'.$dat['rut_dv'] ?></a></td>
		<td><?php echo $dat['nombre'].' '.$dat['apellido'] ?></td>
		<td style="text-align: right"><?php echo $dat['email'] ?></td>
		<td style="text-align: center"><?php echo $dat['est_bombero'] ?></td>
		<td style="text-align: center"><a href="../bomberos/ver_pagos.php?rut=<?php echo $dat['rut']?>"><?php echo obtener_mes($dat['fecha_vence_m']).' '.$dat['fecha_vence_a']; ?></a></td>
		<td style="text-align: center"><?php echo $dat['fecha_mail_alerta']; ?></td>
		<td style="text-align: center"><input type="checkbox" name="sel[]" value="<?php echo $dat['rut']; ?>" /></td>
	</tr>
<?php } ?>
<tbody>
</table>
<p style="text-align: right"><input type="submit" name="enviar" value="Enviar correo masivo" /></p>
</form>
</div>
</div>
</body>
</html>