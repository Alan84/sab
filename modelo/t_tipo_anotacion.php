<?php

class tipo_anotacion{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_tipos_anotaciones() {
		$datos=array();
		$sql="select * from tipo_anotacion";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_tipo_anotacion']]=$row['descripcion'];

		return $datos;
	}
}
