<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Consulta bomberos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-13T01:26:02-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<form method="get" action="consulta_bomberos.php">
<h1 style="color: white; background-color: black">Lista bomberos</h1>
Nombre: <input type="search" onchange="resaltar_envio()" name="buscar" value="<?php echo $filtro_buscar ?>" />
<ul style="margin-left: 0; padding-left: 0">
<?php foreach($estados as $id=>$est){ $check=''; if(in_array($id, $filtro_estados)) $check='checked="checked" '; ?>
	<li style="display: inline; margin: 0 0 0 1.2em; padding: 0; white-space:nowrap;"><input type="checkbox" id="check<?php echo $id ?>" onclick="resaltar_envio()" name="estado<?php echo $id; ?>" value="s" <?php echo $check ?>/><label for="check<?php echo $id ?>"><?php echo $est ?></label></li>
<?php } ?>
</ul>
<input type="submit" id="envio" value="¡filtrar!" />
</form>
<table class="tabla" style="margin-top: 1em">
<thead>
<tr>
	<!-- <th>Sel.</th> -->
	<th rowspan="2"><a href="consulta_bomberos.php?orden=rut<?php echo $orden_link_rut.$link_busqueda ?>" style="color: white">Rut<?php echo $simbolo_orden_rut ?></a></th>
	<th rowspan="2"><a href="consulta_bomberos.php?orden=nombre<?php echo $orden_link_nom.$link_busqueda ?>" style="color: white">Nombre<?php echo $simbolo_orden_nom ?></a></th>
	<th rowspan="2"><a href="consulta_bomberos.php?orden=edad<?php echo $orden_link_edad.$link_busqueda ?>" style="color: white">Ed.<?php echo $simbolo_orden_edad ?></a></th>
	<th rowspan="2"><a href="consulta_bomberos.php?orden=estado<?php echo $orden_link_est.$link_busqueda ?>" style="color: white">Estado<?php echo $simbolo_orden_est ?></a></th>
	<th colspan="2"><a href="consulta_bomberos.php?orden=llamados<?php echo $orden_link_llam.$link_busqueda ?>" style="color: white">Llamados<?php echo $simbolo_orden_llam ?></a></th>
	<?php if($puede_ver_pagos) { ?>
		<th rowspan="2"><a href="consulta_bomberos.php?orden=cuota<?php echo $orden_link_cuota.$link_busqueda ?>" style="color: white">Última cuota<?php echo $simbolo_orden_cuota ?></a></th>
	<?php } ?>
	<th rowspan="2"><a href="consulta_bomberos.php?orden=actualiza<?php echo $orden_link_act.$link_busqueda ?>" style="color : white">Actualiza<?php echo $simbolo_orden_act ?></a></th>
	<th rowspan="2">Acción</th>
</tr>
<tr>
	<th title="A cargo">Ac</th>
	<th title="Asiste">As</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($result as $dat){ ?>
	<tr>
		<!--  <td style="text-align: center"><input type="checkbox" name="rut_<?php echo $i++;?>" value="<?php echo $dat['rut'] ?>" /></td> -->
		<td style="text-align: right"><a href="ver_bombero.php?rut=<?php echo $dat['rut'] ?>"><?php echo number_format($dat['rut'], 0, ',', '.').'-'.$dat['rut_dv'] ?></a></td>
		<td><?php echo $dat['nombre'].' '.$dat['apellido'] ?></td>
		<td style="text-align: right"><?php echo $dat['edad'] ?></td>
		<td style="text-align: center"><?php echo $dat['est_bombero'] ?></td>
		<td style="text-align: center"><?php echo $dat['cant_acargo']; ?></td>
		<td style="text-align: center"><?php echo $dat['cant_asiste']; ?></td>
		<?php if($puede_ver_pagos) { ?>
			<td style="text-align: center"><a href="ver_pagos.php?rut=<?php echo $dat['rut']?>"><?php echo obtener_mes($dat['fecha_vence_m']).' '.$dat['fecha_vence_a']; ?></a></td>
		<?php } ?>
		<td style="text-align: center"><?php echo $dat['fecha_actualiza'] ?> (<?php echo $dat['usuario_actualiza'] ?>)</td>
		<td><ul style="margin:0; padding: 0 0 0 1em">
			<li style="margin:0; padding: 0"><a href="descargar_certificado.php?rut=<?php echo $dat['rut'];?>">Ver certificado</a></li>
			<li style="margin:0; padding: 0"><a href="confirmar_borrar_bombero.php?rut=<?php echo $dat['rut'];?>" style="color: red;">Borrar</a></li>
			<!-- <li><a href="borrar_bombero.php?rut=<?php echo $dat['rut'];?>">borrar bombero</a></li> -->
		</ul></td>
	</tr>
<?php } ?>
<tbody>
</table>
</div>
</div>
</body>
</html>