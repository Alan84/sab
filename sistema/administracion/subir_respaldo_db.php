<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_usuario = new usuario($con);

$errores=verificar_errores($t_usuario);
if($errores==array()){
	if(isset($_POST['cancelar'])) {
		header('location: respaldo.php');
	}
	else if(isset($_POST['enviar'])) { //subiendo archivo
		//print_r($_FILES);
	
		if($_FILES['sql_file']['name']!='' and $_FILES['sql_file']['type']=='application/sql'){ // Se está subiendo foto
			$archivo = $_FILES['sql_file']['tmp_name']; //sprintf(NOM_ARCHIVO_SQL_RESPALDO, $dbname, date('d-m-Y'));
			system(sprintf(COMANDO_MYSQLDUMP_RESTABLECER, $archivo), $return_var);
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_respaldo_db_subido.php';
		}
		else {
			$enlace_volver='respaldo.php';
			$errores=array('No se ha seleccionado ningún archivo o el archivo no es el formato correcto.');
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
		}
		
	}
	else {
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_subir_respaldo_db.php';
	}
}
else {
		$enlace_volver='respaldo.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';	
}

function verificar_errores($t_usuario) {
	$errores=array();
	
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='<strong>Solo un usuario administrador puede restablecer la base de datos</strong>';
		
	return $errores;
}