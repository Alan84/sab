<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_pagos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/PHPMailer.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/SMTP.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/OAuth.php";
include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA."/class/PHPMailer-master/src/Exception.php";

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_pago = new pago($con);
$m_pagos = new m_pagos($con);
$t_parametro = new parametro($con);
$t_bombero = new bombero($con);
$t_usuario = new usuario($con);
$m_cargos = new m_cargos($con);
$errores=array();

$errores=check_errores($t_usuario, $m_cargos);
if(isset($_POST['confirmar'])){
	if($errores==array()){
		try{
		//guardamos
		//$fecha_pago=$_POST['fecha_pago_a'].'-'.$_POST['fecha_pago_m'].'-01';
		$monto=$_POST['monto'];

		$resultado=$m_pagos->guardar_pago($_POST['rut'], $_POST['fechas'], $monto, $_POST['comentario'], $_SESSION['usr']);

		
		$emails=array();
		if(isset($_POST['enviar_mail'])) //si se selecciona enviar email, se agrega el correo al envio
			$emails[] = trim($_POST['email']);
			
		$emails_extra= $t_parametro->obtener_parametro('email_copias_de_pagos');
			
		if(trim($emails_extra)!='')
			$emails[]=$emails_extra;
			
			//echo $emails." \r";
				
		$cuerpo = file_get_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/correo/cuota_pagada.htm');
		$cuerpo = str_replace('[id]', $resultado[0], $cuerpo);
		$cuerpo = str_replace('[rut]', number_format($_POST['rut'], 0, ',', '.').'-'.$t_bombero->obtener_rut_dv($_POST['rut']), $cuerpo);
		$cuerpo = str_replace('[nombre]', $t_bombero->obtener_nombre_completo($_POST['rut']), $cuerpo);
		$cuerpo = str_replace('[monto]', '$'.number_format($monto, 0, ',','.'), $cuerpo);
		$lfechas='<ul>';
		foreach($_POST['fechas'] as $fecha){
			$fecha_separada=preg_split('/\-/', $fecha);
			$fecha_pago=obtener_mes($fecha_separada[1]).' '.$fecha_separada[0];
			$lfechas .= '<li>'.$fecha_pago.'</li>';
		}
		$lfechas.='</ul>';
		$cuerpo = str_replace('[n_cuotas]', $lfechas, $cuerpo);
		$cuerpo = str_replace('[comentario]', $_POST['comentario'], $cuerpo);
		$cuerpo = str_replace('[fecha]', $resultado[1], $cuerpo);
		$cuerpo = str_replace('[usuario]', $_SESSION['usr'], $cuerpo);
			
		//echo $emails.$cuerpo;
		//exit;
		enviar_correos($emails, $cuerpo); //pasamos por parametro el email ingresado, dentro de la funcion se enviaran los demas si estan configurados
		
			
		header('location: ver_pagos.php?rut='.$_POST['rut']);
		//echo $_POST['id_est_bombero'];
		}catch(Exception $e){
			print_r($_POST);
			echo "err:".$e->getMessage();
		}
	}
	else{
		$enlace_volver='ingresar_pago_nuevo.php?rut='.$_POST['rut'];
		include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
	}
}
else {
	header('location: ver_pagos.php?rut='.$_POST['rut']);
}

function check_errores($t_usuario, $m_cargos){
	 //
	 //= new m_cargos($con);
	//$fecha_pago=$_POST['fecha_pago_a'].'-'.$_POST['fecha_pago_m'].'-01';
	$errors=array();
	if(trim($dats['otro_monto'])!=''){
		if(trim($dats['comentario'])=='')
			$errors[]='Si modifica el monto, debe agregar un comentario.';
	}
//	if(trim($dats['direc_'])=='')
//		$errors[]='Campo direccion esta vacio';
	//if(trim($dats['fecha_pago_a'])=='')
	//	$errors[]='Campo año de fecha de pago está vacío';
	//if(trim($dats['fecha_pago_m'])=='')
	//	$errors[]='Campo mes de fecha de pago está vacío';
		
	//$fecha_mayor=$t_pago->es_fecha_mayor($_POST['rut'],$fecha_pago);
	//if($fecha_mayor==FALSE)
		//$errors[]='La fecha a pagar debe ser mayor al último vencimiento.';
		
	if($t_usuario->es_administrador($_SESSION['usr'])==false and $m_cargos->usuario_es_tesorero($_SESSION['usr'])==false)
		$errors[]='Solo pueden guardar pagos un usuario administrador o un usuario con cargo de tesorero';

	return $errors;
}

function enviar_correos($emails, $cuerpo) {

	//require '/usr/share/php/libphp-phpmailer/autoload.php';

	//$emails = ;

	$email_user = CORREO_ENVIO_MAILS;
	$email_password = PASSWORD_ENVIO_MAILS;
	$the_subject = SUBJECT_EMAILS_CUOTAS_PAGADA;
	$address_to = $email_destino;
		
	$from_name = FROM_EMAILS_SISTEMA;
	$phpmailer = new PHPMailer();
	// ---------- datos de la cuenta de Gmail -------------------------------
	$phpmailer->Username = $email_user;
	$phpmailer->Password = $email_password; 
	//-----------------------------------------------------------------------
	// $phpmailer->SMTPDebug = 1;
	$phpmailer->SMTPSecure = 'ssl';
	$phpmailer->Host = "smtp.gmail.com"; // GMail
	$phpmailer->Port = 465;
	$phpmailer->IsSMTP(); // use SMTP
	$phpmailer->SMTPAuth = true;
	$phpmailer->setFrom($phpmailer->Username,$from_name);
	$phpmailer->AddAddress($emails[0]); // recipients email
	if(isset($emails[1])) //si hay otro email, es la copia del pago.
		$phpmailer->AddCC($emails[1]); // recipients email cc
	$phpmailer->Subject = $the_subject;	
	$phpmailer->Body = $cuerpo;
	$phpmailer->IsHTML(true);
	if(!$phpmailer->Send()){
   	echo 'Message could not be sent. '.$address_to.' ';
	   echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
	}
}

function obtener_mes($mes) {
	if ($mes!=''){
		$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
		7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>Mes no encontrado</em>';
	}
}
