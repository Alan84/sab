<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cargo_actual.php');

$errores=check_errores($_POST);

if($errores==array()){
	try{
		//guardamos
		$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$t_bombero = new bombero($con);
		$t_cargo_actual = new cargo_actual($con);

		$fecha_nac=$_POST['fecha_nac'];
		$fecha_ingreso=$_POST['fecha_ingreso'];
		$fecha_ini_cargo='';
		
		//subimos la foto...
		$archivo= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'.jpg'; //.strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
		$archivo_t= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_t.jpg'; //.strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
		$archivo_s= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_s.jpg'; //.strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
		$archivo_m= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_m.jpg'; //.strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));

		if(isset($_POST['borrar_foto'])==TRUE and $_POST['borrar_foto']==1){
			//si selecciona borrar la actual foto, la borramos antes de subir la nueva.
			$foto_bd=$archivo;
			//if($t_bombero->hay_foto($_POST['rut']))
				//$foto_bd=$archivo;
			//se borran las fotos y las redimencionadas
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo))
				unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo);
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_t))
				unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_t);
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_s))
				unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_s);
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_m))
				unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_m);
		}

		//print_r($_FILES);
		if($_FILES['foto']['name']!='' and $_FILES['foto']['type']=='image/jpeg'){ // Se está subiendo foto
			//movemos el archivo subido 
			move_uploaded_file($_FILES['foto']['tmp_name'], DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo);
			
			//Redimensionamos la imagen, para mostrar una version mas liviana y pequeña
			
			// The file
			$filename = DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo;

			// Set a maximum height and width
			$width_t = 100;
			$height_t = 200;
			$width_s = 200;
			$height_s = 400;
			$width_m = 500;
			$height_m = 600;

			// Content type
			//header('Content-Type: image/jpeg'); //no la vamos a mostrar

			// Get new dimensions
			list($width_orig, $height_orig) = getimagesize($filename);

			$ratio_orig = $width_orig/$height_orig;

			if ($width_t/$height_t > $ratio_orig) {
			   $width_t = $height_t*$ratio_orig;
			} else {
			   $height_t = $width_t/$ratio_orig;
			}

			// Resample
			
			$image_p = imagecreatetruecolor($width_t, $height_t);
			$image = imagecreatefromjpeg($filename);
			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_t, $height_t, $width_orig, $height_orig);

			// Output
			// imagejpeg($image_p, null, 100); //no output


			
 
			//$im = $image_p; //@imagecreate(110, 20) or die("Cannot Initialize new GD image stream");
		    //
			//$color_fondo = imagecolorallocate($im, 0, 0, 0);
			//$color_texto = imagecolorallocate($im, 233, 14, 91);
			//imagestring($im, 1, 5, 5,  "A Simple Text String", $color_texto);
			ob_start(); // comienzo a guardar la salida en el buffer
			imagejpeg($image_p);
			imagedestroy($image_p);
 
			$out = ob_get_contents(); // capturo la salida
			ob_end_clean();  // cierro buffer
			file_put_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_t.jpg',$out);  // almaceno
			
			if ($width_s/$height_s > $ratio_orig) {
			   $width_s = $height_s*$ratio_orig;
			} else {
			   $height_s = $width_s/$ratio_orig;
			}
			$image_p = imagecreatetruecolor($width_s, $height_s);
			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_s, $height_s, $width_orig, $height_orig);
			ob_start(); // comienzo a guardar la salida en el buffer
			imagejpeg($image_p);
			imagedestroy($image_p);

			
 
			$out = ob_get_contents(); // capturo la salida
			ob_end_clean();  // cierro buffer
			file_put_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_s.jpg',$out);  // almaceno
			
			if ($width_m/$height_m > $ratio_orig) {
			   $width_m = $height_m*$ratio_orig;
			} else {
			   $height_m = $width_m/$ratio_orig;
			}
			$image_p = imagecreatetruecolor($width_m, $height_m);
			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width_m, $height_m, $width_orig, $height_orig); 
			ob_start(); // comienzo a guardar la salida en el buffer
			imagejpeg($image_p);
			imagedestroy($image_p);
 
			$out = ob_get_contents(); // capturo la salida
			ob_end_clean();  // cierro buffer
			file_put_contents(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_m.jpg',$out);  // almaceno
			
		}
		
		
		if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo)){
			$archivo='foto_'.$_POST['rut'].'.jpg';
			$hay_foto='1';
			//$foto_bd='foto_'.$_POST['rut'].'.jpg';
		}
		else { //if(is_file(DIRECTORIO_WEB.'/'.DIRECTORIO_FOTO.'/'.$foto_bd))
			$archivo='';
			$hay_foto='';
		}

		$t_bombero->actualizar_bombero($_POST['rut'], $_POST['rut_dv'], $_POST['nombre'], 
			$_POST['apellido'], $_POST['profesion'], $_POST['sexo'], $_POST['id_est_civil'],
			$_POST['grup_san'], $_POST['direc_calle'], $_POST['direc_numero'],
			$_POST['direc_comuna'], $_POST['direc_region'], $_POST['direc_depto'], 
			$_POST['email'], $_POST['email2'], $_POST['fono'], $_POST['fono2'],
			$_POST['fecha_nac'], $_POST['fecha_ingreso'], $_POST['id_est_bombero'], $_POST['num_registro'], $_POST['num_tib'],
			$_POST['num_placa'], $_POST['nombre_padre'], $_POST['nombre_madre'], $_POST['comentarios'],
			 $hay_foto, $_SESSION['usr']);

		/*
		if($t_cargo_actual->existe_cargo($_POST['id_cod_cargo'])==TRUE)
			$t_cargo_actual->actualizar_cargo($_POST['rut'], $_POST['id_cod_cargo'], $fecha_ini_cargo);
		else 
			$t_cargo_actual->guardar_cargo($_POST['rut'], $_POST['id_cod_cargo'], $fecha_ini_cargo);
		*/
		
			header('location: ver_bombero.php?rut='.$_POST['rut']);
		}catch(Exception $e){
		//print_r($_POST);
		echo "err:".$e->getMessage();
	}
}
else{
	$enlace_volver='editar_bombero.php?rut='.$_POST['rut'];
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}


function check_errores($dats){

	$errors=array();
	if(trim($dats['nombre'])=='')
		$errors[]='Campo nombre está vacío';
	if(trim($dats['apellido'])=='')
		$errors[]='Campo apellido está vacío';
	if(trim($dats['rut'])==''){
		$errors[]='Campo Rut está vacío';
	}
	else{
		$dv=obtener_digito_verificador($dats['rut']);
		$dats['rut_dv']=strtoupper($dats['rut_dv']);
		if($dv!=$dats['rut_dv'])
			$errors[]='Digito verificador del Rut incorrecto';
	}
	//if(trim($dats['profesion'])=='')
		//$errors[]='Campo profesión vacío';

	//if(trim($dats['id_est_civil'])=='')
		//$errors[]='Estado civil no seleccionado';
	
//	else{
//	if(verificar_estadocivil()==false){
//			$errors[]='El estado civil no existe en la base de datos';
//		}
//	}
	if(trim($dats['id_est_bombero'])==''){
		$errors[]='Estado bombero no seleccionado';
	}
	
	if($_FILES['foto']['name']!='' and $_FILES['foto']['type']!='image/jpeg')
		$errors[]='Solo se adminten imagenes en formato jpeg';

//	if(trim($dats['direc_'])=='')
//		$errors[]='Campo direccion esta vacio';
	if(trim($dats['fecha_nac'])=='')
		$errors[]='Campo fecha de nacimiento está vacío';
	if(trim($dats['fecha_ingreso'])=='')
		$errors[]='Campo fecha de ingreso está vacío';
		
	if(trim($dats['fecha_nac'])!='' and trim($dats['fecha_ingreso'])!='')
		if($dats['fecha_nac'] > $dats['fecha_ingreso'])
			$errors[]='Campo <em>fecha de nacimiento</em> es mayor que <em>fecha de ingreso</em>';
		
	// if(trim($dats['fecha_nac_m'])=='')
		// $errors[]='Campo mes fecha de nacimiento esta vacío';
	// if(trim($dats['fecha_nac_a'])=='')
		// $errors[]='Campo year fecha de nacimiento esta vacío';

	return $errors;
}

function obtener_digito_verificador($rut){
	$d=false;
	$rut = strrev($rut);
	$aux = 1;
	$s = 0;
	for($i=0;$i<strlen($rut);$i++){
		$aux++;
		$s += intval($rut[$i])*$aux;
		if($aux == 7){ $aux=1; }
	}
	$digit = 11-$s%11;
	if($digit == 11){$d = 0;}
	elseif($digit == 10){$d = "K";}
	else{$d = $digit;}
	return $d;
}