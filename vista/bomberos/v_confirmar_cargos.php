<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Cargos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T22:39:27-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar cargos nuevos</h1>

<form method="post" action="guardar_cargos.php">
	<p>Por favor, confirme las acciones. Los cargos actuales irán al histórico de cargos.</p>
	<table>
		<tr>
			<th>cargo</th>
			<th>bombero actual</th>
			<th>acción</th>
		</tr>
		<?php foreach ($cambios as $cambiar){ ?>
		<tr>
			<td><?php echo $cambiar[0];?></td>
			<td><?php echo $cambiar[1];?></td>
			<td><?php echo $cambiar[2]; ?></td>
		</tr>
		<?php } ?>
	</table>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	<input type="hidden" name="monto" value="<?php echo $monto; ?>" />
	<p style="text-align: right"><input type="submit" name="confirmar" value="Confirmar" /></p>
</form>
<form method="post" action="cargos.php">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<p style="text-align: right"><input type="submit" name="cancelar" value="Cancelar" /></p>
</form>
</div>
</div>
</body>
</html>
