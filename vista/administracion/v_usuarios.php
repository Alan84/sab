<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Usuarios') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T21:26:10-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> 	<a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>



</div>


<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">

<h1 style="color: white; background-color: black">Usuarios</h1>
<div style="float: right"><p><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/agregar_usuario.php">Agregar</a></p></div>
	<ul>
	<?php foreach($usuarios as $usuario){ ?>
		<li>
			<a href="modificar_usuario.php?uname=<?php echo $usuario['usuario']; ?>"><?php echo $usuario['usuario']; ?></a>
		 - <?php echo $t_tipo_usuario->obtener_descripcion($usuario['tipo']); ?>
		 <a href="eliminar_usuario.php?username=<?php echo $usuario['usuario']; ?>" style="color: red; text-decoration: none" title="Eliminar"></a>
		 
		 </li>
	<?php } ?>
	</ul>
</div>
</body>
</html>