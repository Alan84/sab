<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_pagos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_reportes.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$m_reportes = new m_reportes($con);
$m_pagos = new m_pagos($con);
$m_cargos = new m_cargos($con);
$t_bombero = new bombero($con);
$t_usuario = new usuario($con);
$t_pago = new pago($con);

$errores=check_errores($t_usuario, $m_cargos);
$borrar=array();
$cambios=array();
$meses=array(''=>'', '01'=> 'Enero', '02'=> 'Febrero', '03'=>'Marzo', '04'=> 'Abril', '05'=> 'Mayo', '06'=> 'Junio',
	'07'=> 'Julio' , '08'=> 'Agosto', '09'=> 'Septiembre', '10'=> 'Octubre', '11'=> 'Noviembre', '12'=> 'Diciembre');
//$rut=$_POST['rut'];

if(isset($_POST['cancelar'])){
	header('location: pagos.php');
}
else{
	if($errores==array()){
		if(isset($_POST['confirmar'])){ //se hace el borrado
			foreach($_POST['ids'] as $id_borrar => $rut){
				$t_pago->set_borrado($rut, $id_borrar, '1'); // 1 = borrado lógico
			}
		
			header('location: pagos.php');
		} 
		else{
			$i=0;
			foreach($_POST['ids'] as $id_borrar => $rut){
				$bombero=$t_bombero->obtener_bombero($rut);
				$pagos[$i]['id'] = $id_borrar;
				$pagos[$i]['rut'] = $rut;
				$pagos[$i]['bombero_datos_basicos'] = $bombero['nombre'].' '.$bombero['apellido'].' '.number_format($bombero['rut'], 0, ',','.').'-'.$bombero['rut_dv'];
				$pagos[$i]['meses'] = $m_pagos->obtener_meses($id_borrar);
				$i++;
			}
				include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_borrar_pagos.php');
			}
	}
	else{
		$enlace_volver = DIRECTORIO_WEB_SISTEMA.'/sistema/administracion/pagos.php';
		include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php');
	}
}


//if($cambios==array())
//	$errores[]='No se han seleccionado cambios.';
	
	

//$estados=$t_est_bombero->obtener_estados_bomberos();
//$cargos=$t_cargo->obtener_cargos();
//$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','-- seleccionar --')));




function check_errores($t_usuario, $m_cargos){
	$errores=array();
	//print_r($_POST);
	if(!isset($_POST['ids']))
		$errores[]='No realizó ninguna seleccion.';
	if($t_usuario->es_administrador($_SESSION['usr'])==false and $m_cargos->usuario_es_tesorero($_SESSION['usr'])==false)
		$errores[]='Solo puede borrar un usuario administrador o un usuario con cargo de tesorero';
	
	return $errores;
}

function obtener_mes($mes) {
	if ($mes!=''){
		$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
		7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>sin informacion</em>';
	}
}