<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_llamado.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_cod_region = new cod_region($con);
$t_bombero = new bombero($con);
$t_parametro = new parametro($con);
$t_tipo_llamado = new tipo_llamado($con);

$tipos_llamado = $t_tipo_llamado->obtener_llamados();
$bomberos = $t_bombero->obtener_checkbox_bomberos(array(1));
$bomberos_cbo_acargo = $t_bombero->obtener_combo_bomberos(array(1), array(array('', '-- seleccionar --')));
$form['fecha_ini'] = date('Y-m-d');
$form['hora_ini'] = '';
$form['fecha_fin'] = date('Y-m-d');
$form['hora_fin'] = '';
$form['dir_region'] = $t_parametro->obtener_parametro('region_por_defecto');
$form['dir_comuna'] = $t_parametro->obtener_parametro('comuna_por_defecto');
$form['dir_calle'] = '';
$form['dir_numero'] = '';
$form['dir_calle2'] = '';
$form['observaciones'] = '';
$form['rut'] = '';
$form['rut_acargo'] = '';
$form['rut_dv'] = '';

foreach($_POST as $indice => $dato){ //recibiendo datos post
	$form[$indice] = $dato;
}


//print_r($form);
// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$regiones = $t_cod_region->obtener_regiones();

include('../../vista/llamados/v_agregar_llamado.php');