<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Enviar emails') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T21:00:40-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar Cambios</h1>
<form method="post" action="confirmar_enviar_emails.php">
	<table>
		<tr>
			<td><p>Confirme el envío masivo de emails</p>
				<p>El servidor usado para enviar emails tiene una limitación de 100 emails por día, si se excede este límite, 
				el servidor bloqueará los envíos por 24hrs.
				Para evitar esto <strong>no envíe demasiados emails</strong>, solo envíe a los que tengan más prioridad, 
				tenga en cuenta que al pagar también se usa el servidor de emails para enviar el comprobante, 
				por lo que se hace necesario que no exista limitación para esa tarea.</p>
				<p>Se enviarán emails a los siguientes bomberos:</p>
				<ol style="font-weight: bold;">
					<?php foreach($bomberos as $bom){ ?>
						<li><?php echo '['.number_format($bom['rut'], 0, ',','.').'-'.$bom['rut_dv'].'] '.$bom['nombre'].' '.$bom['apellido'].'</span> - '.$bom['email'].''; ?></li>
					<?php } ?>
				</ol>
				<p><em>No se enviarán emails a bomberos que no tengan registrado uno. Si el bombero no registra email, actualícelo en editar datos personales</em></p>
				<p style="margin-top: 2em;"><strong style="font-size: 15pt">Por ejemplo se enviará al primer bombero el siguiente formato:</strong></p>
				<div style="background-color: #fafafa; padding: 1em; border: 2px solid #aaa; margin: 1em 5em"><?php echo $cuerpo; ?></div>
			</td>
		</tr>
	</table>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	
	<p style="text-align: right"><input type="submit" name="confirmar" value="Confirmar" onclick="desabilitar(this)" /></p>
</form>
<form method="post" action="bomberos_morosos.php">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<p style="text-align: right"><input type="submit" name="cancelar" value="Cancelar" /></p>
</form>
</div>
</div>
</body>
</html>