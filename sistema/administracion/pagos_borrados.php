<?php

session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_pagos.php');
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_ver_pagos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
//$rut='';
//if(isset($_GET['rut']) and is_numeric($_GET['rut']))
//	$rut=$_GET['rut'];

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_est_bombero = new est_bombero($con);
$m_pagos = new m_pagos($con);
//$m_ver_pagos = new ver_pagos($con);
$t_bombero = new bombero($con);
$t_parametro = new parametro($con);

$pagina= 1;
if(isset($_GET['pagina']))
	$pagina=$_GET['pagina'];

$total_registros=$m_pagos->obtener_total_registros_borrados();
/*
$fecha_ultimo=$m_ver_pagos->obtener_ultima_fecha_vencimiento($rut);
$bombero=$t_bombero->obtener_bombero($rut);
$valor_cuota=$t_parametro->obtener_parametro('valor_cuota');
*/
//seteamos el email de notificacion por defecto
/**
if($bombero['email']!='')
	$form['email'] = $bombero['email']; //.'@'.$t_parametro->obtener_parametro('dominio_email_interno');
else
	$form['email'] = $bombero['email2'];
**/
// seteamos los meses para usarlos en el combo

$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$fecha_pago_m = date('n'); //mes sin cero a la izquierda
$fecha_pago_a_orig = $fecha_pago_a = date('Y');

if (isset($fecha_ultimo[0])){
	$fecha_pago_m = $fecha_ultimo[0]['fecha_siguiente_m'];
	$fecha_pago_a_orig = $fecha_pago_a = $fecha_ultimo[0]['fecha_siguiente_a'];
}

if(isset($_POST['fechas'])){
	foreach($_POST['fechas'] as $indice=>$fecha){
		$check_fechas[$indice]= ' checked="checked"';
	}

	$fecha_fin= preg_split('/\-/', $fecha);
	$fecha_fin_a=$fecha_fin[0];
//	print_r($fecha_fin);

	$x=0;
	while($fecha_pago_a<=$fecha_fin_a){
		$fechas_disponibles[$x]['m'] = $fecha_pago_m;
		$fechas_disponibles[$x]['a'] = $fecha_pago_a;
		if(!isset($check_fechas[$x]))//si no está seteada la variable, no ha sido devuelta por el formulario, la seteamos a vacío para evitar errores
			$check_fechas[$x]= '';
		if($fecha_pago_m>=12){
			$fecha_pago_m=1;
			$fecha_pago_a++;
		}
		else{
			$fecha_pago_m++;
		}
		$x++;
	}
}
else{
	$x=0;
	while($fecha_pago_a<=$fecha_pago_a_orig){
		$fechas_disponibles[$x]['m'] = $fecha_pago_m;
		$fechas_disponibles[$x]['a'] = $fecha_pago_a;
		$check_fechas[$x]= '';
		if($fecha_pago_m>=12){
			$fecha_pago_m=1;
			$fecha_pago_a++;
		}
		else{
			$fecha_pago_m++;
		}
		$x++;
	}
}

$check_enviar_mail = ' checked="checked"';
$form['otro_monto']='';
$form['comentario']='';

/*
$sql="select * from est_bombero";
foreach($con->query($sql) as $row)
	$estados[$row['id_est_bombero']]=$row['descripcion'];
*/

$filtro_buscar='';
$filtro_estados=array(1,2,3,4,5); //el estado uno es el por defecto
//obteniendo las variables de busqueda
$filtro_sql='';

//ordenado por defecto por fecha de actualizacion, la mas nueva primero
$orden_sql=" order by b.fecha_actualiza desc ";
$orden_link_fecha_ingreso=$orden_link_pago_mes=$orden_link_monto=$orden_link_comentario=$orden_link_usuario=$orden_link_fecha_mail_alerta='&tipo_orden=asc';
$simbolo_orden_fecha_ingreso=$simbolo_orden_pago_mes=$simbolo_orden_monto=$simbolo_orden_comentario=$simbolo_orden_usuario=$simbolo_orden_fecha_mail_alerta='';
$link_busqueda='';
$orden='p.rut, p.id_pago'; // ';
$tipo_orden='desc'; //';
if($_GET!==array()){
	if(isset($_GET['buscar']))
		$filtro_buscar=trim($_GET['buscar']);
	$filtro_sql='';
	if($filtro_buscar!=''){
		$filtro_sql.=" and (b.nombre like '%$filtro_buscar%' or b.apellido like '%$filtro_buscar%' or b.rut='$filtro_buscar') ";
		$link_busqueda.='&buscar='.urlencode($filtro_buscar);
	}


	$estados_sql='';
	$hay_filtro_estado=0;
	/*
	foreach($estados as $id=>$est)
		if(isset($_GET['estado'.$id]) and $_GET['estado'.$id]=='s'){
			if($hay_filtro_estado==0)
				$filtro_estados=array();
			if($estados_sql!='')
				$estados_sql.=', ';
			$filtro_estados[]=$id;
			$estados_sql.=$id;
			$hay_filtro_estado=1;
			$link_busqueda.='&estado'.$id.'=s';
		}
		*/
	if($estados_sql!='')
		$filtro_sql.=" and b.id_est_bombero in ($estados_sql) ";

	
	
	//verificar orden
	if(isset($_GET['orden'])){
		switch($_GET['orden']){
			case 'fecha_ingreso':
				$orden='fecha_ingreso';
				$simbolo_orden_fecha_ingreso=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_fecha_ingreso='&tipo_orden=desc';
					$simbolo_orden_fecha_ingreso=' ▲';
				}
			break;
			case 'pago_mes':
				$orden='pago_mes';
				$simbolo_orden_pago_mes=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_pago_mes='&tipo_orden=desc';
					$simbolo_orden_pago_mes=' ▲';
				}
			break;
			case 'monto':
				$orden='monto';
				$simbolo_orden_monto=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_monto='&tipo_orden=desc';
					$simbolo_orden_monto=' ▲';
				}
			break;
			case 'comentario':
				$orden='comentario';
				$simbolo_orden_comentario=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_comentario='&tipo_orden=desc';
					$simbolo_orden_comentario=' ▲';
				}
			break;
			case 'fecha_mail_alerta':
				$orden='fecha_mail_alerta';
				$simbolo_orden_fecha_mail_alerta=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_fecha_mail_alerta='&tipo_orden=desc';
					$simbolo_orden_fecha_mail_alerta=' ▲';
				}
			break;
			case 'usuario':
				$orden='usuario';
				$simbolo_orden_usuario=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_usuario='&tipo_orden=desc';
					$simbolo_orden_usuario=' ▲';
				}
			break;

		}
		$orden_sql=" order by $orden $tipo_orden ";
	}
}

$registros_por_pagina=REGISTROS_POR_PAGINA;
if($pagina==0)
	$registros_por_pagina=0; //mostramos todos
$pagos=$m_pagos->obtener_pagos_borradas($orden, $tipo_orden, $pagina-1, $registros_por_pagina);
//obtenemos la lista de bomberos a consultar
/** $sql="select b.rut, b.rut_dv, b.nombre, b.apellido, b.id_est_bombero, estb.descripcion as est_bombero, b.fecha_actualiza,
		(year(now())-year(fecha_nac)) as edad,
		(select month(fecha_vence)
		from pago p where p.rut = b.rut order by id_pago desc limit 0,1) as fecha_vence_mes, 
		(select year(fecha_vence)
		from pago p where p.rut = b.rut order by id_pago desc limit 0,1) as fecha_vence_anio
		 from bombero as b, est_bombero as estb
		where b.id_est_bombero=estb.id_est_bombero
		 $filtro_sql
		 $orden_sql"; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla


$query=$con->query($sql);
$result=array();
if($query!==false)
	$result=$query->fetchAll(PDO::FETCH_ASSOC);
**/
//obtenemos los valores devueltos por post
foreach($_POST as $indice=> $dato){
	$form[$indice]=$dato;

}

if(count($_POST)>0){
	if(isset($_POST['enviar_mail']))
		$check_enviar_mail = ' checked="checked"';
	else
		$check_enviar_mail = ' ';
}

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_pagos_borrados.php');

function obtener_mes($mes) {
	if ($mes!=''){
		$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
		7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');
		return $meses[$mes];
	}
	else{
		return '<em>sin informacion</em>';
	}
}