<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_civil.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_anotacion.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_anio.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_curso.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_cargo.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_grupo_sangre.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$rut='';
if(isset($_GET['rut']) and is_numeric($_GET['rut']))
        $rut=$_GET['rut'];

$t_est_civil = new est_civil($con);
//$t_cod_anio = new cod_anio($con);
$t_est_bombero = new est_bombero($con);
$t_tipo_curso = new tipo_curso($con);
$t_tipo_anotacion = new tipo_anotacion($con);
$m_bomberos= new m_bomberos($con);
$t_tipo_cargo= new tipo_cargo($con);
$t_cod_region= new cod_region($con);
// $t_cod_grupo_sangre= new cod_grupo_sangre($con);
//$t_tipo_cargo = new tipo_cargo($con);

// obtenemos los estados civiles desde la base de datos para usarlos en el combo
$estados_civil = $t_est_civil->obtener_estados_civiles();

// obtenemos los estados civiles desde la base de datos para usarlos en el combo
//$anios_servicio = $t_cod_anio->obtener_anios_servicios(); ya no se usa

// obtenemos los estados bomberos desde la base de datos para usarlos en el combo
$estados_bombero = $t_est_bombero->obtener_estados_bomberos();

// obtenemos los grupos de sangre desde la base de datos para usarlos en el combo
// $grupos_sangre = $t_cod_grupo_sangre->obtener_grupos_sangre();

// obtenemos los tipos de cursos desde la base de datos para usarlos en el combo
$cursos = $t_tipo_curso->obtener_tipos_cursos();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$tipo_anotaciones = $t_tipo_anotacion->obtener_tipos_anotaciones();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$regiones = $t_cod_region->obtener_regiones();


$tip_cargos = $t_tipo_cargo->obtener_tipos_cargos();

// obtenemos los tipos de cargo desde la base de datos para usarlos en el combo
//$tipo_cargos = $t_tipo_cargo->obtener_tipos_cargos();

//$cantidad_anotaciones=20;

// seteamos los meses para usarlos en el combo
$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$bombero=$m_bomberos->obtener_bombero($rut);

$rut_dv=$direc_calle=$direc_numero=$direc_depto=$direc_region=$direc_comuna='';
$nombre=$apellido=$nombre_padre=$nombre_madre=$tel_movil=$tel_fijo='';
$fecha_ingreso_d=$fecha_ingreso_m=$fecha_ingreso_a=$fecha_nac_d=$fecha_nac_m=$fecha_nac_a='';
$profesion=$grup_san=$email=$email2=$sexo=$sel_sexo_m=$sel_sexo_f='';
$id_est_civil=$id_est_bombero=$num_registro=$num_tib=$num_placa='';
$foto = '';
/*
if ($bombero['fecha_ini_cargo_d']=='')
	$bombero['fecha_ini_cargo_d']=date('d');
if ($bombero['fecha_ini_cargo_m']=='')
	$bombero['fecha_ini_cargo_m']=date('m');

if ($bombero['fecha_ini_cargo_a']=='')
	$bombero['fecha_ini_cargo_a']=date('Y');
*/

foreach($_POST as $indice => $dato){
		$bombero[$indice]= $dato;
}



if($bombero['foto']=='1')
	$foto = 'foto_'.$rut.'.jpg';

//print_r($estados_civil);

if($bombero['sexo']=='M') 
	$sel_sexo_m='checked="checked"';
else if($bombero['sexo']=='F') 
	$sel_sexo_f='checked="checked"';

//print_r($tipo_anotaciones);

//list($fnac_a, $fnac_m, $fnac_d) = explode('-', $bombero['fecha_nac']);

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_editar_bombero.php');