<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$t_usuario = new usuario($con);
$t_bombero = new bombero($con);

$errores=check_errores($t_usuario);
$borrar=array();

$rut=$_POST['rut'];




//if($cambios==array())
//	$errores[]='No se han seleccionado cambios.';
	
	

//$estados=$t_est_bombero->obtener_estados_bomberos();
//$cargos=$t_cargo->obtener_cargos();
//$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','-- seleccionar --')));
if ($errores==array()){
	if(isset($_POST['confirmar']) and $_POST['confirmar']!=''){
		$t_bombero->set_borrado($rut, '1'); //borrado lógico
		//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_confirmar_eliminar_bombero.php');
	}
	header('location: consulta_bomberos.php');
}
else{
	$enlace_volver = DIRECTORIO_WEB_SISTEMA.'/sistema/bomberos/consulta_bomberos.php';
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}



function check_errores($t_usuario){
	
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='Solo puede borrar un usuario administrador';
	
	return $errores;
}