<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_llamados.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$m_llamados = new m_llamados($con);
$t_cod_region = new cod_region($con);

// $estados=$t_est_bombero->obtener_estados_bomberos();
/*
$sql="select * from est_bombero";
foreach($con->query($sql) as $row)
	$estados[$row['id_est_bombero']]=$row['descripcion'];
*/

$filtro_buscar='';
$filtro_estados=array(1,2,3,4,5); //el estado uno es el por defecto
//obteniendo las variables de busqueda
$filtro_sql='and borrado=1';
//ordenado por defecto por fecha de actualizacion, la mas nueva primero
$orden_sql=" ll.id_llamado desc ";
$orden_link_id=$orden_link_tipo=$orden_link_rut=$orden_link_fecha=$orden_link_horas=$orden_link_dir=$orden_link_asisten=$orden_link_user='&tipo_orden=asc';
$simbolo_orden_id=$simbolo_orden_tipo=$simbolo_orden_rut=$simbolo_orden_fecha=$simbolo_orden_horas=$simbolo_orden_dir=$simbolo_orden_asisten=$simbolo_orden_user='';
$link_busqueda='';

if($_GET!==array()){
	if(isset($_GET['buscar']))
		$filtro_buscar=trim($_GET['buscar']);
	$filtro_sql='';
	if($filtro_buscar!=''){
		$filtro_buscar=strtoupper($filtro_buscar);
		$filtro_sql.=" and (UPPER(b.nombre) like '%$filtro_buscar%' or UPPER(b.apellido) like '%$filtro_buscar%' or b.rut like '$filtro_buscar') ";
		$link_busqueda.='&buscar='.urlencode($filtro_buscar);
	}


	$estados_sql='';
/*
	$hay_filtro_estado=0;
	
	foreach($estados as $id=>$est)
		if(isset($_GET['estado'.$id]) and $_GET['estado'.$id]=='s'){
			if($hay_filtro_estado==0)
				$filtro_estados=array();
			if($estados_sql!='')
				$estados_sql.=', ';
			$filtro_estados[]=$id;
			$estados_sql.=$id;
			$hay_filtro_estado=1;
			$link_busqueda.='&estado'.$id.'=s';
		}
	if($estados_sql!='')
		$filtro_sql.=" and b.id_est_bombero in ($estados_sql) ";
*/
	//verificar orden
	if(isset($_GET['orden'])){
		$orden='ll.id_llamado';
		$tipo_orden='desc';
		switch($_GET['orden']){
			case 'id':
				$orden='ll.id_llamado';
				$simbolo_orden_id=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_id='&tipo_orden=desc';
					$simbolo_orden_id=' ▲';
				}
			break;
			case 'tipo_llamado':
				$orden='ll.cod_tipo_llamado';
				$simbolo_orden_tipo=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_tipo='&tipo_orden=desc';
					$simbolo_orden_tipo=' ▲';
				}
			break;
			case 'rut':
				$orden='ll.rut_acargo';
				$simbolo_orden_rut=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_rut='&tipo_orden=desc';
					$simbolo_orden_rut=' ▲';
				}
			break;
			case 'fecha':
				$orden='ll.fecha_ini';
				$simbolo_orden_fecha=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_fecha='&tipo_orden=desc';
					$simbolo_orden_fecha=' ▲';
				}
			break;
			case 'horas':
				$orden='horas';
				$simbolo_orden_horas=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_horas='&tipo_orden=desc';
					$simbolo_orden_horas=' ▲';
				}
			break;
			case 'dir':
				$orden='ll.dir_region, ll.dir_comuna, ll.dir_calle';
				$simbolo_orden_dir=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_dir='&tipo_orden=desc';
					$simbolo_orden_dir=' ▲';
				}
			break;
			case 'asisten':
				$orden='asistentes';
				$simbolo_orden_asisten=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_asisten='&tipo_orden=desc';
					$simbolo_orden_asisten=' ▲';
				}
			break;
			case 'user':
				$orden='ll.usuario';
				$simbolo_orden_user=' ▼';
				if(isset($_GET['tipo_orden']) and $_GET['tipo_orden']=='asc'){
					$tipo_orden='asc';
					$orden_link_user='&tipo_orden=desc';
					$simbolo_orden_user=' ▲';
				}
			break;

		}
		$orden_sql=" $orden $tipo_orden ";
	}
}

$llamados=$m_llamados->obtener_llamados_borrados('', $orden_sql);

//$est_bombero=new tabla($con, 'est_bombero', 'descripcion', 'id_est_bombero');



include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_llamados_borrados.php');

function mostrar_num_romanos($num){
	$numeros=array('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIX','XX','XXI','XXII','XXIII','XXIV','XXV');
	return $numeros[$num-1];
}