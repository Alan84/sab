-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 22-08-2019 a las 00:53:50
-- Versión del servidor: 10.3.15-MariaDB-1
-- Versión de PHP: 7.3.4-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quinta5`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anotacion`
--

CREATE TABLE `anotacion` (
  `id_anotacion` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `id_tipo_anotacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `anotacion`
--

INSERT INTO `anotacion` (`id_anotacion`, `descripcion`, `anio`, `rut`, `id_tipo_anotacion`) VALUES
(1, 'ok', 2001, 15510104, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bombero`
--

CREATE TABLE `bombero` (
  `nombre` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `rut_dv` int(11) DEFAULT NULL,
  `direc_calle` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `direc_numero` int(11) DEFAULT NULL,
  `direc_comuna` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `direc_region` int(10) DEFAULT NULL,
  `tel_movil` int(11) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `num_registro` int(11) DEFAULT NULL,
  `num_tib` int(11) DEFAULT NULL,
  `num_placa` int(11) DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `profesion` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `grup_san` char(6) COLLATE utf8_bin NOT NULL,
  `email` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `email2` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `tel_fijo` int(11) DEFAULT NULL,
  `apellido` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `fecha_actualiza` datetime DEFAULT NULL,
  `direc_depto` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `nombre_padre` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `nombre_madre` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `sexo` char(1) COLLATE utf8_bin DEFAULT NULL,
  `foto` char(20) COLLATE utf8_bin DEFAULT NULL,
  `id_est_civil` int(11) NOT NULL,
  `id_est_bombero` int(11) NOT NULL,
  `usuario_actualiza` varchar(16) COLLATE utf8_bin NOT NULL,
  `id_cod_cargo` int(11) DEFAULT NULL,
  `comentarios` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `bombero`
--

INSERT INTO `bombero` (`nombre`, `rut_dv`, `direc_calle`, `direc_numero`, `direc_comuna`, `direc_region`, `tel_movil`, `fecha_ingreso`, `fecha_nac`, `num_registro`, `num_tib`, `num_placa`, `rut`, `profesion`, `grup_san`, `email`, `email2`, `tel_fijo`, `apellido`, `fecha_actualiza`, `direc_depto`, `nombre_padre`, `nombre_madre`, `sexo`, `foto`, `id_est_civil`, `id_est_bombero`, `usuario_actualiza`, `id_cod_cargo`, `comentarios`) VALUES
('JUAN ENRIQUE', 9, 'Pablo Burchard', 4801, 'SAN BERNARDO', 13, 228572402, '1980-08-05', '1954-08-08', 0, 0, 0, 7031254, 'Empresario', 'A2 RH+', '', '', 91657449, 'ORTEGA VALDES', '2014-12-12 09:10:12', '', 'Guillermo', 'Ema', 'm', '', 1, 1, 'admin', NULL, NULL),
('PATRICIO ANTONIO', 0, 'PABLO BURCHARD', 4801, 'SAN BERNARDO', 13, 0, '2019-05-17', '1982-01-13', 0, 0, 0, 15510104, 'CHOFER', 'A2RH+', 'PORTEGA@QUINTINOS.CL', 'GUESERO@HOTMAIL.COM', 0, 'ORTEGA VELASQUEZ', '2019-07-06 23:25:08', '', 'JUAN', 'BERTA', 'm', 'foto_15510104.jpg', 2, 1, 'admin', NULL, 'comentarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo_actual`
--

CREATE TABLE `cargo_actual` (
  `id_cargo_actual` int(11) NOT NULL,
  `rut` int(11) DEFAULT 0,
  `fecha_inicio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cargo_actual`
--

INSERT INTO `cargo_actual` (`id_cargo_actual`, `rut`, `fecha_inicio`) VALUES
(1, 7031254, '2014-12-12'),
(2, 15510104, '2019-05-17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo_hst`
--

CREATE TABLE `cargo_hst` (
  `id_cargo` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `anio` date DEFAULT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_termino` date NOT NULL,
  `id_cod_cargo` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cargo_hst`
--

INSERT INTO `cargo_hst` (`id_cargo`, `rut`, `anio`, `fecha_inicio`, `fecha_termino`, `id_cod_cargo`, `descripcion`) VALUES
(1, 15510104, NULL, '2019-05-29', '2019-06-06', 6, 'okaa\r\n\r\noka...\r\n....'),
(2, 15510104, NULL, '2019-05-27', '2019-06-05', 3, 'todo.\r\n\r\n...bien.'),
(3, 15510104, NULL, '2019-06-05', '2019-06-20', 2, '...............'),
(4, 15510104, NULL, '2019-06-20', '2019-06-26', 5, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cod_anio`
--

CREATE TABLE `cod_anio` (
  `cant_anios` int(11) DEFAULT NULL,
  `id_cod_anio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cod_anio`
--

INSERT INTO `cod_anio` (`cant_anios`, `id_cod_anio`) VALUES
(5, 1),
(10, 2),
(15, 3),
(20, 4),
(25, 5),
(30, 6),
(35, 7),
(40, 8),
(45, 9),
(50, 10),
(55, 11),
(60, 12),
(65, 13),
(70, 14),
(75, 15),
(80, 16),
(85, 17),
(90, 18),
(95, 19),
(100, 20),
(105, 21),
(110, 22),
(115, 23),
(120, 24),
(125, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cod_cargo`
--

CREATE TABLE `cod_cargo` (
  `id_cod_cargo` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cod_cargo`
--

INSERT INTO `cod_cargo` (`id_cod_cargo`, `descripcion`) VALUES
(1, 'DIRECTOR'),
(2, 'CAPITAN'),
(3, 'TENIENTE'),
(4, 'TENIENTE 2do'),
(5, 'TENIENTE 3ro'),
(6, 'TESORERO'),
(7, 'CONSEJERO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cod_region`
--

CREATE TABLE `cod_region` (
  `id_cod_region` int(11) NOT NULL,
  `descripcion` varchar(25) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cod_region`
--

INSERT INTO `cod_region` (`id_cod_region`, `descripcion`) VALUES
(1, 'Tarapaca'),
(2, 'Antofagasta'),
(3, 'Atacama'),
(4, 'Coquimbo'),
(5, 'Valparaiso'),
(6, 'O\'Higgins'),
(7, 'Maule'),
(8, 'Biobio'),
(9, 'La araucania'),
(10, 'Los lagos'),
(11, 'Aysen'),
(12, 'Magallanes'),
(13, 'Region metropolitana'),
(14, 'Los rios'),
(15, 'Arica y parinacota');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  `anio` smallint(6) DEFAULT NULL,
  `id_curso` int(11) NOT NULL,
  `certificado` tinytext COLLATE utf8_bin DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `id_tipo_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`descripcion`, `anio`, `id_curso`, `certificado`, `rut`, `id_tipo_curso`) VALUES
('uih', 2002, 1, '', 15510104, 2),
('ccc', 1933, 2, 'certificado_15510104_2.jpg', 15510104, 1),
('xxx', 1900, 3, '', 15510104, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est_bombero`
--

CREATE TABLE `est_bombero` (
  `id_est_bombero` int(11) NOT NULL,
  `descripcion` varchar(32) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `est_bombero`
--

INSERT INTO `est_bombero` (`id_est_bombero`, `descripcion`) VALUES
(1, 'activo'),
(2, 'suspendido'),
(3, 'retirado'),
(4, 'con licencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `est_civil`
--

CREATE TABLE `est_civil` (
  `id_est_civil` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `est_civil`
--

INSERT INTO `est_civil` (`id_est_civil`, `descripcion`) VALUES
(1, 'casado'),
(2, 'soltero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llamado`
--

CREATE TABLE `llamado` (
  `id_llamado` int(11) NOT NULL,
  `cod_tipo_llamado` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `direccion` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fue_acargo` tinyint(1) DEFAULT NULL,
  `rut_acargo` int(11) NOT NULL,
  `fecha_ini` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `dir_region` int(11) NOT NULL,
  `dir_comuna` varchar(50) COLLATE utf8_bin NOT NULL,
  `dir_calle` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `dir_numero` int(11) DEFAULT NULL,
  `dir_calle2` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `usuario` varchar(16) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `llamado`
--

INSERT INTO `llamado` (`id_llamado`, `cod_tipo_llamado`, `direccion`, `observaciones`, `fecha`, `fue_acargo`, `rut_acargo`, `fecha_ini`, `fecha_fin`, `dir_region`, `dir_comuna`, `dir_calle`, `dir_numero`, `dir_calle2`, `usuario`) VALUES
(1, '0', NULL, 'okas', NULL, NULL, 7031254, '2019-08-21 12:57:00', '2019-08-21 14:00:00', 13, 'SAN BERNARDO', 'ines de suarez', 223, NULL, 'admin'),
(2, '0', NULL, '', NULL, NULL, 7031254, '2019-08-21 11:08:00', '2019-08-21 11:10:00', 13, 'SAN BERNARDO', 'okkk', 9999, NULL, 'admin'),
(3, '0', NULL, NULL, NULL, NULL, 7031254, '2019-08-21 14:31:00', '2019-08-22 12:29:00', 13, 'SAN BERNARDO', 'camino internacional norte', NULL, NULL, 'admin'),
(4, '0', NULL, 'okkk', NULL, NULL, 15510104, '2019-08-23 13:46:00', '2019-09-05 15:46:00', 13, 'SAN BERNARDO', 'ok', NULL, NULL, 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llamado_bomberos`
--

CREATE TABLE `llamado_bomberos` (
  `id_llamado` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `comentario` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `llamado_bomberos`
--

INSERT INTO `llamado_bomberos` (`id_llamado`, `rut`, `comentario`) VALUES
(1, 15510104, NULL),
(4, 7031254, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id_pago` int(11) NOT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  `fecha_vence` date DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `comentario` text COLLATE utf8_bin DEFAULT NULL,
  `fecha_mail_alerta` date DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `usuario` varchar(16) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`id_pago`, `fecha_ingreso`, `fecha_vence`, `monto`, `comentario`, `fecha_mail_alerta`, `rut`, `usuario`) VALUES
(1, '2019-08-11 04:57:45', NULL, 3000, '', NULL, 15510104, 'admin'),
(2, '2019-08-11 05:00:01', NULL, 6000, '', NULL, 15510104, 'admin'),
(3, '2019-08-11 05:11:25', NULL, 6000, '', NULL, 7031254, 'admin'),
(4, '2019-08-11 23:14:05', NULL, 6000, '', NULL, 15510104, 'admin'),
(5, '2019-08-11 23:14:26', NULL, 3000, '', NULL, 15510104, 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_mes`
--

CREATE TABLE `pago_mes` (
  `id_pago_mes` int(11) NOT NULL,
  `id_pago` int(11) NOT NULL,
  `pago_mes` date NOT NULL,
  `rut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pago_mes`
--

INSERT INTO `pago_mes` (`id_pago_mes`, `id_pago`, `pago_mes`, `rut`) VALUES
(1, 1, '2019-05-01', 15510104),
(1, 2, '2019-06-01', 15510104),
(1, 3, '2019-08-01', 7031254),
(1, 4, '2019-06-01', 15510104),
(1, 5, '2019-08-01', 15510104),
(2, 2, '2019-05-01', 15510104),
(2, 3, '2019-09-01', 7031254),
(2, 4, '2019-07-01', 15510104);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametro`
--

CREATE TABLE `parametro` (
  `nombre` varchar(32) COLLATE utf8_bin NOT NULL,
  `valor` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `comentario` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `parametro`
--

INSERT INTO `parametro` (`nombre`, `valor`, `comentario`) VALUES
('comuna_por_defecto', 'SAN BERNARDO', 'Util para agregar llamado, aparecerá esta comuna por defecto, pudiendo ser cambiada en caso necesario'),
('dominio_email_interno', 'QUINTINOS.CL', ''),
('premio_por_anios', '5', 'Este valor se usa para listar los premio por años , por ejemplo un valor 5 para listar premios multiplos de 5: 5, 10, 15...'),
('region_por_defecto', '13', 'numero de la region mostrada por defecto. util para agregar llamado'),
('valor_cuota', '3000', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `premio_anios`
--

CREATE TABLE `premio_anios` (
  `id_premio_anios` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `anio_premio_cia` int(11) DEFAULT NULL,
  `anio_premio_cuerpo` int(11) NOT NULL,
  `id_cod_anio` int(11) DEFAULT NULL,
  `anios` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `premio_anios`
--

INSERT INTO `premio_anios` (`id_premio_anios`, `rut`, `anio_premio_cia`, `anio_premio_cuerpo`, `id_cod_anio`, `anios`) VALUES
(0, 15510104, 1900, 1901, NULL, 5),
(6, 15510104, 1994, 1995, NULL, 35);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_anotacion`
--

CREATE TABLE `tipo_anotacion` (
  `id_tipo_anotacion` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tipo_anotacion`
--

INSERT INTO `tipo_anotacion` (`id_tipo_anotacion`, `descripcion`) VALUES
(1, 'Distincion'),
(2, 'Amonestacion'),
(9, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_curso`
--

CREATE TABLE `tipo_curso` (
  `descripcion` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `id_tipo_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tipo_curso`
--

INSERT INTO `tipo_curso` (`descripcion`, `id_tipo_curso`) VALUES
('ANB', 1),
('CBSB', 2),
('QUINTA', 3),
('OTRO', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `descripcion`) VALUES
(1, 'Administrador'),
(2, 'Normal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario` varchar(16) COLLATE utf8_bin NOT NULL,
  `clave` char(64) COLLATE utf8_bin DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `rut` int(11) DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario`, `clave`, `tipo`, `rut`, `habilitado`) VALUES
('admin', 'b33cfad024e7044966dc1b8dcf29e44a40775474be3aa21089fa02e99d773bd3', 1, NULL, 1),
('pato', 'c4aa39c9c54ac9034bae6f0bd95d24f5cf15ccffd1984a297182fab2e193f4d3', 2, 15510104, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anotacion`
--
ALTER TABLE `anotacion`
  ADD PRIMARY KEY (`id_anotacion`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_tipo_anotacion` (`id_tipo_anotacion`);

--
-- Indices de la tabla `bombero`
--
ALTER TABLE `bombero`
  ADD PRIMARY KEY (`rut`),
  ADD KEY `id_est_civil` (`id_est_civil`),
  ADD KEY `id_est_bombero` (`id_est_bombero`),
  ADD KEY `usuario_actualiza` (`usuario_actualiza`),
  ADD KEY `id_cod_cargo` (`id_cod_cargo`),
  ADD KEY `direc_region` (`direc_region`);

--
-- Indices de la tabla `cargo_actual`
--
ALTER TABLE `cargo_actual`
  ADD PRIMARY KEY (`id_cargo_actual`),
  ADD UNIQUE KEY `id_cargo_actual_2` (`id_cargo_actual`),
  ADD UNIQUE KEY `id_cargo_actual_4` (`id_cargo_actual`),
  ADD UNIQUE KEY `id_cargo_actual_3` (`id_cargo_actual`,`rut`),
  ADD KEY `rut` (`rut`);

--
-- Indices de la tabla `cargo_hst`
--
ALTER TABLE `cargo_hst`
  ADD PRIMARY KEY (`id_cargo`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_cod_cargo` (`id_cod_cargo`);

--
-- Indices de la tabla `cod_anio`
--
ALTER TABLE `cod_anio`
  ADD PRIMARY KEY (`id_cod_anio`);

--
-- Indices de la tabla `cod_cargo`
--
ALTER TABLE `cod_cargo`
  ADD PRIMARY KEY (`id_cod_cargo`);

--
-- Indices de la tabla `cod_region`
--
ALTER TABLE `cod_region`
  ADD PRIMARY KEY (`id_cod_region`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id_curso`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_tipo_curso` (`id_tipo_curso`);

--
-- Indices de la tabla `est_bombero`
--
ALTER TABLE `est_bombero`
  ADD PRIMARY KEY (`id_est_bombero`);

--
-- Indices de la tabla `est_civil`
--
ALTER TABLE `est_civil`
  ADD PRIMARY KEY (`id_est_civil`);

--
-- Indices de la tabla `llamado`
--
ALTER TABLE `llamado`
  ADD PRIMARY KEY (`id_llamado`,`rut_acargo`),
  ADD KEY `rut` (`rut_acargo`);

--
-- Indices de la tabla `llamado_bomberos`
--
ALTER TABLE `llamado_bomberos`
  ADD PRIMARY KEY (`id_llamado`,`rut`),
  ADD KEY `rut` (`rut`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id_pago`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `pago_mes`
--
ALTER TABLE `pago_mes`
  ADD PRIMARY KEY (`id_pago_mes`,`id_pago`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_pago` (`id_pago`);

--
-- Indices de la tabla `parametro`
--
ALTER TABLE `parametro`
  ADD PRIMARY KEY (`nombre`);

--
-- Indices de la tabla `premio_anios`
--
ALTER TABLE `premio_anios`
  ADD PRIMARY KEY (`id_premio_anios`,`rut`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_cod_anio` (`id_cod_anio`);

--
-- Indices de la tabla `tipo_anotacion`
--
ALTER TABLE `tipo_anotacion`
  ADD PRIMARY KEY (`id_tipo_anotacion`);

--
-- Indices de la tabla `tipo_curso`
--
ALTER TABLE `tipo_curso`
  ADD PRIMARY KEY (`id_tipo_curso`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario`),
  ADD KEY `rut` (`rut`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `anotacion`
--
ALTER TABLE `anotacion`
  ADD CONSTRAINT `anotacion_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  ADD CONSTRAINT `anotacion_ibfk_2` FOREIGN KEY (`id_tipo_anotacion`) REFERENCES `tipo_anotacion` (`id_tipo_anotacion`);

--
-- Filtros para la tabla `bombero`
--
ALTER TABLE `bombero`
  ADD CONSTRAINT `bombero_ibfk_1` FOREIGN KEY (`id_est_civil`) REFERENCES `est_civil` (`id_est_civil`),
  ADD CONSTRAINT `bombero_ibfk_2` FOREIGN KEY (`id_est_bombero`) REFERENCES `est_bombero` (`id_est_bombero`),
  ADD CONSTRAINT `bombero_ibfk_3` FOREIGN KEY (`usuario_actualiza`) REFERENCES `usuario` (`usuario`),
  ADD CONSTRAINT `bombero_ibfk_4` FOREIGN KEY (`id_cod_cargo`) REFERENCES `cod_cargo` (`id_cod_cargo`),
  ADD CONSTRAINT `bombero_ibfk_6` FOREIGN KEY (`direc_region`) REFERENCES `cod_region` (`id_cod_region`);

--
-- Filtros para la tabla `cargo_actual`
--
ALTER TABLE `cargo_actual`
  ADD CONSTRAINT `cargo_actual_ibfk_1` FOREIGN KEY (`id_cargo_actual`) REFERENCES `cod_cargo` (`id_cod_cargo`),
  ADD CONSTRAINT `cargo_actual_ibfk_2` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`);

--
-- Filtros para la tabla `cargo_hst`
--
ALTER TABLE `cargo_hst`
  ADD CONSTRAINT `cargo_hst_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  ADD CONSTRAINT `cargo_hst_ibfk_2` FOREIGN KEY (`id_cod_cargo`) REFERENCES `cod_cargo` (`id_cod_cargo`);

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  ADD CONSTRAINT `curso_ibfk_2` FOREIGN KEY (`id_tipo_curso`) REFERENCES `tipo_curso` (`id_tipo_curso`);

--
-- Filtros para la tabla `llamado`
--
ALTER TABLE `llamado`
  ADD CONSTRAINT `llamado_ibfk_1` FOREIGN KEY (`rut_acargo`) REFERENCES `bombero` (`rut`);

--
-- Filtros para la tabla `llamado_bomberos`
--
ALTER TABLE `llamado_bomberos`
  ADD CONSTRAINT `llamado_bomberos_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  ADD CONSTRAINT `llamado_bomberos_ibfk_2` FOREIGN KEY (`id_llamado`) REFERENCES `llamado` (`id_llamado`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  ADD CONSTRAINT `pago_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`);

--
-- Filtros para la tabla `pago_mes`
--
ALTER TABLE `pago_mes`
  ADD CONSTRAINT `pago_mes_ibfk_2` FOREIGN KEY (`id_pago`) REFERENCES `pago` (`id_pago`);

--
-- Filtros para la tabla `premio_anios`
--
ALTER TABLE `premio_anios`
  ADD CONSTRAINT `premio_anios_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
