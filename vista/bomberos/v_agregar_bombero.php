<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Agregar llamado') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-06T06:54:23-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
</head>
<body>
<div>
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>
<p style="clear: both; text-align: right">último actualizado:
<strong><?php  echo $ultimo['nombre'] ?></strong>
RUT:<strong><a href="ver_bombero.php?rut=<?php echo $ultimo['rut'] ?>"><?php echo $ultimo['rut']; ?></a></strong></p>
<div class="tabla" style="margin-top: 0.5em">
<h1 style="color: white; background-color: black">Agregar Bombero</h1>
<form method="post" action="guardar_bombero.php">
<fieldset style="margin: 10px">
    <legend>datos de bombero:</legend>
	<table><tr>
	<td>nombres:</td><td><input type="text" name="nombre" value="<?php echo $form['nombre']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>apellidos:</td><td><input type="text" name="apellido" value="<?php echo $form['apellido']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>rut:</td><td><input type="number" name="rut" value="<?php echo $form['rut']; ?>" min="1" class="amarillo" style="width: 9em" /> - <input type="text" maxlength="1" name="rut_dv" value="<?php echo $form['rut_dv']; ?>" class="amarillo" style="width: 3em" /></td></tr>
	<tr><td style="vertical-align: top">sexo:</td><td>
	<ul style="list-style-type: none">
		<li><input type="radio" name="sexo" value="m" class="amarillo" id="sexo_m" <?php echo $sel_sexo_m ?> /><label for="sexo_m"> Masculino</label></li>
		<li><input type="radio" name="sexo" value="f" class="amarillo" id="sexo_f" <?php echo $sel_sexo_f ?> /><label for="sexo_f"> Femenino</label></li>	
	</ul>
	</td>
	</tr>
	<tr>
	<td>nombre padre:</td><td><input type="text" name="nombre_padre" value="<?php echo $form['nombre_padre']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>nombre madre:</td><td><input type="text" name="nombre_madre" value="<?php echo $form['nombre_madre']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>profesión:</td><td><input type="text" name="profesion" value="<?php echo $form['profesion'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>estado civil:</td><td>
	<select name="id_est_civil">
		<option value="">-- selecione --</option>
		<?php foreach($estados_civil as $id => $est){ ($id==$form['id_est_civil'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $est ?></option>
		<?php } ?>
	</select>
	</td></tr>
	<tr><td>grupo sanguineo:</td><td><input type="text" name="grup_san" value="<?php echo $form['grup_san']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td style="vertical-align: top">direccion:</td>
		<td>
		<table>
		<tr><td>
		Región:</td><td>
		<select name="direc_region">
		<option value="">-- selecione --</option>
		<?php foreach($regiones as $id => $region){ ($id==$form['direc_region'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel; ?>><?php echo $region; ?></option>
		<?php } ?>
		</select>
		</td></tr>
		<tr><td>
		Comuna:</td><td><input type="text" name="direc_comuna" value="<?php echo $form['direc_comuna'];?>" class="amarillo" style="width: 20em" /> 
		</td></tr>
		<tr><td>
			Calle:</td><td><input type="text" name="direc_calle" value="<?php echo $form['direc_calle'];?>" class="amarillo" style="width: 20em" />
		</td></tr>
		<tr><td>
		Número:</td><td><input type="text" name="direc_numero" value="<?php echo $form['direc_numero'];?>" class="amarillo" style="width: 20em" /> 
		</td></tr>
		<tr><td>
		Departamento:</td><td><input type="text" name="direc_depto" value="<?php echo $form['direc_depto'];?>" class="amarillo" style="width: 20em" />
		</td></tr>
		</table>
	<tr><td>teléfono:</td><td><input type="tel" name="fono" value="<?php echo $form['fono']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>teléfono2:</td><td><input type="tel" name="fono2" value="<?php echo $form['fono2']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>email:</td><td><input type="email" name="email" value="<?php echo $form['email']; ?>" class="amarillo" style="width: 20em;" /></td></tr>
	<tr><td>email2:</td><td><input type="email" name="email2" value="<?php echo $form['email2']; ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>fecha de nacimiento:</td>
<td>
<input type="date" name="fecha_nac" class="amarillo" value="<?php echo $form['fecha_nac']; ?>" />
</td>
<tr><td>fecha de ingreso:</td>
<td>
<input type="date" name="fecha_ingreso" class="amarillo" value="<?php echo $form['fecha_ingreso']; ?>" />
</td>
</tr>
<tr><td>Número registro:</td><td><input type="number" name="num_registro" value="<?php echo $form['num_registro']; ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Número placa:</td><td><input type="number" name="num_placa" value="<?php echo $form['num_placa']; ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Número tib:</td><td><input type="number" name="num_tib" value="<?php echo $form['num_tib']; ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Estado:</td><td>
	<select name="id_est_bombero">
		<option value="">-- selecione --</option>
		<?php foreach($estados_bombero as $id => $est){ ($id==$form['id_est_bombero'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $est ?></option>
		<?php } ?>
	</select>
	</td>
</tr>
<tr><td>Comentario extra <em>(opcional)</em>:</td><td><textarea name="comentario" cols="40" rows="4"><?php echo $form['comentario']; ?></textarea>
	</td>
</tr>
</tbody>
</table>
</fieldset>
	<p style="text-align: right; margin: 0.5em">
		<input type="submit" name="guardar_agrega_mas" value="guardar y agregar otro" style="" />
		<input type="submit" name="guardar_editar_vida" value="guardar y editar hoja de vida" style="" />
		<input type="submit" name="guardar_ver_lista" value="guardar y ver lista" style="" />
	</p>
</form>
</div>
</div>
</body>
</html>