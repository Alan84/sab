<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Reportes') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-13T01:21:20-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> 	<a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>

<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">

<h1 style="color: white; background-color: black">Reportes</h1>
	<ul>
		<li>Usuarios: <?php echo $total_usuarios; ?></li>
		<li>Bomberos: <?php echo $total_bomberos; ?></li>
		<li><a href="bomberos_morosos.php">Bomberos con deuda: <?php echo $total_bomberos_morosos; ?></a></li>
		<li><a href="bomberos_borrados.php">Bomberos borrados: <?php echo $total_bomberos_borrados; ?></a></li>
		<?php if($puede_ver_pagos){ ?>
			<li><a href="pagos.php">Pagos: <?php echo $total_pagos; ?></a></li>
			<li><a href="pagos_borrados.php">Pagos borrados: <?php echo $total_pagos_borrados; ?></a></li>
		<?php } ?>
		<li>Llamados: <?php echo $total_llamados; ?></li>
		<li><a href="llamados_borrados.php">Llamados borrados: <?php echo $total_llamados_borrados; ?></a></li>
		
	</ul>
</div>
</body>
</html>