<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_cargo.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_est_bombero = new est_bombero($con);
$t_cod_cargo = new cod_cargo($con);
$m_cargos = new m_cargos($con);
$t_bombero = new bombero($con);

$errores=check_errores();
$borrar=array();
$cambios=array();
$meses=array(''=>'', '01'=> 'Enero', '02'=> 'Febrero', '03'=>'Marzo', '04'=> 'Abril', '05'=> 'Mayo', '06'=> 'Junio',
	'07'=> 'Julio' , '08'=> 'Agosto', '09'=> 'Septiembre', '10'=> 'Octubre', '11'=> 'Noviembre', '12'=> 'Diciembre');
if(isset($_POST['rut'])){
	foreach ($_POST['rut'] as $cod_cargo=>$rut_cambio){
		if($rut_cambio!=='') {
			if($rut_cambio=='0') {
				$accion='Borrar ';
			}
			else{
				$bombero=$t_bombero->obtener_bombero($rut_cambio);
				$accion='Cambiar por <strong>'.$bombero['nombre'].' '.$bombero['apellido'].' '.number_format($bombero['rut'], 0, ',','.').'-'.$bombero['rut_dv'].'</strong>';
				if(trim($_POST['fini_cargo'][$cod_cargo])==''){
					$errores[]='El campo Fecha inicio cargo está vacío';
				}
				else {
					list($anio, $mes, $dia) = explode('-', $_POST['fini_cargo'][$cod_cargo]);
					$accion.='. Fecha de inicio: '.$anio.' '.$meses[$mes].' '.$dia;
				}
				
				//if(trim($_POST['fini_cargo'][$cod_cargo])=='')
				//	$errores[]='El campo Fecha inicio cargo está vacío';	
				
			}
			/*
				$m_cargos->obtener_rut_cargo_actual($cod_cargo).' '.
					$m_cargos->obtener_nombre_cargo_actual($cod_cargo).'->'.
					$m_cargos->obtener_rut_cargo_actual($cod_cargo).' '.$m_cargos->obtener_nombre_cargo_actual($cod_cargo);
					*/
			$cambios[]=array($t_cod_cargo->obtener_descripcion($cod_cargo), $m_cargos->obtener_nombre_cargo_actual($cod_cargo).' '.$m_cargos->obtener_rut_cargo_actual($cod_cargo), $accion);
		}
	}
}



if($cambios==array())
	$errores[]='No se han seleccionado cambios.';
	
	

$estados=$t_est_bombero->obtener_estados_bomberos();
//$cargos=$t_cargo->obtener_cargos();
$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','-- seleccionar --')));


if ($errores==array()){
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_confirmar_cargos.php');
}
else{
	$enlace_volver = DIRECTORIO_WEB_SISTEMA.'/sistema/bomberos/cargos.php';
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}

function check_errores(){
	;
}