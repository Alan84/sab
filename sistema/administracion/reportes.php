<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

$_SESSION['uname']='';
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_reportes.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$m_reportes = new m_reportes($con);
$m_cargos = new m_cargos($con);
$t_usuario = new usuario($con);

//$usuarios = $t_usuario->obtener_lista_usuarios();

$puede_ver_pagos=false;
if($m_cargos->usuario_es_tesorero($_SESSION['usr']) or $t_usuario->es_administrador($_SESSION['usr']))
	$puede_ver_pagos=true;
	
$total_usuarios = $m_reportes->obtener_total_usuarios();
$total_bomberos = $m_reportes->obtener_total_bomberos();
$total_bomberos_morosos = $m_reportes->obtener_total_bomberos_morosos();
$total_bomberos_borrados = $m_reportes->obtener_total_bomberos_borrados();
if($puede_ver_pagos){
	$total_pagos_borrados = $m_reportes->obtener_total_pagos_borradas();
	$total_pagos = $m_reportes->obtener_total_pagos();
}
$total_llamados = $m_reportes->obtener_total_llamados();
$total_llamados_borrados = $m_reportes->obtener_total_llamados_borrados();
$total_socios = $m_reportes->obtener_total_socios();



include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_reportes.php';