<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
require(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/class/fpdf.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');

$rut='';
if(isset($_GET['rut']) and is_numeric($_GET['rut']))
        $rut=$_GET['rut'];

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$m_bomberos = new m_bomberos($con);
$bombero=$m_bomberos->obtener_bombero($rut);

$pdf = new FPDF();
$pdf->AddPage();
$pdf->Image('../../imagenes/Cuerpo_de_Bomberos_de_San_Bernardo.png',20,12, 30);
$pdf->Image('../../imagenes/296081_248184175216075_7878036_n.jpg',155,11, 25);
$pdf->SetFont('Times','B',20);
$pdf->Cell(0,120,'CERTIFICADO DE BOMBERO', 0, 0, 'C');
$pdf->SetFont('Arial','',15);
$pdf->ln(80);
$pdf->MultiCell(0,10, utf8_decode('Por medio de este certificado se acredita al señor '.$bombero['nombre'].' '.$bombero['apellido'].' como un bombero voluntario de muestra compañía de bomberos.'));
$pdf->ln(50);
//$pdf->Cell(90,60, utf8_decode('Director 5ta cía CBSB'), 0, 0, 'C');
$pdf->Output('certificado_bombero_'.$rut.'.pdf', 'D');