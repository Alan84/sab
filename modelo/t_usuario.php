<?php

class usuario{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_lista_usuarios() {
		$sql="select * from usuario";
		$query=$this->con->prepare($sql);
		$query->execute();
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	function obtener_usuario($rut) {
		$sql="select * from usuario where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0];
		else
			return false;
	}

	function obtener_rut($usuario) {
		$sql="select rut from usuario where usuario=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['rut'];
		else
			return '';
	}

	function obtener_tipo($usuario) {
		$sql="select tipo from usuario where usuario=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['tipo'];
		else
			return '';
	}
	
	function obtener_clave($usuario) {
		$sql="select clave  from usuario where usuario=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['clave'];
		else
			return false;
	}

	function usuario_habilitado($usuario) {
		$sql="select habilitado from usuario where usuario=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]) and $result[0]['habilitado']=='1')
			return true;
		else
			return false;
	}

	
	function guardar_usuario($rut, $uname, $upass, $tipo_usuario, $habilitado) {
		$sql="insert into usuario (rut, usuario, clave, tipo, habilitado) values (?, ?, ?, ?, ?)";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $uname, hash('sha256',$upass), $tipo_usuario, $habilitado));
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		return 0; //$result[0];
	}
	
	
	function actualizar_usuario($rut, $uname, $upass, $tipo_usuario, $habilitado) {
		$sqlpass='';
		if($upass!='')
			$sqlpass='clave=?,';
		$sql="update usuario set rut=?, $sqlpass tipo=?, habilitado=? where usuario=?";
		$query=$this->con->prepare($sql);
		if($sqlpass=='')
			$query->execute(array($rut, $tipo_usuario, $habilitado, $uname));
		else
			$query->execute(array($rut, hash('sha256',$upass), $tipo_usuario, $habilitado, $uname));
			
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		return 0; //$result[0];
	}
	
	function eliminar_usuario($usuario) {
		$sql="delete from usuario where usuario=? and tipo!=".TIPO_USUARIO_ADMINISTRADOR; //no se puede eliminar usuarios administradores
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		//print_r($query->errorInfo());
	}

	function es_administrador($usuario){
		$sql="select 1 from usuario where usuario=? and tipo=".TIPO_USUARIO_ADMINISTRADOR;
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return true;
		else
			return false;
	}

	function es_normal($usuario){
		$sql="select 1 from usuario where usuario=? and tipo=".TIPO_USUARIO_NORMAL;
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return true;
		else
			return false;
	}
	
}