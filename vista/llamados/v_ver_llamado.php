<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Ver llamado') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-04T11:16:03-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<div class="tabla">
<h1 style="color: white; background-color: black">Llamado</h1>
	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/llamados/consulta_llamados.php" method="post">
			<table>
				<tr><td>id llamado:</td><td>
					<?php echo $llamado['id_llamado']; ?>	
				</td>
				</tr>
				<tr><td>tipo llamado:</td><td>
					<?php echo $llamado['cod_tipo_llamado']; ?>	
				</td>
				</tr>
			<tr><td>fecha/hora inicio:</td><td><?php echo $llamado['fecha_ini']; ?> * salida del cuartel</td></tr>
			<tr><td>fecha/hora termino:</td><td><?php echo $llamado['fecha_fin']; ?>	* llegada al cuartel</td></tr>
			
			<tr><td>dirección:</td><td>Región: <?php echo $t_cod_region->obtener_descripcion($llamado['dir_region']);?><br /> 
				Comuna: <?php echo $llamado['dir_comuna']; ?> <br /> 
				Calle: <?php echo $llamado['dir_calle']; ?><br /> 
				Número: <?php echo $llamado['dir_numero']; ?><br />
				Calle2: <?php echo $llamado['dir_calle2']; ?> 
			</td></tr>
			<tr><td>Observaciones:</td><td><?php echo $llamado['observaciones']; ?></td></tr>
			<tr><td>Rut bombero a cargo: <br />
			
			</td><td><?php echo $t_bombero->obtener_nombre_completo($llamado['rut_acargo']).' ['.$llamado['rut_acargo'].'-'.$t_bombero->obtener_rut_dv($llamado['rut_acargo']).']'; ?></td></tr>
			
			<tr><td style="vertical-align: top">Bomberos asitentes:</td><td>
			<em>*Nota: No se mostrará en la lista el rut del bombero a cargo.</em>
				<ul>
				<?php if(isset($llamado_bomberos) and is_array($llamado_bomberos)) foreach($llamado_bomberos as $llamado_bom){ ?>
					<?php ; // if($rut_asiste!=$_POST['rut_acargo']){ ?>
						<li><span class="bombero_asiste"><?php echo $t_bombero->obtener_nombre_completo($llamado_bom['rut']).' ['.$llamado_bom['rut'].'-'.$t_bombero->obtener_rut_dv($llamado_bom['rut']).']'; ?></span></li>
					<?php ; // } ?>
				<?php } ?>
				</ul>
			</td></td></tr>
			</table>

	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	<input type="hidden" name="monto" value="<?php echo $monto; ?>" />
	<p style="text-align: right"><input type="submit" name="volver" value="Volver" /></p>
</form>
</div>
</div>
</body>
</html>