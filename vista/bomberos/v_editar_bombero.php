<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Editar bombero') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-06T07:08:05-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
</head>
<body>
<div>
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
	<p style="text-align: right; margin: 0em">
		<a href="ver_bombero.php?rut=<?php echo $rut?>"  class="tab">ver bombero</a>
		<a href="editar_bombero.php?rut=<?php echo $rut?>"  class="tab_elegido">editar datos personales</a>
		<a href="editar_vida_bombero.php?rut=<?php echo $rut?>" class="tab">editar hoja de vida</a>
	</p>
<div class="tabla" style="margin-top: 0em">
<h1 style="color: white; background-color: black">Editar Bombero</h1>
<form method="post" action="actualizar_bombero.php" enctype="multipart/form-data">
<fieldset style="margin: 10px">
    <legend>datos de bombero:</legend>
	<table><tr>
	<td>nombres:</td><td><input type="text" name="nombre" value="<?php echo $bombero['nombre'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>apellidos:</td><td><input type="text" name="apellido" value="<?php echo $bombero['apellido'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>rut:</td><td><input type="number" name="rut" value="<?php echo $rut ?>" min="1" class="amarillo" style="width: 9em" /> - <input type="text" maxlength="1" name="rut_dv" value="<?php echo $bombero['rut_dv'] ?>" class="amarillo" style="width: 3em" /></td></tr>
	<tr><td>foto:</td><td><input type="file" name="foto" class="amarillo" /><?php if($bombero['foto']=='1'){ ?><br /> <a href="<?php echo DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$foto?>"><?php echo $foto;?></a> <input type="checkbox" id="borrar_foto" name="borrar_foto" value="1" /><label for="borrar_foto">borrar</label><?php } ?></td></tr>
	<tr><td style="vertical-align: top">sexo:</td><td>
	<ul style="list-style-type: none">
		<li><input type="radio" name="sexo" value="m" class="amarillo" id="sexo_m" <?php echo $sel_sexo_m ?> /><label for="sexo_m"> Masculino</label></li>
		<li><input type="radio" name="sexo" value="f" class="amarillo" id="sexo_f" <?php echo $sel_sexo_f ?> /><label for="sexo_f"> Femenino</label></li>	
	</ul>
	</td>
	</tr>
	<tr><td>nombre padre:</td><td><input type="text" name="nombre_padre" value="<?php echo $bombero['nombre_padre'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>nombre madre:</td><td><input type="text" name="nombre_madre" value="<?php echo $bombero['nombre_madre'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>profesión:</td><td><input type="text" name="profesion" value="<?php echo $bombero['profesion'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>estado civil:</td><td>
	<select name="id_est_civil">
		<option value="">-- selecione --</option>
		<?php foreach($estados_civil as $id => $est){ ($id==$bombero['id_est_civil'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $est ?></option>
		<?php } ?>
	</select>
	</td></tr>
	<tr><td>grupo sanguineo:</td><td>
		<input type="text" name="grup_san" value="<?php echo $bombero['grup_san'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td style="vertical-align: top">direccion:</td>
		<td>
		<table>
		<tr><td>
		Región:</td><td>
		<select name="direc_region">
		<option value="">-- selecione --</option>
		<?php foreach($regiones as $id => $region){ ($id==$bombero['direc_region'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel; ?>><?php echo $region; ?></option>
		<?php } ?>
		</select>
		</td></tr>
		<tr><td>
		Comuna:</td><td><input type="text" name="direc_comuna" value="<?php echo $bombero['direc_comuna']?>" class="amarillo" style="width: 20em" /> 
		</td></tr>
		<tr><td>
			Calle:</td><td><input type="text" name="direc_calle" value="<?php echo $bombero['direc_calle']?>" class="amarillo" style="width: 20em" />
		</td></tr>
		<tr><td>
		Número:</td><td><input type="text" name="direc_numero" value="<?php echo $bombero['direc_numero']?>" class="amarillo" style="width: 20em" /> 
		</td></tr>
		<tr><td>
		Departamento:</td><td><input type="text" name="direc_depto" value="<?php echo $bombero['direc_depto']?>" class="amarillo" style="width: 20em" />
		</td></tr>
		</table>
	<tr><td>teléfono:</td><td><input type="tel" name="fono" value="<?php echo $bombero['fono'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>teléfono2:</td><td><input type="tel" name="fono2" value="<?php echo $bombero['fono2'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>email:</td><td><input type="text" name="email" value="<?php echo $bombero['email'] ?>" class="amarillo" style="width: 20em;" /></td></tr>
	<tr><td>email2:</td><td><input type="email" name="email2" value="<?php echo $bombero['email2'] ?>" class="amarillo" style="width: 20em" /></td></tr>
	<tr><td>fecha de nacimiento:</td>
<td>
<!--
<input type="number" name="fecha_nac_d" value="<?php echo $bombero['fecha_nac_d'] ?>" min="1" max="31" class="amarillo" style="width: 3em" />
<span style="color: #666"> / </span><select name="fecha_nac_m">
		<option value="">-- selecione --</option>
		<?php foreach($meses as $id => $mes){ ($id==$bombero['fecha_nac_m'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $mes ?></option>
		<?php } ?>
	</select>
<span style="color: #666"> / </span><input type="number" name="fecha_nac_a" value="<?php echo $bombero['fecha_nac_a'];?>" min="<?php echo date('Y')-200;?>" max="<?php echo date('Y')-10;?>" class="amarillo" style="width: 4em" />
-->
<input type="date" class="amarillo" name="fecha_nac" value="<?php echo $bombero['fecha_nac']; ?>" />
</td>
<tr><td>fecha de ingreso:</td>
<td>
<!--
<input type="number" name="fecha_ingreso_d" value="<?php echo $bombero['fecha_ingreso_d'] ?>" min="1" max="31" class="amarillo" style="width: 3em" />
<span style="color: #666"> / </span><select name="fecha_ingreso_m">
		<option value="">-- selecione --</option>
		<?php foreach($meses as $id => $mes){ ($id==$bombero['fecha_ingreso_m'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $mes ?></option>
		<?php } ?>
	</select>
<span style="color: #666"> / </span><input type="number" name="fecha_ingreso_a" value="<?php echo $bombero['fecha_ingreso_a'];?>" min="<?php echo date('Y')-200;?>" max="<?php echo date('Y')-10;?>" class="amarillo" style="width: 4em" />
-->
<input type="date" class="amarillo" name="fecha_ingreso" value="<?php echo $bombero['fecha_ingreso']; ?>" />
</td>
</tr>
<tr><td>Número registro:</td><td><input type="number" name="num_registro" value="<?php echo $bombero['num_registro'] ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Número placa:</td><td><input type="number" name="num_placa" value="<?php echo $bombero['num_placa'] ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Número tib:</td><td><input type="number" name="num_tib" value="<?php echo $bombero['num_tib'] ?>" class="amarillo" style="width: 20em" /></td></tr>
<tr><td>Estado:</td><td>
	<select name="id_est_bombero">
		<option value="">-- selecione --</option>
		<?php foreach($estados_bombero as $id => $est){ ($id==$bombero['id_est_bombero'])?$sel=' selected="selected"':$sel='';?>
			<option value="<?php echo $id ?>"<?php echo $sel ?>><?php echo $est ?></option>
		<?php } ?>
	</select>
	</td>
</tr>
<tr><td>Comentario extra:</td><td>
	<textarea cols="40" rows="4" name="comentarios"><?php echo $bombero['comentarios']; ?></textarea>
	</td>
</tr>
</tbody>
</table>
</fieldset>
<em>formato de ingreso de fecha: DD-MM-AAAA (ejemplo 01-12-1999)</em>
	<p style="text-align: right; margin: 0.5em">
		<input type="submit" name="guardar" value="guardar" style="" />
	</p>
</form>
</div>
</div>
</body>
</html>