<?php

class est_bombero{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_estados_bomberos() {
		$datos=array();
		$sql="select * from est_bombero";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_est_bombero']]=$row['descripcion'];
		return $datos;
	}
}
