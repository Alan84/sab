<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Agregar llamados') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-07T15:01:25-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc'); ?>

</div>

<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%"  >
<div class="tabla">
<h1 style="color: white; background-color: black">Agregar llamado</h1>

	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/llamados/confirmar_llamado.php" method="post">
			<table>
				<tr><td>tipo llamado:</td><td><select name="cod_tipo_llamado">
				<option value=""></option>
				<?php foreach($tipos_llamado as $id => $tipo){ ?>
					<?php $sel=($form['cod_tipo_llamado'] == "$id")?' selected="selected"':''; ?>
					<option value="<?php echo $id ?>" <?php echo $sel; ?>><?php echo $tipo ?></option>
				<?php } ?>
				</select></td>
				</tr>
			<tr><td>fecha/hora inicio:</td><td><input type="date" name="fecha_ini" value="<?php echo $form['fecha_ini']; ?>" class="amarillo" /> <input type="time" name="hora_ini" value="<?php echo $form['hora_ini']; ?>" class="amarillo" /> * salida del cuartel</td></tr>
			<tr><td>fecha/hora termino:</td><td><input type="date" name="fecha_fin" value="<?php echo $form['fecha_fin']; ?>" class="amarillo" /> <input type="time" name="hora_fin" value="<?php echo $form['hora_fin']; ?>" class="amarillo" />	* llegada al cuartel</td></tr>
			
			<tr><td>dirección:</td><td>Región:
			<select name="dir_region">
				<option value="">-- seleccionar --</option>
				<?php foreach($regiones as $id => $region){ ($id==$form['dir_region'])?$sel=' selected="selected"':$sel='';?>
					<option value="<?php echo $id ?>"<?php echo $sel; ?>><?php echo $region; ?></option>
				<?php } ?>
			</select> <br /> 
				Comuna: <input type="text" name="dir_comuna" value="<?php echo $form['dir_comuna']; ?>" class="amarillo" /> <br /> 
				Calle: <input type="text" name="dir_calle" value="<?php echo $form['dir_calle']; ?>" class="amarillo" /> <br /> 
				Número: <input type="text" name="dir_numero" value="<?php echo $form['dir_numero']; ?>" class="amarillo" /><br />
				Calle2: <input type="text" name="dir_calle2" value="<?php echo $form['dir_calle2']; ?>" class="amarillo" /> *calle cercana, intersección o referencial
			</td></tr>
			<tr><td>Observaciones:</td><td><textarea name="observaciones" cols="25" rows="5"><?php echo $form['observaciones']; ?></textarea></td></tr>
			<tr><td>Bombero a cargo:</td><td>
				<select name="rut_acargo">
				<?php foreach($bomberos_cbo_acargo as $bom){ ?>
					<option value="<?php echo $bom[0]; ?>" <?php echo ($form['rut_acargo'] == $bom[0])?' selected="selected"':''; ?> ><?php echo $bom[1]; ?></option>
				<?php } ?>
				</select> * Este dato no es necesario incluirlo en la lista de <em>bomberos asistentes</em> (lista de abajo), se entiende que el bombero a cargo asistió. Si lo agrega en la lista, el sistema automáticamente no lo considerará para así <strong>evitar una redundancia de datos</strong>.
				
				<!-- <input type="text" name="rut" value="<?php echo $form['rut']; ?>" /> <span style="font-weight: bold;">-</span> <input type="text" name="rut_dv" value="<?php echo $form['rut_dv']; ?>" maxlength="2" style="width: 2em" /> * Este dato no es necesario incluirlo en la lista de <em>bomberos asistentes</em> (lista de abajo), se entiende que el bombero a cargo asistió. Si lo agrega en la lista, automáticamente el sistema no lo considerará para así <strong>evitar una redundancia de datos</strong>.</td></tr> -->
			</td>
			</tr>
			<tr><td style="vertical-align: top">Bomberos asitentes:</td><td>
				<ul>
				<?php $i=0; foreach($bomberos as $bom){ ?>
					<li><input type="checkbox" name="rut_asistentes[<?php echo $i; ?>]" value="<?php echo $bom['rut']; ?>" id="rut_asiste[<?php echo $bom['rut']; ?>]" <?php echo (isset($form['rut_asistentes'][$i]))?' checked="checked"':''; ?> /><label for="rut_asiste[<?php echo $bom['rut']; ?>]"><span class="bombero_asiste"><?php echo $bom['nombre'].' '.$bom['apellido'].' ['.number_format($bom['rut'], 0,',','.').'-'.$bom['rut_dv'].']'; ?></span></label></li>
				<?php $i++; } ?>
				</ul>
			</td></td></tr>
			</table>			
			<input type="submit" name="enviar" value="Agregar" />
	</form>
	</div>
</div>
</body>
</html>