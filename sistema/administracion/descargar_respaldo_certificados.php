<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');

//$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
//$t_usuario = new usuario($con);
//backDb($con);



//include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_base_de_datos.php';
/*
$zip = new ZipArchive();

$filename = 'test.zip';


if($zip->open($filename,ZIPARCHIVE::CREATE)===true) {
***/
// Get real path for our folder
$rootPath = DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO;

// Initialize archive object
$zip = new ZipArchive();
$nombre_zip='certificados_'.date('d-m-Y').'.zip';
$zip->open($rootPath.'/'.$nombre_zip, ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

$i=0;
foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);
			if(preg_match('/'.str_replace('%d', '[0-9]+', NOM_ARCHIVO_CERTIFICADO).'\.[a-z]{2,4}/', $relativePath)) {
				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
				$i++;
        }
    }
}

//for($index=0; $index<$i; $index++)
  //  $zip->setCompressionIndex($index, ZipArchive::CM_STORE);

// Zip archive will be created only after closing object
$zip->close();


/////////// forzar descarga
 header("Content-type: application/octet-stream");
 header("Content-disposition: attachment; filename=".$nombre_zip);
 // leemos el archivo creado
 readfile($rootPath.'/'.$nombre_zip);
 
 unlink($rootPath.'/'.$nombre_zip);