<?php

class m_llamados{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}
	
	function obtener_llamados($filtro_sql='', $orden='', $pagina=0, $total_por_pagina=0) {
		 //obtenemos la lista de bomberos a consultar
		$sql="select ll.rut_acargo, b.rut_dv, ll.id_llamado, ll.cod_tipo_llamado, 
					date_format(ll.fecha_ini, '%H:%i %d-%m-%Y') as fecha_ini, date_format(ll.fecha_fin, '%H:%i %d-%m-%Y') as fecha_fin,
					date_format(timediff(ll.fecha_fin, ll.fecha_ini), '%H:%i') as horas,
					ll.observaciones, ll.dir_region, ll.dir_comuna, ll.dir_calle, ll.dir_numero, ll.dir_calle2,
					(select count(*) from llamado_bomberos as llb where llb.id_llamado = ll.id_llamado) as asistentes, ll.usuario
					
        	        
	                 from bombero b, llamado ll
        	        where b.rut=ll.rut_acargo and b.borrado is null and ll.borrado is null 
                	 $filtro_sql "; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
                	 
                	 /* (select pago_mes
	                	from pago_mes pm where pm.id_pago = p.id_pago and p.rut=b.rut order by id_pago desc, id_pago_mes desc limit 0,1) as ultima_cuota,
	                	*/

		if($orden!='')
			$sql.=" order by $orden";
			
		$inicio= $pagina*$total_por_pagina;
		if($total_por_pagina>0)
			$sql.=" limit $inicio, $total_por_pagina";
			
		$query=$this->con->query($sql);
		$result=array();
		if($query!==false)
        		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function obtener_llamados_borrados($filtro_sql='', $orden='', $pagina=0, $registros_por_pagina=0) {
		 //obtenemos la lista de bomberos a consultar
		$sql="select ll.rut_acargo, b.rut_dv, ll.id_llamado, ll.cod_tipo_llamado, 
					date_format(ll.fecha_ini, '%H:%i %d-%m-%Y') as fecha_ini, date_format(ll.fecha_fin, '%H:%i %d-%m-%Y') as fecha_fin,
					date_format(timediff(ll.fecha_fin, ll.fecha_ini), '%H:%i') as horas,
					ll.observaciones, ll.dir_region, ll.dir_comuna, ll.dir_calle, ll.dir_numero, ll.dir_calle2,
					(select count(*) from llamado_bomberos as llb where llb.id_llamado = ll.id_llamado) as asistentes, ll.usuario
					
        	        
	                 from bombero b, llamado ll
        	        where b.rut=ll.rut_acargo and ll.borrado = 1 
                	 $filtro_sql "; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
                	 
                	 /* (select pago_mes
	                	from pago_mes pm where pm.id_pago = p.id_pago and p.rut=b.rut order by id_pago desc, id_pago_mes desc limit 0,1) as ultima_cuota,
	                	*/

		if($orden!='')
			$sql.=" order by $orden";
			
		
		$query=$this->con->query($sql);
		$result=array();
		if($query!==false)
        		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	
	
	function obtener_total_registros(){
		$sql="select count(*) total
			from llamado where borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		
		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return '0';
	}

}