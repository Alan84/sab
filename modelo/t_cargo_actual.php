<?php

class cargo_actual{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}

	function existe_cargo($id) {
		$sql="select 1 from cargo_actual where id_cargo_actual=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id));
		if($query->rowCount())
			return true;
		else
			return false;
	}
	/* $_POST['rut'], $id, $cargo['id_tipo_cargo'], $cargo['fecha_inicio_a'].'-'$cargo['fecha_inicio_m'].'-'$cargo['fecha_inicio_d'],
				 		$cargo['fecha_termino_a'].'-'$cargo['fecha_termino_m'].'-'$cargo['fecha_termino_d'], $cargo['descripcion'] */
	function guardar_cargo($rut, $id_cargo_actual, $fecha_inicio) {
		$sql="insert into cargo_actual (rut, id_cargo_actual, fecha_inicio) values
						(?, ?, ?)";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id_cargo_actual, $fecha_inicio));
	}

	function actualizar_cargo($rut, $id_cargo_actual, $fecha_inicio) {
		$sql="update cargo_actual set fecha_inicio=?, rut=? where id_cargo_actual=?";
		$q=$this->con->prepare($sql);
		$q->execute(array($fecha_inicio, $rut, $id_cargo_actual));
	}
	
	
	function obtener_fini_cargo_actual($id, $ymd) {
		$sql="select YEAR(fecha_inicio) as fy, MONTH(fecha_inicio) as fm, DAY(fecha_inicio) as fd from cargo_actual  where id_cargo_actual=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			if($ymd=='y')
				return $result[0]['fy'];
			else if($ymd=='m')
				return $result[0]['fm'];
			else
				return $result[0]['fd'];
		else
			return '';
	}
	
}
