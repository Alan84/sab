<?php

class pago{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}


	function existe_pago_para_rut($rut) {
		$sql="select 1 from pago where rut=? and borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		
		if($query->rowCount()>0){
			return true;
		}
		else
			return false;
	}
	
/*
	function guardar_pago($rut, $fecha_vence, $monto, $comentario, $usuario_actualiza) {
		$this->con->beginTransaction();
		$indice = $this->obtener_ultimo_indice($rut);
 		$sql="insert into pago (rut, id_pago, fecha_ingreso, fecha_vence, monto, comentario, usuario)
				values (?, ?, now(), ?,?, ?, ?)";

		$q=$this->con->prepare($sql);
		//echo "$rut, $fecha_vence, $monto, $comentario, $usuario_actualiza";
		$q->execute(array($rut, $indice+1, $fecha_vence, $monto, $comentario, $usuario_actualiza));
		$this->con->commit();
	}
*/
	function obtener_ultimo_indice() {
 		$sql="select id_pago  from pago order by id_pago desc limit 0,1";
 		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['id_pago'];
		else
			return 0;
	}

	function es_fecha_mayor($rut, $fecha_pago) {
 		$sql="select if(fecha_vence<'$fecha_pago', 1, 0) as mayor from pago where rut = ? order by id_pago desc limit 0,1 ";
 		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			if ($result[0]['mayor']==0)
				return FALSE;
			else
				return TRUE;
		else // si no hay registros, es el primero y lo dejamos ingresarlo
			return TRUE;

	}

	function obtener_meses_a_pagar($rut, $fecha_pago_a, $fecha_pago_m) {
		$fecha_pago=$fecha_pago_a.'-'.$fecha_pago_m.'-01';
 		//$sql="select PERIOD_DIFF(DATE_FORMAT('$fecha_pago', '%Y%m'), DATE_FORMAT(fecha_vence, '%Y%m')) as diferencia
 		$sql="select PERIOD_DIFF(DATE_FORMAT('$fecha_pago', '%Y%m'), DATE_FORMAT(fecha_vence, '%Y%m')) as diferencia
			from pago where rut = ? order by id_pago desc limit 0,1 ";
 		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0])){
			return $result[0]['diferencia'];
		}
		else{
			//si no devolvió datos la base de datos, no hay registros anteriores, entonces tomamos de referencia la fecha actual
			$anio_act=date('Y');
			$mes_act=date('m');
			$meses=1;
			if($anio_act<=$fecha_pago_a){
				$meses=$fecha_pago_m-$mes_act;
				$meses=($fecha_pago_a-$anio_act)*12+$meses+1;
			}
			return $meses; //por mientras 1, pero hay que calcular la fecha

		}
	}
/*
		function obtener_ultima_fecha_vencimiento($rut){
           $sql="select month(fecha_vence) fecha_vence_m, year(fecha_vence) fecha_vence_a,
                month(DATE_ADD(fecha_vence, INTERVAL 1 MONTH)) fecha_siguiente_m,
                year(DATE_ADD(fecha_vence, INTERVAL 1 MONTH)) fecha_siguiente_a
                from pago where rut=? order by id_pago desc limit 0,1";
                $query=$this->con->prepare($sql);
                $query->execute(array($rut));
                $result=$query->fetchAll(PDO::FETCH_ASSOC);
			if(isset($result[0]))
				return $result[0];
			else
				return false;
		}
		*/
		
	function set_borrado($rut, $id_pago, $borrado) {
		$borrado_sql='?';
		if($borrado=='')
			$borrado_sql='null';
		
		
		$sql="update pago set borrado=$borrado_sql where rut=? and id_pago=?";
		$query=$this->con->prepare($sql);
		if($borrado=='')
			$query->execute(array($rut, $id_pago));
		else
			$query->execute(array($borrado, $rut, $id_pago));
			
	}
	
	function obtener_datos_basicos($rut, $id, $borrado=''){
		$borrado_sql='is null';
		if($borrado=='1')
			$borrado_sql='=1'; //que muestre el borrado
		$sql="select * from pago where rut=? and id_pago=? and borrado $borrado_sql ";

		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		// echo $sql;
		if(isset($result[0]))
			return $result[0];
		else
			return false; //no encontrado
	}
	
	
	function eliminar_pago($rut, $id_pago) {
		$sql="delete from pago where rut=? and id_pago=?";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id_pago)); 
	}
	
	function set_usuario($usuario, $valor) {
		$valor_sql='?';
		if($valor=='')
			$valor_sql='null';
		
		
		$sql="update pago set usuario=$valor_sql where usuario=?";
		$query=$this->con->prepare($sql);
		if($valor=='')
			$query->execute(array($usuario));
		else
			$query->execute(array($valor, $usuario));
			
		//print_r($query->errorInfo());
	}
	
}