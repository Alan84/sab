<?php

class est_civil{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_estados_civiles() {
		$datos=array();
		$sql="select * from est_civil";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_est_civil']]=$row['descripcion'];
		return $datos;
	}
}
