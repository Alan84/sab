<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Actualizar parametros') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:50-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar Cambios</h1>
<form method="post" action="actualizar_parametros.php">
	<table>
		<tr>
			<td>Confirme actualización de los parametros</td>
		</tr>
	</table>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	
	<p style="text-align: right"><input type="submit" name="enviar" value="Confirmar" /></p>
</form>
<form method="post" action="parametros.php">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<p style="text-align: right"><input type="submit" name="cancelar" value="Cancelar" /></p>
</form>
</div>
</div>
</body>
</html>