<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');

if(isset($_POST['enviar']) and $_POST['enviar']=='Actualizar'){
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$t_parametro = new parametro($con);
	$parametros = $t_parametro->obtener_lista_parametros();
	$errores=verificar_errores($parametros);
	
		
	if($errores==array()){
		//foreach($parametros as $param)
		//	$t_parametro->actualizar_parametro($param['nombre'], trim($_POST[$param['nombre']]));
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_actualizar_parametros.php';
	}
	else{	
		$enlace_volver='parametros.php';
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
//else if(isset($_POST['enviar']) and $_POST['enviar']=='Eliminar'){
//	include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_eliminar_usuario.php';
//}
else {
	header('location: parametros.php');
}

function verificar_errores($parametros) {
	$errores=array();
	
	foreach($parametros as $param){
		if(trim($_POST[$param['nombre']])=='')
			$errores[]='Campo '.$param['nombre'].' es requerido';
	}	
	return $errores;
}