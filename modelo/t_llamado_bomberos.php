<?php

class llamado_bomberos{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function guardar_bombero($rut, $id_llamado) {
		$sql="insert into llamado_bomberos (rut, id_llamado) values
						(?, ?)";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id_llamado)); 
	}
	
	function eliminar_llamado($id_llamado) {
		$sql="delete from llamado_bomberos where id_llamado=?";

		$q=$this->con->prepare($sql);
		$q->execute(array($id_llamado)); 
	}
	
	function obtener_datos($id, $borrado=''){
		//$borrado_sql=' is null';
		//if($borrado=='1')
		//	$borrado_sql=' =1'; //que muestre el borrado
		$sql="select * from llamado_bomberos where id_llamado=?";

		$query=$this->con->prepare($sql);
		$query->execute(array($id));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result;
		else
			return false; //no encontrado
	}
}
