<?php

class cargo_hst{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}

	function existe_cargo($rut, $id) {
		$sql="select 1 from cargo_hst where rut=? and id_cargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if($query->rowCount())
			return true;
		else
			return false;
	}

	function cuenta_cargos($rut) {
		$sql="select count(*) from cargo_hst where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
	
	function obtener_ultimo_id($rut) {
		$sql="select id_cargo from cargo_hst where rut=? order by id_cargo desc limit 1";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
	
	/* $_POST['rut'], $id, $cargo['id_tipo_cargo'], $cargo['fecha_inicio_a'].'-'$cargo['fecha_inicio_m'].'-'$cargo['fecha_inicio_d'], 
				 		$cargo['fecha_termino_a'].'-'$cargo['fecha_termino_m'].'-'$cargo['fecha_termino_d'], $cargo['descripcion'] */
	function guardar_cargo($rut, $id, $id_cod_cargo, $fecha_inicio, $fecha_termino, $descripcion) {
		
		$desc_null='?';
		if(trim($descripcion)=='')
			$desc_null='NULL';
		$sql="insert into cargo_hst (rut, id_cargo, id_cod_cargo, fecha_inicio, fecha_termino, descripcion) values						
						(?, ?, ?, ?, ?, $desc_null)";
						
		$q=$this->con->prepare($sql);
		if(trim($descripcion)=='')
			$q->execute(array($rut, $id, $id_cod_cargo, $fecha_inicio, $fecha_termino));
		else
			$q->execute(array($rut, $id, $id_cod_cargo, $fecha_inicio, $fecha_termino, $descripcion));
	}

	function actualizar_cargo($rut, $id, $id_cod_cargo, $fecha_inicio, $fecha_termino, $descripcion) {		
		$desc_null='?';
		if(trim($descripcion)=='')
			$desc_null='NULL';
			
		$sql="update cargo_hst set id_cod_cargo=?, fecha_inicio=?, fecha_termino=?, descripcion=$desc_null 
						where rut=? and id_cargo=?";
						
		$q=$this->con->prepare($sql);
		if(trim($descripcion)=='')
			$q->execute(array($id_cod_cargo, $fecha_inicio, $fecha_termino, $rut, $id));
		else
			$q->execute(array($id_cod_cargo, $fecha_inicio, $fecha_termino, $descripcion, $rut, $id));
	}
	
	function borrar_cargo($rut, $id) {
		$sql="delete from cargo_hst where rut=? and id_cargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
	}
	
	function ordenar_cargos($rut) {
		$i=1;
		foreach($this->obtener_ids_cargo_asc($rut) as $id){
			if(!$this->existe_cargo($rut, $i))
				$this->set_id_cargo($rut, $id['id_cargo'], $i);
			$i++;
		}
		
		//id_cargo 1 i 1
		//id_cargo 4 i 2
		//id_cargo 5 i 3
	}
	
	function set_id_cargo($rut, $id, $id_nuevo){
		$sql="update cargo_hst set id_cargo=? where rut=? and id_cargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id_nuevo, $rut, $id));
	}
	
	function obtener_ids_cargo_asc($rut) {
		$sql="select id_cargo from cargo_hst where rut=? order by id_cargo asc";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll();
	}
}