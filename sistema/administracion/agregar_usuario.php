<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_usuario = new usuario($con);
$t_tipo_usuario = new tipo_usuario($con);

$usuarios = $t_usuario->obtener_lista_usuarios();
$tipos_usuarios = $t_tipo_usuario->obtener_tipos_usuarios();

$form['uname'] = '';
$form['upass'] = '';
$form['tipo_usuario'] = '';
//$datos_basic_b = $t_bombero->obtener_datos_basicos($_POST['rut']);
$form['rut'] = '';
$form['rut_dv'] = '';
$habilitado_checked = '';
if(isset($_POST['uname'])) {
	$form['uname'] = $_POST['uname'];
	$form['descripcion_tipo_usuario'] = $t_tipo_usuario->obtener_descripcion($_POST['tipo_usuario']);
	//$datos_basic_b = $t_bombero->obtener_datos_basicos($_POST['rut']);
	$form['tipo_usuario'] = $_POST['tipo_usuario'];
	$form['rut'] = $_POST['rut'];
	$form['rut_dv'] = $_POST['rut_dv'];
	$form['habilitado'] = 0;
	if(isset($_POST['habilitado']))
		$form['habilitado'] = 1;
	
	if($form['habilitado']==1)
		$habilitado_checked = 'checked="checked"';
	//$form['nombre']= $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];
}

include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_agregar_usuario.php';
