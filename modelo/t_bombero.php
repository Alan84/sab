<?php

class bombero{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function existe_bombero($rut) {
		$sql="select 1 from bombero where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		if($query->rowCount())
			return true;
		else
			return false;
	}

	function obtener_bombero($rut) {
		$sql="select * from bombero where rut=? and borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0];
		else 
			return array();
	}

	function obtener_rut_dv($rut) {
		$sql="select rut_dv from bombero where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['rut_dv'];
		else
			return false;
	}

	function obtener_combo_bomberos($estados=array(), $datos_iniciales=array()){
		if($estados==array()){
			$sql="select nombre, apellido, rut, rut_dv from bombero where borrado is null";
			$query=$this->con->prepare($sql);
			$query->execute();
		}
		else {
			$in  = str_repeat('?,', count($estados) - 1) . '?';
			$sql="select nombre, apellido, rut, rut_dv from bombero where id_est_bombero in ($in) and borrado is null ";
			$query=$this->con->prepare($sql);
			$query->execute($estados);
		}
		
		//print_r($estados);
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		$datos_combo=array();
		if($datos_iniciales!=array())
			$datos_combo=$datos_iniciales;

		foreach($result as $dato)
			$datos_combo[]=array($dato['rut'], $dato['nombre'].' '.$dato['apellido'].' '.number_format($dato['rut'],0,',','.').'-'.$dato['rut_dv']);
			
		return $datos_combo;
	}

	function obtener_checkbox_bomberos($estados=array()) {
		if($estados==array()){
			$sql="select nombre, apellido, rut, rut_dv from bombero where borrado is null";
			$query=$this->con->prepare($sql);
			$query->execute();
		}
		else {
			$in  = str_repeat('?,', count($estados) - 1) . '?';
			$sql="select nombre, apellido, rut, rut_dv from bombero where id_est_bombero in ($in) and borrado is null ";
			$query=$this->con->prepare($sql);
			$query->execute($estados);
		}
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	function obtener_datos_basicos($rut, $borrado='') {
		$borrado_sql = " and borrado=1 ";
		
		if($borrado=='')
			$borrado_sql = ' and borrado is null';
		
		$sql="select nombre, apellido, rut_dv from bombero where rut=? $borrado_sql ";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		
		if(isset($result[0]))
			return $result[0];
		else
			return false;
	}

	function obtener_nombre_completo($rut) {
		$sql="select nombre, apellido from bombero where rut=? and borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		
		if(isset($result[0]))
			return $result[0]['nombre'].' '.$result[0]['apellido'];
		else		
			return false;
	}

	function hay_foto($rut) {
		$sql="select foto from bombero where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]['foto']))
			return true;
		else
			return false;
	}

	function guardar_bombero($rut, $rut_dv, $nombre, $apellido, $profesion, $sexo,
		$id_est_civil, $grup_san, $direc_calle, $direc_numero, $direc_comuna, $direc_region, 
		$direc_depto, $email, $email2, $fono, $fono2, $fecha_nac, $fecha_ingreso, 
		$id_est_bombero, $num_registro, $num_tib, $num_placa, $nombre_padre,	$nombre_madre, $comentarios, $usuario_actualiza) {
		
		$rut_dv_sql=$nombre_sql=$apellido_sql=$sexo_sql=$profesion_sql=$id_est_civil_sql=$grup_san_sql=
				$direc_calle_sql=$direc_numero_sql=$direc_comuna_sql=$direc_region_sql=$direc_depto_sql=
				$email_sql=$email2_sql=$fono_sql=$fono2_sql=$fecha_nac_sql=$fecha_ingreso_sql=
				$id_est_bombero_sql=$num_registro_sql=$num_tib_sql=$num_placa_sql=$nombre_padre_sql=$nombre_madre_sql=
				$comentarios_sql=$foto_sql=$usuario_actualiza_sql=$rut_sql='?';
		if($rut_dv=='')
				$rut_dv_sql='null';
			if($nombre=='')
				$nombre_sql;
			if($apellido=='')
				$apellido_sql='null';
			if($sexo=='')
				$sexo_sql='null';
			if($profesion=='')
				$profesion_sql='null';
	 		if($id_est_civil=='')
	 			$id_est_civil_sql='null';
	 		if($grup_san=='')
	 			$grup_san_sql='null';
	 		if($direc_calle=='')
	 			$direc_calle_sql='null';
	 		if($direc_numero=='')
	 			$direc_numero_sql='null';
	 		if($direc_comuna=='')
	 			$direc_comuna_sql='null';
	 		if($direc_region=='')
	 			$direc_region_sql='null';
	 		if($direc_depto=='')
	 			$direc_depto_sql='null';
			if($email=='')
				$email_sql='null';
			if($email2=='')
				$email2_sql='null';
			if($fono=='')
				$fono_sql='null';
			if($fono2=='')
				$fono2_sql='null';
			if($fecha_nac=='')
				$fecha_nac_sql='null';
			if($fecha_ingreso=='')
				$fecha_ingreso_sql='null';
			if($id_est_bombero=='')
				$id_est_bombero_sql='null';
			if($num_registro=='')
				$num_registro_sql='null';
			if($num_tib=='')
				$num_tib_sql='null';
			if($num_placa=='')
				$num_placa_sql='null';
			if($nombre_padre=='')
				$nombre_padre_sql='null';
			if($nombre_madre=='')
				$nombre_madre_sql='null';
			if($comentarios=='')
				$comentarios_sql='null';
			//if($foto=='') //no hay fotos cuando se ingresa por primera vez al bombero
				//$foto_sql='null';
			if($usuario_actualiza=='')
				$usuario_actualiza_sql='null';
				
 		$sql="insert into bombero (rut, rut_dv, nombre, apellido, sexo, profesion, id_est_civil,
				grup_san, direc_calle, direc_numero, direc_comuna, direc_region, direc_depto,
				email, email2, fono, fono2, fecha_nac, fecha_ingreso,
				fecha_actualiza, id_est_bombero, num_registro, num_tib, num_placa, nombre_padre, nombre_madre, comentarios, usuario_actualiza) 
				values ( $rut_sql ,$rut_dv_sql , $nombre_sql , $apellido_sql ,
				$sexo_sql , $profesion_sql , $id_est_civil_sql , $grup_san_sql ,$direc_calle_sql , $direc_numero_sql , 
				$direc_comuna_sql , $direc_region_sql , $direc_depto_sql ,$email_sql , $email2_sql , $fono_sql , $fono2_sql ,
				$fecha_nac_sql , $fecha_ingreso_sql , now() , $id_est_bombero_sql , $num_registro_sql , $num_tib_sql , 
				$num_placa_sql , $nombre_padre_sql , $nombre_madre_sql , $comentarios_sql , $usuario_actualiza_sql )";

		//echo $sql;
		
//	include('v_guardar_bombero.php');
		//echo $num_registro;
		$q=$this->con->prepare($sql);
		
		$array_datos_query=array();
			
			
			$array_datos_query[]=$rut;
			
			if($rut_dv!='')
				$array_datos_query[]=$rut_dv;
			if($nombre!='')
				$array_datos_query[]=strtoupper(trim($nombre));
			if($apellido!='')
				$array_datos_query[]=strtoupper(trim($apellido));
			if($sexo!='')
				$array_datos_query[]=strtoupper(trim($sexo));
			if($profesion!='')
				$array_datos_query[]=strtoupper(trim($profesion));
	 		if($id_est_civil!='')
	 			$array_datos_query[]=$id_est_civil;
	 		if($grup_san!='')
	 			$array_datos_query[]=strtoupper(trim($grup_san));
	 		if($direc_calle!='')
	 			$array_datos_query[]=strtoupper(trim($direc_calle));
	 		if($direc_numero!='')
	 			$array_datos_query[]=$direc_numero;
	 		if($direc_comuna!='')
	 			$array_datos_query[]=strtoupper(trim($direc_comuna));
	 		if($direc_region!='')
	 			$array_datos_query[]=$direc_region;
	 		if($direc_depto!='')
	 			$array_datos_query[]=$direc_depto;
			if($email!='')
				$array_datos_query[]=strtoupper(trim($email));
			if($email2!='')
				$array_datos_query[]=strtoupper(trim($email2));
			if($fono!='')
				$array_datos_query[]=$fono;
			if($fono2!='')
				$array_datos_query[]=$fono2;
			if($fecha_nac!='')
				$array_datos_query[]=$fecha_nac;
			if($fecha_ingreso!='')
				$array_datos_query[]=$fecha_ingreso;
			if($id_est_bombero!='')
				$array_datos_query[]=$id_est_bombero;
			if($num_registro!='')
				$array_datos_query[]=$num_registro;
			if($num_tib!='')
				$array_datos_query[]=$num_tib;
			if($num_placa!='')
				$array_datos_query[]=$num_placa;
			if($nombre_padre!='')
				$array_datos_query[]=strtoupper(trim($nombre_padre));
			if($nombre_madre!='')
				$array_datos_query[]=strtoupper(trim($nombre_madre));
			if($comentarios!='')
				$array_datos_query[]=$comentarios;
			if($usuario_actualiza!='')
				$array_datos_query[]=$usuario_actualiza;

		
//print_r($array_datos_query);			
			$q->execute($array_datos_query);
			//echo $sql."\n";
			//print_r($array_datos_query);
		/* $q->execute(array(':rut'=>$rut, ':rut_dv'=>$rut_dv,':nombre'=>strtoupper(trim($nombre),
			':apellido'=>strtoupper(trim($apellido), ':profesion'=>strtoupper(trim($profesion), ':sexo'=>$sexo,
			':id_est_civil'=>$id_est_civil,
			':grup_san'=>strtoupper(trim($grup_san), ':direc_calle'=>strtoupper(trim($direc_calle), ':direc_numero'=>$direc_numero,
			':direc_comuna'=>strtoupper(trim($direc_comuna), ':direc_region'=>$direc_region,
			':direc_depto'=>strtoupper(trim($direc_depto), ':email'=>strtoupper(trim($email), ':email2'=>strtoupper(trim($email2),
			':fono'=>$fono, ':fono2'=>$fono2,
			':fecha_nac'=>$fecha_nac, ':fecha_ingreso'=>$fecha_ingreso,
			':id_est_bombero'=>$id_est_bombero, ':num_registro'=> $num_registro, ':num_tib'=>$num_tib,
			':num_placa'=>$num_placa, ':nombre_padre'=>strtoupper(trim($nombre_padre), ':nombre_madre'=>strtoupper(trim($nombre_madre), ':usuario_actualiza'=>$usuario_actualiza));
			*/
	}

	function actualizar_bombero($rut, $rut_dv, $nombre, $apellido, $profesion, $sexo,
		$id_est_civil, $grup_san, $direc_calle, $direc_numero, $direc_comuna, $direc_region,
		$direc_depto, $email, $email2, $fono, $fono2, $fecha_nac, $fecha_ingreso,
		$id_est_bombero, $num_registro, $num_tib, $num_placa, $nombre_padre, $nombre_madre, $comentarios, $foto, $usuario_actualiza) {
		if(FALSE){ //sin cargo
	 		$sql="update bombero set rut_dv=?, nombre=?, apellido=?, sexo=?, profesion=?, id_est_civil=?,
				grup_san=?, direc_calle=?, direc_numero=?, direc_comuna=?, direc_region=?, direc_depto=?,
				email=?, email2=?, tel_movil=?, tel_fijo=?, fecha_nac=?, fecha_ingreso=?,
				fecha_actualiza=NOW(), id_est_bombero=?, num_registro=?, 
				num_tib=?, num_placa=?, nombre_padre=?, nombre_madre=?, comentarios=?, foto=?, usuario_actualiza=?
				where rut=?";

			$q=$this->con->prepare($sql);
			$q->execute(array($rut_dv,strtoupper(trim($nombre)),
				strtoupper(trim($apellido)), $sexo,strtoupper(trim($profesion)), 
				$id_est_civil,
				$grup_san, strtoupper(trim($direc_calle)), $direc_numero,
				strtoupper(trim($direc_comuna)), $direc_region,
				$direc_depto, strtoupper(trim($email)), strtoupper(trim($email2)),
				$tel_movil, $tel_fijo,
				$fecha_nac, $fecha_ingreso,
				$id_est_bombero, $num_registro, $num_tib,
				$num_placa, strtoupper(trim($nombre_padre)), strtoupper(trim($nombre_madre)), $comentarios, $foto, $usuario_actualiza, $rut));
		}
		else{ //con cargo
				//todos tendran un ? por defecto, si no existe el dato, entonces se reemplazará por un null
				$rut_dv_sql=$nombre_sql=$apellido_sql=$sexo_sql=$profesion_sql=$id_est_civil_sql=$grup_san_sql=
				$direc_calle_sql=$direc_numero_sql=$direc_comuna_sql=$direc_region_sql=$direc_depto_sql=
				$email_sql=$email2_sql=$fono_sql=$fono2_sql=$fecha_nac_sql=$fecha_ingreso_sql=
				$id_est_bombero_sql=$num_registro_sql=$num_tib_sql=$num_placa_sql=$nombre_padre_sql=$nombre_madre_sql=
				$comentarios_sql=$foto_sql=$usuario_actualiza_sql=$rut_sql='?';
								
			if($rut_dv=='')
				$rut_dv_sql='null';
			if($nombre=='')
				$nombre_sql;
			if($apellido=='')
				$apellido_sql='null';
			if($sexo=='')
				$sexo_sql='null';
			if($profesion=='')
				$profesion_sql='null';
	 		if($id_est_civil=='')
	 			$id_est_civil_sql='null';
	 		if($grup_san=='')
	 			$grup_san_sql='null';
	 		if($direc_calle=='')
	 			$direc_calle_sql='null';
	 		if($direc_numero=='')
	 			$direc_numero_sql='null';
	 		if($direc_comuna=='')
	 			$direc_comuna_sql='null';
	 		if($direc_region=='')
	 			$direc_region_sql='null';
	 		if($direc_depto=='')
	 			$direc_depto_sql='null';
			if($email=='')
				$email_sql='null';
			if($email2=='')
				$email2_sql='null';
			if($fono=='')
				$fono_sql='null';
			if($fono2=='')
				$fono2_sql='null';
			if($fecha_nac=='')
				$fecha_nac_sql='null';
			if($fecha_ingreso=='')
				$fecha_ingreso_sql='null';
			if($id_est_bombero=='')
				$id_est_bombero_sql='null';
			if($num_registro=='')
				$num_registro_sql='null';
			if($num_tib=='')
				$num_tib_sql='null';
			if($num_placa=='')
				$num_placa_sql='null';
			if($nombre_padre=='')
				$nombre_padre_sql='null';
			if($nombre_madre=='')
				$nombre_madre_sql='null';
			if($comentarios=='')
				$comentarios_sql='null';
			if($foto=='')
				$foto_sql='null';
			if($usuario_actualiza=='')
				$usuario_actualiza_sql='null';
				
	 		$sql="update bombero set rut_dv=$rut_dv_sql , nombre=$nombre_sql , apellido=$apellido_sql , sexo=$sexo_sql , profesion=$profesion_sql , 
	 				id_est_civil=$id_est_civil_sql , grup_san=$grup_san_sql , direc_calle=$direc_calle_sql , direc_numero=$direc_numero_sql , 
	 				direc_comuna=$direc_comuna_sql , direc_region=$direc_region_sql , direc_depto=$direc_depto_sql ,
				email=$email_sql , email2=$email2_sql , fono=$fono_sql , fono2=$fono2_sql , fecha_nac=$fecha_nac_sql , 
				fecha_ingreso=$fecha_ingreso_sql , fecha_actualiza=NOW(), id_est_bombero=$id_est_bombero_sql , num_registro=$num_registro_sql ,
				num_tib=$num_tib_sql , num_placa=$num_placa_sql , nombre_padre=$nombre_padre_sql , nombre_madre=$nombre_madre_sql , 
				comentarios=$comentarios_sql , foto=$foto_sql , usuario_actualiza=$usuario_actualiza_sql 
				where rut=$rut_sql ";

//echo $sql;
			$q=$this->con->prepare($sql);
			
			$array_datos_query=array();
			
			
			if($rut_dv!='')
				$array_datos_query[]=$rut_dv;
			if($nombre!='')
				$array_datos_query[]=strtoupper(trim($nombre));
			if($apellido!='')
				$array_datos_query[]=strtoupper(trim($apellido));
			if($sexo!='')
				$array_datos_query[]=strtoupper(trim($sexo));
			if($profesion!='')
				$array_datos_query[]=strtoupper(trim($profesion));
	 		if($id_est_civil!='')
	 			$array_datos_query[]=$id_est_civil;
	 		if($grup_san!='')
	 			$array_datos_query[]=strtoupper(trim($grup_san));
	 		if($direc_calle!='')
	 			$array_datos_query[]=strtoupper(trim($direc_calle));
	 		if($direc_numero!='')
	 			$array_datos_query[]=$direc_numero;
	 		if($direc_comuna!='')
	 			$array_datos_query[]=strtoupper(trim($direc_comuna));
	 		if($direc_region!='')
	 			$array_datos_query[]=$direc_region;
	 		if($direc_depto!='')
	 			$array_datos_query[]=$direc_depto;
			if($email!='')
				$array_datos_query[]=strtoupper(trim($email));
			if($email2!='')
				$array_datos_query[]=strtoupper(trim($email2));
			if($fono!='')
				$array_datos_query[]=$fono;
			if($fono2!='')
				$array_datos_query[]=$fono2;
			if($fecha_nac!='')
				$array_datos_query[]=$fecha_nac;
			if($fecha_ingreso!='')
				$array_datos_query[]=$fecha_ingreso;
			if($id_est_bombero!='')
				$array_datos_query[]=$id_est_bombero;
			if($num_registro!='')
				$array_datos_query[]=$num_registro;
			if($num_tib!='')
				$array_datos_query[]=$num_tib;
			if($num_placa!='')
				$array_datos_query[]=$num_placa;
			if($nombre_padre!='')
				$array_datos_query[]=strtoupper(trim($nombre_padre));
			if($nombre_madre!='')
				$array_datos_query[]=strtoupper(trim($nombre_madre));
			if($comentarios!='')
				$array_datos_query[]=$comentarios;
			if($foto!='')
				$array_datos_query[]='1'; //se guarda un 1 en la base de datos, esto indica que hay foto, entonces se debe ir a buscar
			if($usuario_actualiza!='')
				$array_datos_query[]=$usuario_actualiza;

		$array_datos_query[]=$rut;
//print_r($array_datos_query);			
			$q->execute($array_datos_query);
			
		}
	}
	
	
	function set_borrado($rut, $borrado) {
		$borrado_sql='?';
		if($borrado=='')
			$borrado_sql='null';
		
		
		$sql="update bombero set borrado=$borrado_sql where rut=?";
		$query=$this->con->prepare($sql);
		if($borrado=='')
			$query->execute(array($rut));
		else
			$query->execute(array($borrado, $rut));
			
	}
	
	function obtener_email($rut) {
		$sql="select email from bombero where rut=? and borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		
		if(isset($result[0]))
			return $result[0]['email'];
		else		
			return false;
	}

	function update_fecha_mail_alerta($rut) {
		$sql="update bombero set fecha_mail_alerta=now() where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));	
	}
	
	
	function set_usuario_actualiza($usuario, $valor) {
		$valor_sql='?';
		if($valor=='')
			$valor_sql='null';
		
		
		$sql="update bombero set usuario=$valor_sql where usuario=?";
		$query=$this->con->prepare($sql);
		if($valor=='')
			$query->execute(array($usuario));
		else
			$query->execute(array($valor, $usuario));
			
	}
	
	function obtener_ultimo_agregado(){
		$sql="select rut, nombre, apellido from bombero order by fecha_actualiza desc limit 0,1"; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
		$query=$this->con->query($sql);
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0];
		else
			return false;
	}
}