<?php

class cod_anio{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_anios_servicios() {
		$datos=array();
		$sql="select * from cod_anio";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_cod_anio']]=$row['cant_anios'];
		return $datos;
	}
}
