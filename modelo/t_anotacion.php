<?php

class anotacion{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function existe_anotacion($rut, $id) {
		$sql="select 1 from anotacion where rut=? and id_anotacion=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if($query->rowCount())
			return true;
		else
			return false;
	}
	
	function guardar_anotacion($rut, $id, $fecha, $descripcion, $id_tipo_anotacion) {
		$sql="insert into anotacion (rut, id_anotacion, fecha, descripcion, id_tipo_anotacion) values
						(?, ?, ?, ?, ?)";
		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id, $fecha, $descripcion,$id_tipo_anotacion));
	}
	
	function actualizar_anotacion($rut, $id, $fecha, $descripcion,$id_tipo_anotacion) {
		$sql="update anotacion set fecha=?, descripcion=?, id_tipo_anotacion=?
						where rut=? and id_anotacion=?";
		$q=$this->con->prepare($sql);
		$q->execute(array($fecha, $descripcion,$id_tipo_anotacion, $rut, $id));
	}
	
	function borrar_anotacion($rut, $id) {
		$sql="delete from anotacion where rut=? and id_anotacion=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
	}
	
	function obtener_ultimo_id($rut) {
		$sql="select id_anotacion from anotacion where rut=? order by id_anotacion desc limit 1";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
	
	function ordenar_anotaciones($rut) {
		$i=1;
		foreach($this->obtener_ids_anotacion_asc($rut) as $id){
			if(!$this->existe_anotacion($rut, $i))
				$this->set_id_anotacion($rut, $id['id_anotacion'], $i);
			$i++;
		}
	}
	
	function set_id_anotacion($rut, $id, $id_nuevo){
		$sql="update anotacion set id_anotacion=? where rut=? and id_anotacion=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id_nuevo, $rut, $id));
	}
	
	function obtener_ids_anotacion_asc($rut) {
		$sql="select id_anotacion from anotacion where rut=? order by id_anotacion asc";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll();
	}
}
