<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Bombero') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-06T00:47:22-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
</head>
<body>
<div>
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
	<p style="text-align: right; margin: 0em">
		<a href="ver_bombero.php?rut=<?php echo $info['rut']; ?>" class="tab_elegido">ver bombero</a>
		<a href="editar_bombero.php?rut=<?php echo $info['rut']; ?>" class="tab">editar datos personales</a>
		<a href="editar_vida_bombero.php?rut=<?php echo $info['rut']; ?>" class="tab">editar hoja de vida</a>
	</p>
<div class="tabla" style="margin-top: 0em; z-index: 0">
<h2>Datos personales</h2>
	<img src="<?php echo DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$foto?>" alt="Foto del bombero" style="float: right; margin: 1em 3em 0 1em;" />
	<table>
	<tr><td>nombres:</td><td><?php echo $info['nombre'] ?></td></tr>
	<tr><td>apellidos:</td><td><?php echo $info['apellido'] ?></td></tr>
	<tr><td>rut:</td><td><?php echo $info['rut'] ?>-<?php echo $info['rut_dv'] ?></td></tr>
	<tr><td style="vertical-align: top">sexo:</td><td>
	<?php if ($info['sexo']=='M') echo 'Masculino'; else if($info['sexo']=='F') echo 'Femenino'; ?>
	</td></tr>
	<tr>
	<td>nombre padre:</td><td><?php echo $info['nombre_padre'] ?></td></tr>
	<tr><td>nombre madre:</td><td><?php echo $info['nombre_madre'] ?></td></tr>
	<tr><td>profesion:</td><td><?php echo $info['profesion'] ?></td></tr>
	<tr><td>estado civil:</td><td><?php echo $info['est_civil_descripcion'] ?></td></tr>
	<tr><td>grupo sanguineo:</td><td><?php echo $info['grup_san'] ?></td></tr>
	<tr><td style="vertical-align: top">direccion:</td>
		<td><?php echo $info['direc_depto']?><?php echo $info['direc_calle'] ?> <?php echo $info['direc_numero']?>, <?php echo $info['direc_comuna'] ?>, <?php echo $t_cod_region->obtener_descripcion($info['direc_region']); ?></td>
	<tr><td>telefono :</td><td><?php echo $info['fono'] ?></td></tr>
	<tr><td>telefono2:</td><td><?php echo $info['fono2'] ?></td></tr>
	<tr><td>email:</td><td><?php echo $info['email'] ?></td></tr>
	<tr><td>email2:</td><td><?php echo $info['email2'] ?></td></tr>
	<tr><td>fecha de nacimiento:</td>
<td><?php echo $info['fecha_nac'] ?></td>
<tr><td>fecha de ingreso:</td>
<td><?php echo $info['fecha_ingreso'] ?></td>
</tr>
<tr><td>Número registro:</td><td><?php echo $info['num_registro'] ?></td></tr>
<tr><td>Número placa:</td><td><?php echo $info['num_placa'] ?></td></tr>
<tr><td>Número tib:</td><td><?php echo $info['num_tib'] ?></td></tr>
<tr><td>Estado:</td><td><?php echo $info['est_bombero_descripcion'] ?></td>
<tr><td>Cargo actual:</td>

<td>
<ul>
<?php foreach($cargos_act as $carg){ ?>
	<li><?php echo $carg['descripcion'].' <span>- Inicio:</span>'.$carg['fecha_inicio']; ?> </li>
<?php } ?>
</ul>

</td>

<tr><td>Comentarios adicionales:</td><td><?php echo $info['comentarios'] ?></td>
</tr>
</table>
<h2>Premios años de servicio:</h2>
<?php if(count($premio_anios)>0){ ?>
<table>
	<thead>
		<tr>
			<th>Años</th>
			<th>Premio compañía</th>
			<th>Premio cuerpo</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($premio_anios as $anio){ ?>
	<tr>
		<td><?php echo $anio['anios']; ?></td>
		<td><?php echo $anio['anio_premio_cia'];?></td>
		<td><?php echo $anio['anio_premio_cuerpo'];?></td>
	</tr>
	<?php } ?>
	</tbody>
</table>
<?php } else { ?>
<p><em>Sin premios</em></p>
<?php } ?>
<h2>Anotaciones:</h2>
<?php if(count($anotaciones)>0){ ?>
	<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Tipo</th>
			<th>Fecha</th>
			<th>Descripción</th>
		</tr>
	</thead>
	<tbody id="anotacion">
		<?php foreach($anotaciones as $row){ ?>
		<tr>
			<td style="vertical-align: top"><?php echo $row['id_anotacion'] ?></td>
			<td style="vertical-align: top"><?php echo $row['tipo_anotacion_descripcion'] ?></td>
			<td style="vertical-align: top"><?php echo $row['fecha'] ?></td>
			<td><?php echo $row['descripcion'] ?></td>
		</tr>
		<?php } ?>
	</tbody>
	</table>
<?php } else { ?>
<p><em>Sin anotaciones</em></p>
<?php } ?>
<h2>Cursos:</h2>
<?php if(count($cursos)>0){ ?>
	<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Tipo</th>
			<th>Fecha</th>
			<th>Descripción</th>
			<th>Certificado</th>
		</tr>
	</thead>
	<tbody id="curso">
		<?php foreach($cursos as $row){ ?>
		<tr>
			<td style="vertical-align: top"><?php echo $row['id_curso'] ?></td>
			<td style="vertical-align: top"><?php echo $row['tipo_curso_descripcion'] ?></td>
			<td style="vertical-align: top"><?php echo $row['fecha'] ?></td>
			<td><?php echo $row['descripcion'] ?></td>
			<td>
				<?php if(is_file(DIRECTORIO_WEB. DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.sprintf(NOM_ARCHIVO_CERTIFICADO, $info['rut'], $row['id_curso']).'.'.strtolower($row['certificado'])))
						echo '<a href="'.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.sprintf(NOM_ARCHIVO_CERTIFICADO, $info['rut'], $row['id_curso']).'.'.strtolower($row['certificado']).'">'.sprintf(NOM_ARCHIVO_CERTIFICADO, $info['rut'], $row['id_curso']).'.'.strtolower($row['certificado']).'</a>'; ?>			
			</td></td>
		</tr>
		<?php } ?>
	</tbody>
	</table>
<?php } else { ?>
<p><em>Sin cursos</em></p>
<?php } ?>
<h2>Historico de Cargos:</h2>
<?php if(count($cargos)>0){ ?>
	<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Tipo</th>
			<th>Fecha inicio</th>
			<th>Fecha termino</th>
			<th>Descripcion</th>
		</tr>
	</thead>
	<tbody id="curso">
		<?php foreach($cargos as $row){ ?>
		<tr>
			<td style="vertical-align: top"><?php echo $row['id_cargo'] ?></td>
			<td style="vertical-align: top"><?php echo $row['cod_cargo_descripcion'] ?></td>
			<td style="vertical-align: top"><?php echo $row['fecha_inicio'] ?></td>
			<td style="vertical-align: top"><?php echo $row['fecha_termino'] ?></td>
			<td style="vertical-align: top"><?php echo $row['descripcion'] ?></td>
		</tr>
		<?php } ?>
	</tbody>
	</table>
<?php } else { ?>
<p><em>Sin histórico de cargos</em></p>
<?php } ?>
</div>
</div>
</body>
</html>