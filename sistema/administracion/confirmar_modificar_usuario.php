<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_usuario = new usuario($con);
if(isset($_POST['enviar']) and $_POST['enviar']=='Modificar'){
	$uname = $_SESSION['uname'];	
	$errores=verificar_errores();
	$hay_cambio_tipo_usuario=0;
	$hay_cambio_password =0;
	$hay_cambio_habilitado =0;
	
	$t_bombero = new bombero($con);
	$rut =$t_usuario->obtener_rut($uname);
	
	$tipo_usuario_bd=$t_usuario->obtener_tipo($uname);
	$form['tipo_usuario'] = $_POST['tipo_usuario'];
	if(trim($form['tipo_usuario'])!='' and $form['tipo_usuario']!=$tipo_usuario_bd)
		$hay_cambio_tipo_usuario = 1;
		
	if(trim($_POST['upass'])!='' and hash('sha256', $_POST['upass'])!=$t_usuario->obtener_clave($_SESSION['uname'])) //obtenemos el password de la base de datos, este se encuentra cifrado.
		$hay_cambio_password = 1;
		
	$habilitado_db = 0;
	if($t_usuario->usuario_habilitado($_SESSION['uname']))
		$habilitado_db = 1;
	
	//si se habilitó, pero en la db no sale habilitado, o si no se habilitó y en la db si sale como habilitado. entonces hay cambio	
	if((isset($_POST['habilitado']) and $_POST['habilitado']==1 and $habilitado_db!=1) or (!isset($_POST['habilitado']) and $habilitado_db==1))
		$hay_cambio_habilitado = 1;
		
	if($hay_cambio_tipo_usuario == 0 and $hay_cambio_password == 0 and $hay_cambio_habilitado == 0)
		$errores[]='No se han seleccionado cambios';
		
	if($errores==array()){
		$t_tipo_usuario = new tipo_usuario($con);
		
		//$form['uname'] = $_POST['uname'];
		$descripcion_tipo_usuario = $t_tipo_usuario->obtener_descripcion($form['tipo_usuario']);
		$descripcion_tipo_usuario_bd = $t_tipo_usuario->obtener_descripcion($tipo_usuario_bd);
		$rut_dv = '';
		$nombre = '';
		$rutcompleto = '';
		if($rut!=''){
			$datos_basic_b = $t_bombero->obtener_datos_basicos($rut);
			//$form['rut'] = $_POST['rut'];

			$rut_dv = $datos_basic_b['rut_dv'];
		
			$nombre = $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];
			$rutcompleto = $rut.'-'.$rut_dv;
		}
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_modificar_usuario.php';
	}
	else{	
		$enlace_volver='modificar_usuario.php?uname='.$uname;
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else if(isset($_POST['enviar']) and $_POST['enviar']=='Eliminar'){
	$uname = $_SESSION['uname'];
	if($t_usuario->es_administrador($uname)==false){
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_eliminar_usuario.php';
	}
	else{
		$errores=array('No se puede eliminar a un usuario administrador.');	
		$enlace_volver='modificar_usuario.php?uname='.$uname;
		include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
	}
}
else {
	header('location: usuarios.php');
}

function verificar_errores() {
	$errores=array();
	//if(trim($_POST['tipo_usuario'])=='')
		//$errores[]='Campo tipo usuario es requerido';	
	return $errores;
}