<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Errores') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T20:55:56-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Errores</h1>
<form method="post" action="<?php echo $enlace_volver ?>">
	<ul>
	<?php foreach($errores as $error){ ?>
		<li><?php echo $error ?></li>
	<?php } ?>
	</ul>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<?php if(is_array($_POST[$name][$name2])) {?>
					<?php foreach($_POST[$name][$name2] as $name3=>$dato3){ ?>
						<input type="hidden" name="<?php echo $name.'['.$name2.']['.$name3.']';?>" value="<?php echo $dato3 ?>" />
					<?php } ?>
				<?php } else {?>
					<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
				<?php } ?>		
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>
	<input type="submit" value="Volver" />
</form>
</div>
</div>
</body>
</html>