<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Cargos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:50-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}
	
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<h1 style="color: white; background-color: black">Lista de cargos actuales</h1>
<form method="post" action="confirmar_cargos.php">
<table class="tabla">
<thead>
<tr>
	<th rowspan="2">cargo</th>
	<th colspan="3">bombero actual</th>
	<th colspan="2">cambiar por</th>
</tr>
<tr>
	<th>inicio</th>
	<th>rut</th>
	<th>nombre</th>
	<th>bombero</th>
	<th>inicio</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($cargos as $cargo){ ?>
	<tr>
		<td><?php echo $cargo['descripcion']; $rut_cargo=$m_cargos->obtener_rut_cargo_actual($cargo['id_cod_cargo']);?></td>
		<td style="text-align: right"><?php echo $t_cargo_actual->obtener_fini_cargo_actual($cargo['id_cod_cargo'],'d').' '.$meses[$t_cargo_actual->obtener_fini_cargo_actual($cargo['id_cod_cargo'],'m')].' '.$t_cargo_actual->obtener_fini_cargo_actual($cargo['id_cod_cargo'],'y'); ?></td>
		<td style="text-align: right"><?php echo $rut_cargo ?></td>
		<td style="text-align: right"><?php echo $m_cargos->obtener_nombre_cargo_actual($cargo['id_cod_cargo']); ?></td>
		<td style="text-align: center">
			<select name="rut[<?php echo $cargo['id_cod_cargo']; ?>]">
				<?php foreach($cbo_bomberos as $bombero){ ($form['rut'][$cargo['id_cod_cargo']]==$bombero[0])? $selected='selected="selected"':$selected='';?>
					<option value="<?php echo $bombero[0]; ?>" <?php echo $selected; ?>><?php echo $bombero[1]; ?></option>
				<?php } ?>
			</select>
		</td>
		<td style="text-align: right">
			<!-- <input type="number" name="fecha_inicio_d[<?php echo $cargo['id_cod_cargo']; ?>]" value="<?php echo $form['fecha_inicio_d'][$cargo['id_cod_cargo']]; ?>" min="1" max="31" class="amarillo" style="width: 3em" />
			<span style="color: #666"> / </span>
			<select name="fecha_inicio_m[<?php echo $cargo['id_cod_cargo']; ?>]">
				<option value="">-- selecione --</option>
				<?php // foreach($meses as $id => $mes){ ($id==$form['fecha_inicio_m'][$cargo['id_cod_cargo']])?$sel=' selected="selected"':$sel='';?>
					<option value="<?php // echo $id ?>"<?php // echo $sel ?>><?php // echo $mes ?></option>
				<?php // } ?>
			</select>
			<span style="color: #666"> / </span><input type="number" name="fecha_inicio_a[<?php echo $cargo['id_cod_cargo']; ?>]" value="<?php echo $form['fecha_inicio_a'][$cargo['id_cod_cargo']];?>" min="<?php echo date('Y')-200;?>" max="<?php echo date('Y')+1;?>" class="amarillo" style="width: 4em" />
			-->
			<input type="date" name="fini_cargo[<?php echo $cargo['id_cod_cargo']; ?>]" value="<?php echo date('Y-m-d') ?>" class="dateinicargo" />
		</td>
	</tr>
<?php } ?>
<tbody>
</table>
<em>formato de ingreso de fecha: DD-MM-AAAA (ejemplo 01-12-1999)</em>
<p style="text-align: right"><input type="submit" value="actualizar" /></p>
</form>
</div>
</div>
</body>
</html>
