<?php

class tipo_llamado
{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_llamados($datos_iniciales=array()) {
		$datos=array();
		$sql="select * from tipo_llamado";
		
		$datos_combo=array();
		if($datos_iniciales!=array())
			$datos_combo=$datos_iniciales;
		
		foreach($this->con->query($sql) as $row)
			$datos_combo[$row['cod_tipo_llamado']]=$row['cod_tipo_llamado'].' '.$row['descripcion'];
		return $datos_combo;
	}
	
}