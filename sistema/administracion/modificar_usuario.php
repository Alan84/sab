<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_bombero = new bombero($con);
$t_usuario = new usuario($con);
$t_tipo_usuario = new tipo_usuario($con);

$usuarios = $t_usuario->obtener_lista_usuarios();
$tipos_usuarios = $t_tipo_usuario->obtener_tipos_usuarios();

$uname = $_SESSION['uname'] = $_GET['uname']; //guardamos el usuario para las modificaciones posteriores.
$form['upass'] = '';
$form['tipo_usuario'] = $t_usuario->obtener_tipo($uname);
//$datos_basic_b = $t_bombero->obtener_datos_basicos($_POST['rut']);
$rut =$t_usuario->obtener_rut($uname);
$rut_dv = '';
$nombre_bombero= '';
$rutcompleto = '';
$habilitado_checked = '';
if($t_usuario->usuario_habilitado($uname))
	$habilitado_checked = 'checked="checked"';


if($rut!='') {
	$datos_basic_b = $t_bombero->obtener_datos_basicos($rut);
	$rut_dv = $datos_basic_b['rut_dv'];
	$nombre_bombero= $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];
	$rutcompleto=$rut.'-'.$rut_dv;
}

if(isset($_POST['tipo_usuario'])) {
	//$uname	 = $_SESSION['uname'];
	//$form['descripcion_tipo_usuario'] = $t_tipo_usuario->obtener_descripcion($_POST['tipo_usuario']);
	//$datos_basic_b = $t_bombero->obtener_datos_basicos($_POST['rut']);
	$form['tipo_usuario'] = $_POST['tipo_usuario'];
	if(isset($_POST['habilitado']) and $_POST['habilitado']=='1')
		$habilitado_checked = 'checked="checked"';
	
	//$rut = $t_usuario->obtener_rut($uname);
	//$rut_dv = $t_bombero->obtener_rutdv($rut);
	//$form['nombre']= $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];
}

include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_modificar_usuario.php';