<?php

class curso{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function existe_curso($rut, $id) {
		$sql="select 1 from curso where rut=? and id_curso=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if($query->rowCount())
			return true;
		else
			return false;
	}
	
	function guardar_curso($rut, $id, $fecha, $descripcion,$id_tipo_curso, $certificado) {
		$valor_certificado='null';
		$valor_descripcion='null';
		
		if(trim($descripcion)!='')
			$valor_descripcion='?';
		if(trim($certificado)!='')
			$valor_certificado='?';
		
		$sql="insert into curso (rut, id_curso, fecha, id_tipo_curso, descripcion, certificado) values
						(?, ?, ?, ?, $valor_descripcion , $valor_certificado )";

		$q=$this->con->prepare($sql);
		
		$datos_query=array($rut, $id, $fecha, $id_tipo_curso);
		if(trim($descripcion)!='')
			$datos_query[]=$descripcion;
		if(trim($certificado)!='')
			$datos_query[]=$certificado;
			
		$q->execute($datos_query); 
	}
	
	function actualizar_curso($rut, $id, $fecha, $descripcion,$id_tipo_curso, $certificado) {
		$sql_certificado = ', certificado=null ';
		$sql_descripcion = ', descripcion=null ';
		if(trim($certificado)!='')
			$sql_certificado = ', certificado=? ';
		if(trim($descripcion)!='')
			$sql_descripcion = ', descripcion=? '; 
		
		$sql="update curso set fecha=?, id_tipo_curso=? $sql_certificado $sql_descripcion 
						where rut=? and id_curso=?";
		$q=$this->con->prepare($sql);
		
		$datos_query=array($fecha, $id_tipo_curso);
		if(trim($certificado)!='')
			$datos_query[]=$certificado;
		if(trim($descripcion)!='')
			$datos_query[]=$descripcion;
			
		$datos_query[]=$rut;
		$datos_query[]=$id;
		$q->execute($datos_query);
	}
	
	function borrar_curso($rut, $id) {
		$sql="delete from curso where rut=? and id_curso=?";
		$query=$this->con->prepare($sql);
		//echo "$sql : $rut , $id ";
		//exit;
		$query->execute(array($rut, $id));
	}
	
	function obtener_ultimo_id($rut) {
		$sql="select id_curso from curso where rut=? order by id_curso desc limit 1";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchColumn();
	}
	
	function ordenar_cursos($rut) {
		$i=1;
		foreach($this->obtener_ids_curso_asc($rut) as $id){
			if(!$this->existe_curso($rut, $i))
				$this->set_id_curso($rut, $id['id_curso'], $i);
			$i++;
		}
		
		//id_cargo 1 i 1
		//id_cargo 4 i 2
		//id_cargo 5 i 3
	}
	
	function set_id_curso($rut, $id, $id_nuevo){
		$sql="update curso set id_curso=? where rut=? and id_curso=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id_nuevo, $rut, $id));
	}
	
	function obtener_ids_curso_asc($rut) {
		$sql="select id_curso, certificado from curso where rut=? order by id_curso asc";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll();
	}
	
	/** no lo usaremos......
	function obtener_extension_certificado($rut, $id) {
		$sql="select certificado from curso where rut=? and id_curso=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if($query->rowCount())
			return true;
		else
			return false;
	}
	*/
}