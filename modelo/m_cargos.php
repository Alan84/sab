<?php

class m_cargos{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}

	function obtener_nombre_cargo_actual($id) {
		$sql="select b.nombre, b.apellido from cargo_actual ca, bombero b where ca.id_cargo_actual=? and b.rut=ca.rut and b.borrado is null ";
		$query=$this->con->prepare($sql);
		$query->execute(array($id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['nombre']. ' '.$result[0]['apellido'];
		else
			return '';
	}

	function obtener_rut_cargo_actual($id) {
		$sql="select b.rut, b.rut_dv from cargo_actual ca, bombero b where ca.id_cargo_actual=? and b.rut=ca.rut and b.borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute(array($id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return number_format($result[0]['rut'],0, ',','.').'-'.$result[0]['rut_dv'];
		else
			return '';
	}

	/* $_POST['rut'], $id, $cargo['id_tipo_cargo'], $cargo['fecha_inicio_a'].'-'$cargo['fecha_inicio_m'].'-'$cargo['fecha_inicio_d'],
				 		$cargo['fecha_termino_a'].'-'$cargo['fecha_termino_m'].'-'$cargo['fecha_termino_d'], $cargo['descripcion'] */
	function guardar_cargo($rut, $id_cargo_actual, $fecha_inicio) {
		$sql="insert into cargo_actual (rut, id_cargo_actual, fecha_inicio) values
						(?, ?, ?)";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id_cargo_actual, $fecha_inicio));
	}
	
	function guardar_cargo_nuevo($rut, $id_cargo_actual, $fecha_inicio) {
		/*		
		//primero agregamos el cargo actual al histórico de cargos
		$sql="select * from cargo_actual where id_cargo_actual=?";
		
		$sql_id="select id_cargo from cargo_hst where rut=? order by id_cargo desc limit 0,1";

		$query=$this->con->prepare($sql);
		$query->execute(array($id_cargo_actual));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		
		$query_id=$this->con->prepare($sql_id);
		$query_id->execute(array($rut));
		$result_id = $query_id->fetchAll(PDO::FETCH_ASSOC);
		//print_r($result_id);
		
		$id_cargo_hst=0;//partimos en 0 si no se pudo obtener el id del query, entonces no hay resultados
		if(isset($result_id[0]))
			$id_cargo_hst=$result_id[0]['id_cargo']+1;
			
		if(isset($result[0])){
			if($result[0]['rut']!==''){
				$sql="insert into cargo_hst (rut, id_cargo, id_cod_cargo, fecha_inicio, fecha_termino, descripcion) values
								(?, ?, ?, ?, now(),?)";
				
				$q=$this->con->prepare($sql);
				
				
				$q->execute(array($result[0]['rut'], $id_cargo_hst, $result[0]['id_cargo_actual'], $result[0]['fecha_inicio'], ''));
			}			
		}
		
		echo $sql;
		echo " \n " ;
		print_r($result);
		*/
		
		
		//primero borramos el cargo actual, si existe
		$this->borrar_cargo_actual($id_cargo_actual);
		//guardamos el cargo nuevo.
		$sql="insert into cargo_actual (rut, id_cargo_actual, fecha_inicio) values
						(?, ?, ?)";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $id_cargo_actual, $fecha_inicio));
		//exit;
	}
	
	function borrar_cargo_actual($id_cargo_actual) {
		//obtenemos el rut del cargo ingresado, si existe, se agrega al historico y se borra (id_cod_cargo=null)
		$sql="select * from cargo_actual where id_cargo_actual=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($id_cargo_actual));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		
		
		
		//print_r($result_id);
		
			
		if(isset($result[0])){
			$sql_id="select id_cargo from cargo_hst where rut=? order by id_cargo desc limit 0,1";
			$query_id=$this->con->prepare($sql_id);
			$query_id->execute(array($result[0]['rut']));
			$result_id = $query_id->fetchAll(PDO::FETCH_ASSOC);
			$id_cargo_hst=1; //partimos el id en 1, si no tiene aun
			if(isset($result_id[0]['id_cargo']))
				$id_cargo_hst=$result_id[0]['id_cargo']+1;
			
			$sql="insert into cargo_hst (rut, id_cargo, id_cod_cargo, fecha_inicio, fecha_termino, descripcion) values
							(?, ?, ?, ?, now(),?)";
	
			$q=$this->con->prepare($sql);
			$q->execute(array($result[0]['rut'], $id_cargo_hst, $result[0]['id_cargo_actual'], $result[0]['fecha_inicio'], ''));
			
			//borramos el cargo actual.
			$sql="delete from cargo_actual where rut=? and id_cargo_actual=?";
			$q=$this->con->prepare($sql);
			$q->execute(array($result[0]['rut'], $result[0]['id_cargo_actual']));
			
		}
		
		
	}

	function actualizar_cargo($rut, $id_cargo_actual, $fecha_inicio) {
		$sql="update cargo_actual set fecha_inicio=?, rut=? where id_cargo_actual=?";
		$q=$this->con->prepare($sql);
		$q->execute(array($fecha_inicio, $rut, $id_cargo_actual));
	}
	
	function obtener_cargos_actuales($rut) {
		$sql="select ca.*, cc.descripcion from cargo_actual ca, cod_cargo cc where ca.rut=? and ca.id_cargo_actual=cc.id_cod_cargo order by ca.id_cargo_actual";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	
	function usuario_es_tesorero($usuario){
		//primero se obtiene el rut del usuario, luego se ve si ese rut tiene cargo de tesorero
		$sql="select rut from usuario where usuario=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($usuario));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]) and $result[0]['rut']!=''){
			$sql="select 1 from cargo_actual where rut=? and id_cargo_actual=".COD_CARGO_TESORERO;
			$query=$this->con->prepare($sql);
			$query->execute(array($result[0]['rut']));
			$result2=$query->fetchAll(PDO::FETCH_ASSOC);
			//echo $sql;
			if(isset($result2[0]))
				return true;
		}
		return false;
	}
}