<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_cod_region = new cod_region($con);
$t_bombero = new bombero($con);

//para mostrar bomberos que asistieron al llamado, se mostrarán solo bomberos activos
$bomberos = $t_bombero->obtener_checkbox_bomberos(array(1));
/*
$form['fecha_ini'] = date('Y-m-d');
$form['hora_ini'] = '';
$form['fecha_fin'] = date('Y-m-d');
$form['hora_fin'] = '';
$form['dir_region'] = '13';
$form['dir_comuna'] = '';
$form['dir_calle'] = '';
$form['dir_numero'] = '';
$form['observaciones'] = '';
$form['rut'] = '';
$form['rut_dv'] = '';
*/
//foreach($_POST as $indice => $dato){ //recibiendo datos post
	//$form[$indice] = $dato;
//}


// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
//$regiones = $t_cod_region->obtener_regiones();

include('../../vista/llamados/v_agregar_asistentes.php');