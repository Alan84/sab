<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Bomberos borrados') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T20:56:45-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<h1 style="color: white; background-color: black">Bomberos borrados</h1>
<form method="post" action="actualizar_bombero_borrado.php">
<table class="tabla" style="margin-top: 1em">
<thead>
<tr>
	<!-- <th>selecionar</th> -->
	<th><a href="bomberos_borrados.php?orden=rut<?php echo $orden_link_rut.$link_busqueda ?>" style="color: white">rut<?php echo $simbolo_orden_rut ?></a></th>
	<th><a href="bomberos_borrados.php?orden=nombre<?php echo $orden_link_nom.$link_busqueda ?>" style="color: white">nombre<?php echo $simbolo_orden_nom ?></a></th>
	<th><a href="bomberos_borrados.php?orden=edad<?php echo $orden_link_edad.$link_busqueda ?>" style="color: white">edad<?php echo $simbolo_orden_edad ?></a></th>
	<th><a href="bomberos_borrados.php?orden=estado<?php echo $orden_link_est.$link_busqueda ?>" style="color: white">estado<?php echo $simbolo_orden_est ?></a></th>
	<th><a href="bomberos_borrados.php?orden=cuota<?php echo $orden_link_cuota.$link_busqueda ?>" style="color: white">última cuota<?php echo $simbolo_orden_cuota ?></a></th>
	<th><a href="bomberos_borrados.php?orden=actualiza<?php echo $orden_link_act.$link_busqueda ?>" style="color : white">actualización<?php echo $simbolo_orden_act ?></a></th>
	<th>Sel.</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($result as $dat){ ?>
	<tr>
		<!--  <td style="text-align: center"><input type="checkbox" name="rut_<?php echo $i++;?>" value="<?php echo $dat['rut'] ?>" /></td> -->
		<td style="text-align: right"><?php echo number_format($dat['rut'], 0, ',', '.').'-'.$dat['rut_dv'] ?></td>
		<td><?php echo $dat['nombre'].' '.$dat['apellido'] ?></td>
		<td style="text-align: right"><?php echo $dat['edad'] ?></td>
		<td style="text-align: center"><?php echo $dat['est_bombero'] ?></td>
		<td style="text-align: center"><?php echo obtener_mes($dat['fecha_vence_m']).' '.$dat['fecha_vence_a']; ?></td>
		<td style="text-align: center"><?php echo $dat['fecha_actualiza'] ?> (<?php echo $dat['usuario_actualiza'] ?>)</td>
		<td style="text-align: center"><input type="checkbox" name="ruts[]" value="<?php echo $dat['rut']; ?>" /></td>
	</tr>
<?php } ?>
<tbody>
</table>
<p style="text-align: right"><input type="submit" name="eliminar" value="Eliminar"  /> <input type="submit" name="restablecer" value="Restablecer" /></p>
</form>
</div>
</div>
</body>
</html>