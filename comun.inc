<?php

$dbname='quinta5';
$cadena_con="mysql:host=localhost;dbname=$dbname";
$usuario_bd='tester';
$clave_bd='tester';

define('TITULO_HTML', 'SAB - %s'); //titulo de la ventana del navegador, el dato dinámico se cambia por la pantalla actual.

define('CANTIDAD_ANIOS_SERVICIO_INICIO', 5); //cuantos años de servicio, se mostrarán al inicio, se usa en agregar bombero
define('CANTIDAD_ANOTACIONES_INICIO', 5); //cuantas anotaciones se mostrarán al inicio, se usa en agregar bombero
define('CANTIDAD_CURSOS_INICIO', 5); //cuantos cursos se mostrarán al inicio, se usa en agregar bombero
define('CANTIDAD_CARGOS_INICIO', 5); //cuantos cargos se mostrarán al inicio, se usa en agregar bombero

define('DIRECTORIO_WEB', '/home/alvaro/www'); 
define('DIRECTORIO_WEB_SISTEMA', '/sab'); 

define('DIRECTORIO_CERTIFICADO', '/subidos/certificados');
define('NOM_ARCHIVO_CERTIFICADO', 'certificado_%d_%d'); //nombre para guardar el certificado, el %d son datos dinámicos, se reemplazan por el rut del bombero y el numero id del registro

define('DIRECTORIO_FOTO', '/subidos/fotos'); //cuantos cargos se mostrarán al inicio, se usa en agregar bombero
define('NOM_ARCHIVO_FOTO_BOMBERO', 'foto_%d'); //nombre para guardar la foto, el %d es un dato dinámico, se reemplaza por el rut del bombero

define('REGISTROS_POR_PAGINA', 10); //Si hay muchos registros, estos se listaran en filas y se harán páginas para el resto
define('MIN_IMPUT_ANIOS', 1900); //mínimo para entradas de años, solo para mejorar uso de la entrada.

define('TIPO_USUARIO_ADMINISTRADOR', 1);
define('TIPO_USUARIO_NORMAL', 2);
define('COD_CARGO_TESORERO', 6);


define('CORREO_ENVIO_MAILS', 'infosistemasab@gmail.com'); //correo cuenta de gmail para enviar correo 
define('PASSWORD_ENVIO_MAILS', 'bombero5'); //password de la cuenta de gmail para enviar correo
define('SUBJECT_EMAILS_CUOTAS_ATRAZADAS', 'Cuotas atrazadas'); //asunto para el envío masivo de correos, cuotas atrasadas
define('SUBJECT_EMAILS_CUOTAS_PAGADA', 'Pago registrado');  //asunto para el envío del comprobante de pago
define('FROM_EMAILS_SISTEMA', 'Sistema SAB'); //aparecerá en from del receptor de los correos


define('COMENTARIO_EXTRA_MOROSIDAD', 'Según los registros, usted debe %d o más cuotas, por lo que estaría en falta.'); //Comentario extra, si excede la morosidad permitida, se puede enviar un comentario extra (morosidad limite se setean en los parámetros del sistema)
define('COMANDO_MYSQLDUMP', "mysqldump -u $usuario_bd --password=$clave_bd $dbname > %s"); //Comando para hacer respaldos, %s se reemplaza por el nombre del archivo sql
define('DIRECTORIO_TMP', '/subidos/tmp');
define('NOM_ARCHIVO_SQL_RESPALDO', 'respaldo_%s_%s.sql');
define('COMANDO_MYSQLDUMP_RESTABLECER', "mysql -u $usuario_bd --password=$clave_bd $dbname < %s"); //Comando para restaurar la base de datos, %s se reemplaza por el nombre del archivo sql
