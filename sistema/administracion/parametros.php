<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_parametro = new parametro($con);
$parametros = $t_parametro->obtener_lista_parametros();



include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_parametros.php';
