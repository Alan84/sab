<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Pagos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-10T20:42:41-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}

	function mostrar_anio(anio, x){
		meses=document.getElementById("meses");
		li= new Array();
		check= new Array();
		label = new Array();
		<?php $x=0; foreach($meses as $id=>$mes){ ?>
			li[<?php echo $x; ?>]=document.createElement("li");
			check[<?php echo $x; ?>]=document.createElement("input");
			check[<?php echo $x; ?>].setAttribute("type", "checkbox");
			check[<?php echo $x; ?>].setAttribute("id", "fecha_"+x);
			check[<?php echo $x; ?>].setAttribute("name", "fechas["+x+"]");
			check[<?php echo $x; ?>].setAttribute("value", anio+"-<?php echo $id; ?>");

			label[<?php echo $x; ?>]=document.createElement("label");
			label[<?php echo $x; ?>].setAttribute("for", "fecha_"+x);

			label[<?php echo $x; ?>].appendChild(document.createTextNode("<?php echo $mes; ?> "+anio));
			li[<?php echo $x; ?>].appendChild(check[<?php echo $x; ?>]);
			li[<?php echo $x; ?>].appendChild(label[<?php echo $x; ?>]);
			meses.appendChild(li[<?php echo $x; ?>]);
			x++;
		<?php $x++; } ?>
		enlace=document.getElementById("muestra_siguiente");
		enlace.setAttribute("href", "javascript:mostrar_anio("+(anio+1)+", "+x+")");
	}
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">

<h1 style="color: white; background-color: black; margin-bottom: 1em">Total de pagos</h1>
<?php if($pagina>0) for($x=1; $x<=ceil($total_registros/REGISTROS_POR_PAGINA); $x++){ ?>
	<a href="pagos.php?pagina=<?php echo $x; ?>&orden=<?php echo $orden;?>&tipo_orden=<?php echo $tipo_orden; ?>#historico" class="<?php echo ($x==$pagina)? 'pagina_actual': 'pagina'; ?>"><?php echo $x; ?></a>
<?php }?>
<form method="post" action="modificar_pagos.php">
<table class="tabla" style="margin: 1em">
<thead>
<tr>
	<th>ID</th>
	<th><a href="pagos.php?orden=rut<?php echo $orden_link_rut; ?>" style="color: white">Rut<?php echo $simbolo_orden_rut; ?></a></th>
	<th><a href="pagos.php?orden=fecha_ingreso<?php echo $orden_link_fecha_ingreso; ?>" style="color: white">Fecha de ingreso<?php echo $simbolo_orden_fecha_ingreso; ?></a></th>
	<th><a href="pagos.php?orden=pago_mes<?php echo $orden_link_pago_mes; ?>" style="color: white">Pagos<?php echo $simbolo_orden_pago_mes; ?></a></th>
	<th><a href="pagos.php?orden=monto<?php echo $orden_link_monto; ?>" style="color: white">Monto<?php echo $simbolo_orden_monto; ?></a></th>
	<th><a href="pagos.php?orden=comentario<?php echo $orden_link_comentario; ?>" style="color: white">Comentario<?php echo $simbolo_orden_comentario; ?></a></th>
	<!-- <th><a href="ver_pagos.php?orden=fecha_mail_alerta<?php echo $orden_link_fecha_mail_alerta; ?>" style="color: white">fecha mail alerta<?php echo $simbolo_orden_fecha_mail_alerta; ?></a></th> -->
	<th><a href="pagos.php?orden=usuario<?php echo $orden_link_usuario; ?>" style="color: white">Usuario<?php echo $simbolo_orden_usuario; ?></a></th>
	<th>Sel.</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($pagos as $pago){ ?>
	<tr>
		<td style="text-align: center; vertical-align: top;">
			<?php echo $pago['id_pago'] ?>
		</td>
		<td style="text-align: center; vertical-align: top;">
			<a href="../bomberos/ver_bombero.php?rut=<?php echo $pago['rut']; ?>" ><?php echo number_format($pago['rut'], 0, ',', '.').'-'.$pago['rut_dv']; ?></a>
		</td>
		<td style="vertical-align: top;"><?php echo $pago['fecha_ingreso'] ?></td>
		<td style="text-align: left">
			<ol style="margin: 0.5em">
			<?php foreach ($m_pagos->obtener_meses($pago['id_pago']) as $cuota){ ?>
				<li><span style="font-weight: bold;"><?php echo obtener_mes($cuota['pago_mes_m']).' '.$cuota['pago_mes_a'];?></span></li>
			<?php } ?>
			</ol>
		</td>
		<td style="text-align: right; vertical-align: top;">$<?php echo number_format($pago['monto'], 0, ',', '.') ?></td>
		<td style="text-align: left; vertical-align: top;"><?php echo $pago['comentario'] ?></td>
		<!-- <td style="text-align: center; vertical-align: top;"><?php ; // echo $pago['fecha_mail_alerta'] ?></td> -->
		<td style="text-align: center; vertical-align: top;"><?php echo $pago['usuario'] ?></td>
		<td style="text-align: center; vertical-align: top;"><input type="checkbox" name="ids[<?php echo $pago['id_pago'] ?>]" value="<?php echo $pago['rut'] ?>" /></td>
	</tr>
<?php } ?>
<tbody>
</table>
<input type="hidden" name="rut" value="<?php echo $rut; ?>" />
<p style="text-align: right"><input type="submit" name="borrar" value="Borrar" /></p>
</form>
</div>
</div>
</body>
</html>
