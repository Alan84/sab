<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_cargo.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_est_bombero = new est_bombero($con);
$t_cod_cargo = new cod_cargo($con);
$m_cargos = new m_cargos($con);
$t_bombero = new bombero($con);

$errores=check_errores();
$borrar=array();
$cambios=array();
if(isset($_POST['rut'])){
	foreach ($_POST['rut'] as $cod_cargo=>$rut_cambio){
		if($rut_cambio!=='') {
			if($rut_cambio=='0') {
				$m_cargos->borrar_cargo_actual($cod_cargo);
			}
			else{
				list($anio, $mes, $dia) = explode('-', $_POST['fini_cargo'][$cod_cargo]);
				$m_cargos->guardar_cargo_nuevo($rut_cambio, $cod_cargo, $anio.'-'.$mes.'-'.$dia);
			}
		}
	}
}

//if($cambios==array())
//	$errores[]='No han seleccionado cambios.';
	
	

$estados=$t_est_bombero->obtener_estados_bomberos();
//$cargos=$t_cargo->obtener_cargos();
$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','no cambiar')));


if ($errores==array()){
	header('location: cargos.php');
	
	//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_confirmar_cargos.php');
}
else{
	$enlace_volver = DIRECTORIO_WEB_SISTEMA.'/sistema/bomberos/cargos.php';
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}

function check_errores(){
	$errores=array();
	if(false)
		$errores[]='Solo pueden cambiar los cargos un usuario normal o administrador';
		
	return $errores;
}