<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Borrar pagos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:50-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar eliminar datos</h1>

<form method="post" action="modificar_pagos.php">
	<p>Va a borrar pagos, por favor, confirme la acción. 
	Al confirmar se realizará un <em>borrado lógico</em>, esto se puede revertir en el modulo administración.</p>
	<p>Recuerde siempre hacer respaldos, sobretodo antes de eliminar alguna información.</p>
	<p>pagos a borrar:</p>
			<ul>
			<?php foreach($pagos as $pago){ ?>
				<li>Bombero: <?php echo $pago['bombero_datos_basicos']; ?>; ID Pago: <?php echo $pago['id']; ?>
					<ol>
						<?php foreach($pago['meses'] as $mes){ ?>
							<li><?php echo obtener_mes($mes['pago_mes_m']).' '.$mes['pago_mes_a']; ?></li>
						<?php } ?>
					</ol>				
				</li>
				
			<?php } ?>
			</ul>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>
	
	<input type="hidden" name="rut" value="<?php echo $rut; ?>" />
	<p style="text-align: right"><input type="submit" name="confirmar" value="Borrado lógico" />  <input type="submit" name="cancelar" value="Cancelar" /></p>
</form>

</div>
</div>
</body>
</html>