<?php

class cod_cargo{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}

	function obtener_cargos() {
		$sql="select * from cod_cargo";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function obtener_descripcion($cod) {
		$sql="select descripcion from cod_cargo where id_cod_cargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($cod));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['descripcion'];
		else
			return false;
	}
}
