<nav>
<ul style="">
	<li style="float: left; margin-left: 1em">Administración
		<ul>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/usuarios.php">Usuarios</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/respaldo.php">Respaldo</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/parametros.php">Parametros</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/reportes.php">Reportes</a></li>
		</ul>
	</li>
	<li style="float: left; margin-left: 1em">Bomberos
		<ul>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/bomberos/consulta_bomberos.php">Lista Bomberos</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/bomberos/agregar_bombero.php">Agregar</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/bomberos/cargos.php">Ver Cargos</a></li>
		</ul>
	</li>
	<li style="float: left; margin-left: 1em">Llamados
		<ul>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/llamados/consulta_llamados.php">Lista llamados</a></li>
			<li><a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/llamados/agregar_llamado.php">Agregar llamado</a></li>
		</ul>
	</li>
	
</ul>
</nav>
