<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$errores=check_errores($_POST);

if($errores==array()){
	try{
	//guardamos
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$t_bombero = new bombero($con);

	$fecha_nac=$_POST['fecha_nac'];
	$fecha_ingreso=$_POST['fecha_ingreso'];
	
	$t_bombero->guardar_bombero($_POST['rut'], $_POST['rut_dv'], $_POST['nombre'], 
		$_POST['apellido'], $_POST['profesion'], $_POST['sexo'], $_POST['id_est_civil'],
		$_POST['grup_san'], $_POST['direc_calle'], $_POST['direc_numero'],
		$_POST['direc_comuna'], $_POST['direc_region'],
		$_POST['direc_depto'], $_POST['email'], $_POST['email2'],
		$_POST['fono'], $_POST['fono2'],
		$fecha_nac, $fecha_ingreso, $_POST['id_est_bombero'], $_POST['num_registro'], $_POST['num_tib'],
		$_POST['num_placa'], $_POST['nombre_padre'], $_POST['nombre_madre'], $_POST['comentario'], $_SESSION['usr']);
	
	if(isset($_POST['guardar_agrega_mas']))
		header('location: agregar_bombero.php');
	else if(isset($_POST['guardar_editar_vida']))
		header('location: editar_vida_bombero.php?rut='.$_POST['rut']);
	else
		header('location: consulta_bomberos.php?orden=actualiza&tipo_orden=desc');
	
	}catch(Exception $e){
		print_r($_POST);
		echo "err:".$e->getMessage();
	}
}
else{
	$enlace_volver='agregar_bombero.php';
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}


function check_errores($dats){

	$errors=array();
	
	if(trim($dats['nombre'])=='')
		$errors[]='Campo nombre está vacío';
	if(trim($dats['apellido'])=='')
		$errors[]='Campo apellido está vacío';
	if(trim($dats['rut'])==''){
		$errors[]='Campo Rut está vacío';
	}
	else{
		$dv=obtener_digito_verificador($dats['rut']);
		$dats['rut_dv']=strtoupper($dats['rut_dv']);
		if($dv!=$dats['rut_dv'])
			$errors[]='Digito verificador del Rut está incorrecto';
	}
	//if(trim($dats['profesion'])=='')
		//$errors[]='Campo profesión vacío';

	//if(trim($dats['id_est_civil'])=='')
	//	$errors[]='Estado civil no seleccionado';
	
//	else{
//	if(verificar_estadocivil()==false){
//			$errors[]='El estado civil no existe en la base de datos';
//		}
//	}
	if(trim($dats['id_est_bombero'])==''){
		$errors[]='Estado bombero no seleccionado';
	}

//	if(trim($dats['direc_'])=='')
//		$errors[]='Campo direccion esta vacio';
	if(!isset($dats['sexo']) or trim($dats['sexo'])=='')
		$errors[]='Campo <em>sexo</em> está vacío';
		
	if(trim($dats['fecha_nac'])=='')
		$errors[]='Campo <em>fecha de nacimiento</em> está vacío';
		
	if(trim($dats['fecha_ingreso'])=='')
		$errors[]='Campo <em>fecha de ingreso</em> está vacío';
		
	if(trim($dats['fecha_ingreso'])!='' and trim($dats['fecha_nac'])!='')
		if($dats['fecha_ingreso'] < $dats['fecha_nac'])
			$errors[]='Campo <em>fecha de nacimiento</em> es mayor a <em>fecha de ingreso</em>';
	
	return $errors;
}

function obtener_digito_verificador($rut){
	$d=false;
	$rut = strrev($rut);
	$aux = 1;
	$s = 0;
	for($i=0;$i<strlen($rut);$i++){
		$aux++;
		$s += intval($rut[$i])*$aux;
		if($aux == 7){ $aux=1; }
	}
	$digit = 11-$s%11;
	if($digit == 11){$d = 0;}
	elseif($digit == 10){$d = "K";}
	else{$d = $digit;}
	return $d;
}