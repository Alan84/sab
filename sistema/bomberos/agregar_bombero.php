<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_civil.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_anotacion.php');
//include('../modelo/t_tipo_cargo.php');
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_anio.php'); ya no se usa
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_curso.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$rut=$form['rut']='';
if(isset($_GET['rut']) and is_numeric($_GET['rut']))
        $rut=$form['rut']=$_GET['rut'];

$t_est_civil = new est_civil($con);
///$t_cod_anio = new cod_anio($con); ya no se usa
$t_est_bombero = new est_bombero($con);
$t_tipo_curso = new tipo_curso($con);
$t_tipo_anotacion = new tipo_anotacion($con);
$t_cod_region= new cod_region($con);
$t_parametro= new parametro($con);
$t_bombero= new bombero($con);

// obtenemos los estados civiles desde la base de datos para usarlos en el combo
$estados_civil = $t_est_civil->obtener_estados_civiles();

// obtenemos los estados civiles desde la base de datos para usarlos en el combo
///$anios_servicio = $t_cod_anio->obtener_anios_servicios();

// obtenemos los estados bomberos desde la base de datos para usarlos en el combo
$estados_bombero = $t_est_bombero->obtener_estados_bomberos();

// obtenemos los tipos de cursos desde la base de datos para usarlos en el combo
$cursos = $t_tipo_curso->obtener_tipos_cursos();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$tipo_anotaciones = $t_tipo_anotacion->obtener_tipos_anotaciones();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$regiones = $t_cod_region->obtener_regiones();

// obtenemos los tipos de cargo desde la base de datos para usarlos en el combo
//$tipo_cargos = $t_tipo_cargo->obtener_tipos_cargos();

$cantidad_anios_servicio=CANTIDAD_ANIOS_SERVICIO_INICIO;
$cantidad_anotaciones=CANTIDAD_ANOTACIONES_INICIO;
$cantidad_cursos=CANTIDAD_CURSOS_INICIO;
$cantidad_cargos=CANTIDAD_CARGOS_INICIO;

/*
foreach($anios_servicio as $id=>$anio_servicio)
	$anio[$id]='';
*/
/*
if(isset($_POST['anio']) and count($_POST['anio'])>0){
	for($i=1;$i<count($_POST['anio']);$i++){
		if($_POST['anio'][$i]!='' and $cantidad_anios_servicio<$i){
				$cantidad_anios_servicio=$i;
		}
	}
}

if(isset($_POST['anotacion_anio']) || isset($_POST['id_anotacion']) || isset($_POST['anotacion_descripcion'])){
	for($i=0;$i<count($_POST['anotacion_anio']);$i++){
		if($i>=CANTIDAD_ANOTACIONES_INICIO)
			$cantidad_anotaciones=$i+1;
	}
}

if(isset($_POST['curso_anio']) || isset($_POST['id_curso']) || isset($_POST['curso_descripcion'])){
	for($i=0;$i<count($_POST['curso_anio']);$i++){
		if($i>=CANTIDAD_CURSOS_INICIO)
			$cantidad_cursos=$i+1;
		
	}
}

*/
//$cantidad_anotaciones=20;

// seteamos los meses para usarlos en el combo
$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$result=$t_bombero->obtener_ultimo_agregado();

$ultimo['rut']=$ultimo['nombre']=$ultimo['apellido']='';
if($result!=false)
	$ultimo=$result;

$form['rut']=$form['rut_dv']=$form['direc_calle']=$form['direc_numero']=$form['direc_depto']='';
$form['nombre']=$form['apellido']=$form['nombre_padre']=$form['nombre_madre']=$form['fono']=$form['fono2']='';
$form['fecha_ingreso']=$form['fecha_nac']='';
$form['profesion']=$form['grup_san']=$form['email']=$form['email2']=$form['sexo']=$form['sel_sexo_m']=$form['sel_sexo_f']='';
$form['id_est_civil']=$form['id_est_bombero']=$form['num_registro']=$form['num_tib']=$form['num_placa']=$form['comentario']='';
$form['direc_comuna']=$t_parametro->obtener_parametro('comuna_por_defecto');
$form['direc_region']=$t_parametro->obtener_parametro('region_por_defecto');
/* $curso_tipo=array();
$anotacion_descripcion=array();
for($i=0; $i<$cantidad_anotaciones; $i++){
	$id_anotacion[$i]='';
	$anotacion_anio[$i]='';
	$anotacion_descripcion[$i]='';
}
for($i=0; $i<$cantidad_cursos; $i++){
	$id_curso[$i]='';
	$curso_anio[$i]='';
	$curso_descripcion[$i]='';
} */
/*
for($i=0; $i<$cantidad_cargos; $i++){
	$id_cargo[$i]='';
	$cargo_fecha_inicio_d[$i]=$cargo_fecha_inicio_m[$i]=$cargo_fecha_inicio_a[$i]='';
	$cargo_fecha_termino_d[$i]=$cargo_fecha_termino_m[$i]=$cargo_fecha_termino_a[$i]='';
	$cargo_descripcion[$i]='';
}

**/

foreach($_POST as $indice => $dato){
	if(is_array($_POST[$indice])){
	/*	foreach($_POST[$indice] as $indice2=>$dato2){
			switch($indice) {
				case 'anio': //error de php, no se puede poner el nombre en forma dinámica
					$anio[$indice2]= $dato2;
					break;
				case 'anotacion_descripcion': //error de php, no se puede poner el nombre en forma dinámica
					$anotacion_descripcion[$indice2]= $dato2;
					break;
				case 'anotacion_anio': //error de php, no se puede poner el nombre en forma dinámica
					$anotacion_anio[$indice2]=$dato2;
					break;
				case 'id_anotacion': //error de php, no se puede poner el nombre en forma dinámica
					$id_anotacion[$indice2]=$dato2;
					break;
				case 'curso_descripcion': //error de php, no se puede poner el nombre en forma dinámica
					$curso_descripcion[$indice2]= $dato2;
					break;
				case 'curso_anio': //error de php, no se puede poner el nombre en forma dinámica
					$curso_anio[$indice2]=$dato2;
					break;
				case 'id_curso': //error de php, no se puede poner el nombre en forma dinámica
					$id_curso[$indice2]=$dato2;
					break;
			}
		}
		*/
	}
	else
		$form[$indice]= $dato;
}

// print_r($id_curso);
$sel_sexo_m=$sel_sexo_f='';
if($form['sexo']=='m') 
	$sel_sexo_m='checked="checked"';
else if($form['sexo']=='f') 
	$sel_sexo_f='checked="checked"';

//print_r($tipo_anotaciones);

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_agregar_bombero.php');