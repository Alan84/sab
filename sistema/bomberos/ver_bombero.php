<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$m_bomberos = new m_bomberos($con);
$t_cod_region= new cod_region($con);

$rut='';
if(isset($_GET['rut']) and is_numeric($_GET['rut']))
	$rut=$_GET['rut'];

// obtenemos los premios por años
$premio_anios=$m_bomberos->obtener_premios($rut);
// obtenemos las anotaciones
$anotaciones=$m_bomberos->obtener_anotaciones($rut);
// obtenemos los cursos
$cursos=$m_bomberos->obtener_cursos($rut);
// obtenemos los cursos
$cargos=$m_bomberos->obtener_cargos_hst($rut);
$cargos_act=$m_bomberos->obtener_cargos_actuales($rut);


// seteamos los meses para usarlos en el combo
$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$result=$m_bomberos->obtener_datos_bombero($rut); //datos personales del bombero
// seteamos valores a cero
$rut=$rut_dv=$direccion=$nombre=$apellido=$fono=$fono2='';
$fecha_ingreso=$fecha_nac=$fecha_nac_d=$fecha_nac_m=$fecha_nac_a=$profesion=$grup_san=$email_quinta=$email2='';
$id_est_civil=$id_est_bombero='';
// print_r($result);
//if($result!=array())
	//foreach($result[0] as $indice => $dato)
		//$$indice= $dato;
		
$info = $result[0]; //pasamos la info a otra variable para mostrarla

if($info['foto']=='1')
	$foto = 'foto_'.$info['rut'].'_s.jpg';


//foreach($_POST as $indice => $dato){
//	$$indice= $dato;
//}

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_ver_bombero.php');