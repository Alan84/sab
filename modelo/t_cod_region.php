<?php

class cod_region{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_regiones($datos_iniciales=array()) {
		$datos=array();
		$sql="select * from cod_region";
		
		$datos_combo=array();
		if($datos_iniciales!=array())
			$datos_combo=$datos_iniciales;
		
		foreach($this->con->query($sql) as $row)
			$datos_combo[$row['id_cod_region']]=$row['descripcion'];
		return $datos_combo;
	}

	function obtener_descripcion($cod) {
		$sql="select descripcion from cod_region where id_cod_region=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($cod));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['descripcion'];
		else
			return false;
	}
}