<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Respaldo base de datos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T21:26:30-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar eliminar datos</h1>

<form method="post" action="respaldo.php">
	<p>Respaldo de la base de datos subido.</p>
	
	<p style="text-align: right"><input type="submit" name="aceptar" value="Aceptar" /></p>
</form>

</div>
</div>
</body>
</html>
