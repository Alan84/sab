<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_pago_mes.php');




//$_POST['uname'] = trim($_POST['uname']); //eliminamos los espacios en blanco	

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_usuario = new usuario($con);
$t_pago = new pago($con);
$t_pago_mes = new pago_mes($con);

$errores=verificar_errores($t_usuario);
//if($t_bombero->existe_bombero($_POST['rut'])==false)
	//$errores[]='Rut ingresado no se encuentra registrado';


//$t_usuario = new usuario($con);
//$t_tipo_usuario = new tipo_usuario($con);
//$form['uname'] = $_POST['uname'];
//$form['descripcion_tipo_usuario'] = $t_tipo_usuario->obtener_descripcion($_POST['tipo_usuario']);
if(isset($_POST['confirmar'])){ //segunda pasada y en esta presionó el botón "confirmar"
	if(isset($_POST['eliminar'])){
		foreach($_POST['ids'] as $id => $rut){
			//primero hay que borrar los asistentes al llamado
			$t_pago_mes->eliminar_cuotas($id); //primero se elimina la tabla hijo
			$t_pago->eliminar_pago($rut, $id); // se elimina definitivamente el pago
		}
	}
	else if(isset($_POST['restablecer'])) {
		foreach($_POST['ids'] as $id => $rut)
			$t_pago->set_borrado($rut, $id, ''); //el vacío lo dejará como null, osea, como no borrado
	}	
	header('location: reportes.php');
}
else {
	foreach($_POST['ids'] as $id => $rut){
		$form['datos_basicos'][] = $t_pago->obtener_datos_basicos($rut, $id, '1'); // 1 para forzar que muestre el borrado
	}
	if(isset($_POST['eliminar'])){
		if($errores==array()){
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_eliminar_pagos.php';
		}
		else{
			$enlace_volver='pagos_borrados.php';
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
		}
	}
	else if(isset($_POST['restablecer'])) {
		if($errores==array()){
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_restablecer_pagos.php';
		}
		else{	
			$enlace_volver='pagos_borradas.php';
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
		}
	}
	else {
		header('location: reportes.php');
	}
	//print_r($_POST);
}//fin confirmar

function verificar_errores($t_usuario) {
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='Solo puede eliminar un usuario administrador';
		
	return $errores;
}