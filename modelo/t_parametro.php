<?php

class parametro{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_parametro($indice) {
		$sql="select valor from parametro where nombre=?";
		$q=$this->con->prepare($sql);
		$q->execute(array($indice));
		$result = $q->fetchAll(PDO::FETCH_ASSOC);;
		return $result[0]['valor'];
	}
	
	function obtener_lista_parametros() {
		$sql="select * from parametro";
		$q=$this->con->prepare($sql);
		$q->execute();
		$result = $q->fetchAll(PDO::FETCH_ASSOC);;
		return $result;
	}
	
	function actualizar_parametro($name, $dato) {
		$sql='update parametro set valor=? where nombre=?';
		$q=$this->con->prepare($sql);
		$q->execute(array($dato, $name));
		//$result = $q->fetchAll(PDO::FETCH_ASSOC);;
		//echo $dato;
		//exit;
		return 0; //$result;
	}
}
