<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Llamado') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T22:38:10-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar llamado</h1>
	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/llamados/guardar_llamado.php" method="post">
			<table>
				<tr><td>tipo llamado:</td><td>
					<?php echo $_POST['cod_tipo_llamado']; ?>	
				</td>
				</tr>
			<tr><td>fecha/hora inicio:</td><td><?php echo $_POST['fecha_ini'].' '.$_POST['hora_ini']; ?> * salida del cuartel</td></tr>
			<tr><td>fecha/hora termino:</td><td><?php echo $_POST['fecha_fin'].' '.$_POST['hora_fin']; ?>	* llegada al cuartel</td></tr>
			
			<tr><td>dirección:</td><td>Región: <?php echo $t_cod_region->obtener_descripcion($_POST['dir_region']);?><br /> 
				Comuna: <?php echo $_POST['dir_comuna']; ?> <br /> 
				Calle: <?php echo $_POST['dir_calle']; ?><br /> 
				Número: <?php echo $_POST['dir_numero']; ?><br />
				Calle2: <?php echo $_POST['dir_calle2']; ?> *calle cercana, intersección o referencial
			</td></tr>
			<tr><td>Observaciones:</td><td><?php echo $_POST['observaciones']; ?></td></tr>
			<tr><td>Rut bombero a cargo: <br />
			
			</td><td><?php echo $t_bombero->obtener_nombre_completo($_POST['rut_acargo']).' ['.$_POST['rut_acargo'].'-'.$t_bombero->obtener_rut_dv($_POST['rut_acargo']).']'; ?></td></tr>
			
			<tr><td style="vertical-align: top">Bomberos asitentes:</td><td>
			<em>*Nota: No se mostrará en la lista el rut del bombero a cargo.</em>
				<ul>
				<?php if(isset($_POST['rut_asistentes'])) foreach($_POST['rut_asistentes'] as $rut_asiste){ ?>
					<?php if($rut_asiste!=$_POST['rut_acargo']){ ?>
						<li><span class="bombero_asiste"><?php echo $t_bombero->obtener_nombre_completo($rut_asiste).' ['.$rut_asiste.'-'.$t_bombero->obtener_rut_dv($rut_asiste).']'; ?></span></li>
					<?php } ?>
				<?php } ?>
				</ul>			
			</td></td></tr>
			</table>

	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	<input type="hidden" name="monto" value="<?php echo $monto; ?>" />
	<p style="text-align: right"><input type="submit" name="confirmar" value="Confirmar" /></p>
</form>
<form method="post" action="cargos.php">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<p style="text-align: right"><input type="submit" name="cancelar" value="Cancelar" /></p>
</form>
</div>
</div>
</body>
</html>