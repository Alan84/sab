<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
	exit; //para no continuar con la ejecución
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_curso.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_anotacion.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cargo_hst.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_premio_anios.php');


$errores=array();
$errores=check_errores($_POST);
//print_r($_FILES);
if($errores==array()){
	try{
	//guardamos
	$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	/*
	$fecha_nac=$_POST['fecha_nac_a'].'-'.$_POST['fecha_nac_m'].'-'.$_POST['fecha_nac_d'];
	$fecha_ingreso=$_POST['fecha_ingreso_a'].'-'.$_POST['fecha_ingreso_m'].'-'.$_POST['fecha_ingreso_d'];
 	$sql="insert into bombero (rut, rut_dv, nombre, apellido, sexo, profesion, id_est_civil,
			grup_san, direc_calle, direc_numero, direc_comuna, direc_region, direc_depto,
			email_quinta, email2, tel_movil, tel_fijo, fecha_nac, fecha_ingreso,
			fecha_actualiza, id_est_bombero, num_registro, num_tib, num_placa, nombre_padre, nombre_madre) values (:rut,:rut_dv, :nombre, :apellido,
			:sexo, :profesion, :id_est_civil, :grup_san,:direc_calle, :direc_numero, :direc_comuna, :direc_region, :direc_depto,
			:email_quinta, :email2, :tel_movil, :tel_fijo,
			:fecha_nac, :fecha_ingreso, now(), :id_est_bombero, :num_registro, :num_tib, :num_placa, :nombre_padre, :nombre_madre)";
	

//	include('v_guardar_bombero.php');
	$q=$con->prepare($sql);
	$q->execute(array(':rut'=>$_POST['rut'], ':rut_dv'=>$_POST['rut_dv'],':nombre'=>$_POST['nombre'],
		':apellido'=>$_POST['apellido'], ':profesion'=>$_POST['profesion'], ':sexo'=>$_POST['sexo'],
		':id_est_civil'=>$_POST['id_est_civil'],
		':grup_san'=>$_POST['grup_san'], ':direc_calle'=>$_POST['direc_calle'], ':direc_numero'=>$_POST['direc_numero'],
		':direc_comuna'=>$_POST['direc_comuna'], ':direc_region'=>$_POST['direc_region'],
		':direc_depto'=>$_POST['direc_depto'], ':email_quinta'=>$_POST['email_quinta'], ':email2'=>$_POST['email2'],
		':tel_movil'=>$_POST['tel_movil'], ':tel_fijo'=>$_POST['tel_fijo'],
		':fecha_nac'=>$fecha_nac, ':fecha_ingreso'=>$fecha_ingreso,
		':id_est_bombero'=>$_POST['id_est_bombero'], ':num_registro'=> $_POST['num_registro'], ':num_tib'=>$_POST['num_tib'],
		':num_placa'=>$_POST['num_placa'], ':nombre_padre'=>$_POST['nombre_padre'], ':nombre_madre'=>$_POST['nombre_madre']));
	*/

	$t_cargo_hst= new cargo_hst($con);
	
	$hay_borrado=0;
	foreach($_POST['bombero_cargos'] as $id=>$cargo){
		if(isset($cargo['id_cod_cargo']) && $cargo['id_cod_cargo']!='') {
			if($t_cargo_hst->existe_cargo($_POST['rut'], $id)){
				$t_cargo_hst->actualizar_cargo($_POST['rut'], $id, $cargo['id_cod_cargo'], $cargo['fecha_inicio'], 
				 		$cargo['fecha_termino'], $cargo['descripcion']);
			}
			else{
				$t_cargo_hst->guardar_cargo($_POST['rut'], $id, $cargo['id_cod_cargo'], $cargo['fecha_inicio'], 
				 		$cargo['fecha_termino'], $cargo['descripcion']);
			}
		}
		if(isset($cargo['borrar'])) {
			$t_cargo_hst->borrar_cargo($_POST['rut'], $id);
			$hay_borrado=1;
		}
	}
	$t_cargo_hst->ordenar_cargos($_POST['rut']);
	
	
	$t_premio_anios= new premio_anios($con);
	foreach($_POST['bombero_premios_anios'] as $id=>$premio_anio){
		if($premio_anio['anio_premio_cia']!='' && $premio_anio['anio_premio_cuerpo']!='') {
			if($t_premio_anios->existe_premio($_POST['rut'], $id)){
				$t_premio_anios->actualizar_premio($_POST['rut'], $id, $premio_anio['anios'], $premio_anio['anio_premio_cia'], $premio_anio['anio_premio_cuerpo']);
			}
			else {
				$t_premio_anios->guardar_premio($_POST['rut'], $id, $premio_anio['anios'], $premio_anio['anio_premio_cia'], $premio_anio['anio_premio_cuerpo']);
			}
		}
		if(isset($premio_anio['borrar'])) {
			$t_premio_anios->borrar_premio($_POST['rut'], $id);
		}
	} //no se ordenan los años borrados, por que este está asociado a una n cantidad de años (5, 10, 15, 20...), lo que desordenaría esta lógica 
	
	$hay_borrado=0;
	$t_curso= new curso($con);
	foreach($_POST['bombero_cursos'] as $id=>$curso){
		$ext_archivo=''; //para guardar la extensión del archivo, si queda vacío, se guardará como null
		//$id++; //los id parten en 1 asi q sumanos 1 al id entregado por el form
		$archivo='';
		if($curso['fecha']!='' and $curso['id_tipo_curso']!='') {
			
			//print_r($_FILES);
			if(isset($_FILES['certificado']['name'][$id]) and $_FILES['certificado']['name'][$id]!=''){ //hay archivo por subir			
				$archivo= sprintf(NOM_ARCHIVO_CERTIFICADO, $_POST['rut'], $id).'.'.strtolower(pathinfo($_FILES['certificado']['name'][$id], PATHINFO_EXTENSION));
				//movemos el archivo subido
				move_uploaded_file($_FILES['certificado']['tmp_name'][$id], DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$archivo);
				//echo DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$archivo;
			}
			else {
				$dir= opendir(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO);
				while($file = readdir($dir)){
					$archivo_sin_ext= sprintf(NOM_ARCHIVO_CERTIFICADO, $_POST['rut'], $id);
					if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file) and preg_match('/'.$archivo_sin_ext.'\.[a-z]{2,4}/', $file)){
						$archivo=$file;
					}
				}
			}
			
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$archivo)){
				$ext_archivo=strtoupper(pathinfo($archivo, PATHINFO_EXTENSION)); //guardamos solo la extension
				//$hay_archivo='1'; //guardamos un 1 en la base de datos, esto indica que existe archivo subido
			}
			
			if($t_curso->existe_curso($_POST['rut'], $id))
				$t_curso->actualizar_curso($_POST['rut'], $id, $curso['fecha'], $curso['descripcion'], $curso['id_tipo_curso'], $ext_archivo);
			else
				$t_curso->guardar_curso($_POST['rut'], $id, $curso['fecha'], $curso['descripcion'], $curso['id_tipo_curso'], $ext_archivo);
		}
		if(isset($curso['borrar'])) {
			$dir= opendir(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO);
			while($file = readdir($dir)){
				$archivo_sin_ext= sprintf(NOM_ARCHIVO_CERTIFICADO, $_POST['rut'], $id);
				if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file) and preg_match('/('.$archivo_sin_ext.')\.[a-z]{2,4}/', $file)){
					unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file); //borramos archivos para el rut y el id seleccionado.
				}
			}
			$t_curso->borrar_curso($_POST['rut'], $id);
		}
	}
	
	
	
	///primero se ordenan los certificados subidos
	$i=1;
	foreach($t_curso->obtener_ids_curso_asc($_POST['rut']) as $id){
		$file = sprintf(NOM_ARCHIVO_CERTIFICADO, $_POST['rut'], $id['id_curso']).'.'.strtolower($id['certificado']);
		$file_nombre_nuevo = sprintf(NOM_ARCHIVO_CERTIFICADO, $_POST['rut'], $i).'.'.strtolower($id['certificado']); //solo se cambia el indice
		if(!$t_curso->existe_curso($_POST['rut'], $i))
			if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file) and preg_match('/certificado_'.$_POST['rut'].'_'.$id['id_curso'].'\.[a-z]{2,4}/', $file))
				rename(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file, DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_CERTIFICADO.'/'.$file_nombre_nuevo);
		$i++;
	}
	//ahora se ordenan los cursos
	$t_curso->ordenar_cursos($_POST['rut']); //se ordena para que no hayan saltos
	
	
	$hay_borrado=0;
	$t_anotacion= new anotacion($con);
	foreach($_POST['bombero_anotaciones'] as $id=>$anotacion){
		if(trim($anotacion['fecha'])!='' and $anotacion['id_tipo_anotacion']){
			if($t_anotacion->existe_anotacion($_POST['rut'], $id)){
				$t_anotacion->actualizar_anotacion($_POST['rut'], $id, $anotacion['fecha'], $anotacion['descripcion'],
					$anotacion['id_tipo_anotacion']);
			}
			else {
				$t_anotacion->guardar_anotacion($_POST['rut'], $id, $anotacion['fecha'], $anotacion['descripcion'],
					$anotacion['id_tipo_anotacion']);
			}
		}
		
		if(isset($anotacion['borrar'])) {
			$t_anotacion->borrar_anotacion($_POST['rut'], $id);
			$hay_borrado=1;
		}
	}
	
	$t_anotacion->ordenar_anotaciones($_POST['rut']); //se ordenan para que no hayan salto

	header('location: ver_bombero.php?rut='.$_POST['rut']);
	}catch(Exception $e){
		echo "err:".$e->getMessage();
	}
}
else{
	$enlace_volver='editar_vida_bombero.php?rut='.$_POST['rut'];
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}


function check_errores($dats){
	$errores=array();

	$id_fila_vacia=0;
	foreach($dats['bombero_cargos'] as $id=>$cargo){
		if($cargo['id_cod_cargo']!='' || $cargo['fecha_inicio']!='' || $cargo['fecha_termino']!='' || $cargo['descripcion']!='') {
			if($cargo['id_cod_cargo']=='') $errores[]='Cargo: Campo cargo vacío';
			if($cargo['fecha_inicio']=='') $errores[]='Cargo: Campo fecha inicio vacío';
			if($cargo['fecha_termino']=='') $errores[]='Cargo: Campo fecha termino vacío';
			if(strtotime($cargo['fecha_inicio'])>strtotime($cargo['fecha_termino'])) $errores[]='Cargo: Campo fecha inicio mayor a fecha termino';
			if($id_fila_vacia==1) {
				$errores[]='Cargo: se saltó una fila';
				$id_fila_vacia=0;
			}
		}
		else if ($cargo['id_cod_cargo']=='' && $cargo['fecha_inicio']=='' && $cargo['fecha_termino']=='') {
			$id_fila_vacia=1;
		}
	}

	$id_fila_vacia=0;
	//$t_premio_anios->guardar_premio($_POST['rut'], $id, $premio_anio['anios'], $premio_anio['anio_premio_cia'], $premio_anio['anio_premio_cuerpo']);
	foreach($dats['bombero_premios_anios'] as $id=>$cargo){
		if($cargo['anio_premio_cia']!='' || $cargo['anio_premio_cuerpo']!='') {
			if($cargo['anios']=='') $errores[]='Premios Años: Campo Años vacío';
			if($cargo['anio_premio_cia']=='') $errores[]='Premios Años: Campo premio compañía vacío';
			if($cargo['anio_premio_cuerpo']=='') $errores[]='Premios Años: Campo premio cuerpo vacío';
			if($id_fila_vacia==1) {
				$errores[]='Premios Años: No se pueden saltar filas';
				$id_fila_vacia=0;
			}
		}
		else if ($cargo['anio_premio_cia']=='' && $cargo['anio_premio_cuerpo']=='') {
			$id_fila_vacia=1;
		}
	}
	
	$id_fila_vacia=0;
	//$_POST['rut'], $id, $anotacion['anio'], $anotacion['descripcion'], $anotacion['id_tipo_anotacion']
	foreach($dats['bombero_anotaciones'] as $id=>$cargo){
		if($cargo['fecha']!='' || $cargo['id_tipo_anotacion']!='' || $cargo['descripcion']!='') {
			if($cargo['fecha']=='') $errores[]='Anotación: Campo fecha vacío';
			if($cargo['descripcion']=='') $errores[]='Anotación: Campo descripción vacío';
			if($cargo['id_tipo_anotacion']=='') $errores[]='Anotación: Campo tipo de anotación vacío';
			if($id_fila_vacia==1) {
				$errores[]='Anotaciones: No se pueden saltar filas';
				$id_fila_vacia=0;
			}
		}
		else if ($cargo['fecha']=='' && $cargo['id_tipo_anotacion']=='' && $cargo['descripcion']=='') {
			$id_fila_vacia=1;
		}
	}

	
	$id_fila_vacia=0;
	//$_POST['rut'], $id, $anotacion['anio'], $anotacion['descripcion'], $anotacion['id_tipo_anotacion']
	foreach($dats['bombero_cursos'] as $id=>$cargo){
		if($cargo['id_tipo_curso']!='' || $cargo['fecha']!='' || $cargo['descripcion']!='') {
			if($cargo['id_tipo_curso']=='') $errores[]='Cursos: Campo tipo de curso vacío';
			if($cargo['fecha']=='') $errores[]='Cursos: Campo fecha vacío';
			if($cargo['descripcion']=='') $errores[]='Cursos: Campo descripción vacío';
			if($id_fila_vacia==1) {
				$errores[]='Cursos: No se pueden saltar filas';
				$id_fila_vacia=0;
			}
		}
		else if($cargo['id_tipo_curso']=='' && $cargo['fecha']=='' && $cargo['descripcion']=='') {
			$id_fila_vacia=1;
		}
	}

//$_POST['rut'], $id, $cargo['id_cod_cargo'], $cargo['fecha_inicio'], 
	//			 		$cargo['fecha_termino'], $cargo['descripcion']
				 			
	/***
	if(trim($dats['nombre'])=='')
		$errors[]='Campo nombre está vacío';
	if(trim($dats['apellido'])=='')
		$errors[]='Campo apellido está vacío';
	if(trim($dats['rut'])==''){
		$errors[]='Campo Rut está vacío';
	}
	else{
		$dv=obtener_digito_verificador($dats['rut']);
		$dats['rut_dv']=strtoupper($dats['rut_dv']);
		if($dv!=$dats['rut_dv'])
			$errors[]='Digito verifivador del Rut está incorrecto';
	}
	if(trim($dats['profesion'])=='')
		$errors[]='Campo profesión vacío';

	if(trim($dats['id_est_civil'])==''){
		$errors[]='Estado civil no seleccionado';
	}
//	else{
//	if(verificar_estadocivil()==false){
//			$errors[]='El estado civil no existe en la base de datos';
//		}
//	}
	if(trim($dats['id_est_bombero'])==''){
		$errors[]='Estado bombero no seleccionado';
	}

//	if(trim($dats['direc_'])=='')
//		$errors[]='Campo direccion esta vacio';
	if(trim($dats['fecha_nac_d'])=='')
		$errors[]='Campo dia fecha de nacimiento esta vacío';
	if(trim($dats['fecha_nac_m'])=='')
		$errors[]='Campo mes fecha de nacimiento esta vacío';
	if(trim($dats['fecha_nac_a'])=='')
		$errors[]='Campo year fecha de nacimiento esta vacío';
	****/
	return $errores;
}

function obtener_digito_verificador($rut){
	$d=false;
	$rut = strrev($rut);
	$aux = 1;
	$s = 0;
	for($i=0;$i<strlen($rut);$i++){
		$aux++;
		$s += intval($rut[$i])*$aux;
		if($aux == 7){ $aux=1; }
	}
	$digit = 11-$s%11;
	if($digit == 11){$d = 0;}
	elseif($digit == 10){$d = "K";}
	else{$d = $digit;}
	return $d;
}