<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include '../../comun.inc';
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_reportes.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_cargos.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');




//$_POST['uname'] = trim($_POST['uname']); //eliminamos los espacios en blanco	

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$m_reportes = new m_reportes($con);
$t_usuario = new usuario($con);
$t_bombero = new bombero($con);
$m_bomberos = new m_bomberos($con);
$m_cargos = new m_cargos($con);

$errores=verificar_errores($t_usuario);
//if($t_bombero->existe_bombero($_POST['rut'])==false)
	//$errores[]='Rut ingresado no se encuentra registrado';


//$t_usuario = new usuario($con);
//$t_tipo_usuario = new tipo_usuario($con);
//$form['uname'] = $_POST['uname'];
//$form['descripcion_tipo_usuario'] = $t_tipo_usuario->obtener_descripcion($_POST['tipo_usuario']);
if(isset($_POST['confirmar'])){ //segunda pasada y en esta presionó el botón "confirmar"
	if($errores==array()) {
		if(isset($_POST['eliminar'])){
			foreach($_POST['ruts'] as $rut)
				$m_bomberos->eliminar_bombero($rut,''); // se elimina definitivamente, también se tienen que eliminar los datos vincualos al bombero
				$archivo= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'.jpg';
				$archivo_t= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_t.jpg';
				$archivo_s= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_s.jpg';
				$archivo_m= sprintf(NOM_ARCHIVO_FOTO_BOMBERO, $_POST['rut']).'_m.jpg';
				//se borran la foto y las fotos redimencionadas
				if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo))
					unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo);
				if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_t))
					unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_t);
				if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_s))
					unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_s);
				if(is_file(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_m))
					unlink(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.DIRECTORIO_FOTO.'/'.$archivo_m);
		}
		else if(isset($_POST['restablecer'])) {
			foreach($_POST['ruts'] as $rut)
				$t_bombero->set_borrado($rut, ''); //el vacío lo dejará como null, osea, como no borrado
		}
	}	
	header('location: reportes.php');
}
else {
	
	foreach($_POST['ruts'] as $rut){
		$datos_basic_b = $t_bombero->obtener_datos_basicos($rut, '1'); // 1 para forzar que muestre el borrado
		$form['rut'][] = $rut;
		$form['rut_dv'][] = $datos_basic_b['rut_dv'];
		$form['nombre'][]= $datos_basic_b['nombre'].' '.$datos_basic_b['apellido'];
		$form['cargos_act'][]=$m_cargos->obtener_cargos_actuales($rut);	
		//$form['bombero'][]=$t_bombero->obtener_bombero($rut);
		$form['cant_llamados_acargo'][]= $m_reportes->obtener_total_llamados($rut);
	}
	if(isset($_POST['eliminar'])){
		if($errores==array()){
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_eliminar_bomberos.php';
		}
		else{	
			$enlace_volver='bomberos_borrados.php';
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
		}
	}
	else if(isset($_POST['restablecer'])) {
		if($errores==array()){
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_confirmar_restablecer_bomberos.php';
		}
		else{	
			$enlace_volver='bomberos_borrados.php';
			include DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/administracion/v_mostrar_errores.php';
		}
	}
	else {
		header('location: reportes.php');
	}
	//print_r($_POST);
}//fin confirmar

function verificar_errores($t_usuario) {
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='Solo puede eliminar un usuario administrador';
	
	return $errores;
}