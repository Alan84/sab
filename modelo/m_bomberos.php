<?php

/****
*	Multi tablas
*
***/

class m_bomberos{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
   
   
   ////////consulta bomberos
	function obtener_bomberos($filtro_sql='', $orden='', $pagina=0, $registros_por_pagina=0) {
		 //obtenemos la lista de bomberos a consultar
		$sql="select b.rut, b.rut_dv, b.nombre, b.apellido, b.id_est_bombero,
			(select count(*) from llamado where b.rut=rut_acargo and borrado is null) as cant_acargo,
			(select count(*) from llamado_bomberos where b.rut=rut) as cant_asiste,
			((select count(*) from llamado where b.rut=rut_acargo)+(select count(*) from llamado_bomberos where b.rut=rut)) as cant_llamados,
			(select descripcion from est_bombero where b.id_est_bombero=id_est_bombero) as est_bombero,
	                b.fecha_actualiza, b.usuario_actualiza,
        	        (year(now())-year(fecha_nac)) as edad,
			(select pago_mes from pago_mes pm, pago p
				where pm.id_pago = p.id_pago and p.rut = b.rut and p.borrado is null order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as cuota,
			
			(select month(pago_mes)
	                	from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and p.borrado is null  order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as fecha_vence_m,
        	        (select year(pago_mes)
                		from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and p.borrado is null  order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as fecha_vence_a
	                 from bombero b
        	        where 1=1 
                	 $filtro_sql "; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
                	 
                	 /* (select pago_mes
	                	from pago_mes pm where pm.id_pago = p.id_pago and p.rut=b.rut order by id_pago desc, id_pago_mes desc limit 0,1) as ultima_cuota,
	                	*/

		if($orden!='')
			$sql.=" order by $orden";
			//echo $sql;
		$query=$this->con->query($sql);
		$result=array();
		if($query!==false)
        		$result=$query->fetchAll(PDO::FETCH_ASSOC);
        		
		return $result;
	}
	
	////////ver bomberos
	function obtener_premios($rut) {
		$datos=array();
		$sql="select * from premio_anios where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		//foreach($result as $row)
			//$datos[]=$row;
		return $result;
	}
	
	function obtener_anotaciones($rut){
		$sql="select a.*, ta.descripcion tipo_anotacion_descripcion from anotacion a, tipo_anotacion ta where a.rut=? and a.id_tipo_anotacion=ta.id_tipo_anotacion";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	function obtener_cursos($rut) {
		$sql="select c.*, tc.descripcion tipo_curso_descripcion from curso c, tipo_curso tc where c.rut=? and c.id_tipo_curso=tc.id_tipo_curso order by c.id_curso asc";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	function obtener_cargos_hst($rut) {
		$sql="select c.*, cc.descripcion cod_cargo_descripcion from cargo_hst c, cod_cargo cc where c.rut=? and c.id_cod_cargo=cc.id_cod_cargo order by c.id_cargo";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
		
	function obtener_cargos_actuales($rut) {
		$sql="select ca.*, cc.descripcion from cargo_actual ca, cod_cargo cc where ca.rut=? and ca.id_cargo_actual=cc.id_cod_cargo order by ca.id_cargo_actual";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	function obtener_datos_bombero($rut) {
		//(select cc.descripcion from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut)
		//(select ca.fecha_inicio from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut)
		$sql="select b.*, 
				(select descripcion from est_civil where b.id_est_civil=id_est_civil)  as est_civil_descripcion, 
				eb.descripcion as est_bombero_descripcion, 
			 (select cc.descripcion from cargo_actual ca, cod_cargo cc 
			 	where ca.id_cargo_actual = cc.id_cod_cargo and ca.rut=b.rut limit 0,1) as cod_cargo_descripcion,
				(select ca.fecha_inicio from cargo_actual ca where ca.rut=b.rut limit 0,1) as fecha_ini_cargo 
			from bombero b, est_bombero eb
			where b.rut=? and 
				eb.id_est_bombero=b.id_est_bombero"; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
				
				
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	//////fin ver bomberos

	
	function eliminar_bombero($rut) {
		//hay que eliminar de otras tablas primero
		$this->usuario_set_rut_null($rut);
		$this->eliminar_bombero_tabla_anotacion($rut);
		$this->eliminar_bombero_tabla_cargo_actual($rut);
		$this->eliminar_bombero_tabla_cargo_hst($rut);
		$this->eliminar_bombero_tabla_curso($rut);
		$this->eliminar_bombero_tabla_llamado($rut);
		$this->eliminar_bombero_tabla_llamado_bombero($rut);
		
		$this->eliminar_bombero_tabla_pago($rut);
		$this->eliminar_bombero_tabla_pago_mes($rut);
		
		$this->eliminar_bombero_tabla_premio_anios($rut);
		
		//finalmente lo borramos de la tabla bombero 
		$sql="delete from bombero where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		
		//print_r($query->errorInfo());
	}
	
	function obtener_bomberos_morosos($filtro_sql='', $orden='', $morosidad=1, $pagina=0, $registros_por_pagina=0) {
		//$morosidad-=1; //mayor o igual
		 //obtenemos la lista de bomberos a consultar
		$sql="select b.rut, b.rut_dv, b.nombre, b.apellido, b.id_est_bombero, b.email, b.fecha_mail_alerta,
			(select descripcion from est_bombero where b.id_est_bombero=id_est_bombero) as est_bombero,
	                b.fecha_actualiza, b.usuario_actualiza,
        	        (year(now())-year(fecha_nac)) as edad,
			(select pago_mes from pago_mes pm, pago p
				where pm.id_pago = p.id_pago and p.rut = b.rut and p.rut = pm.rut and p.borrado is null order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as cuota,
			
			(select month(pago_mes)
	                	from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and p.rut = pm.rut  and p.borrado is null order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as fecha_vence_m,
        	     (select year(pago_mes)
                		from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and p.rut = pm.rut  and p.borrado is null order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1) as fecha_vence_a 
                		
                		
                		
	                 from bombero b
        	        where (((select pago_mes from pago_mes pm, pago p 
								where pm.id_pago = p.id_pago and p.rut = b.rut and 
									p.rut = pm.rut and p.borrado is null  
									order by pm.pago_mes desc LIMIT 0, 1)<date_sub(CURRENT_DATE, interval ( $morosidad ) month)) or (select pago_mes from pago_mes pm, pago p 
								where pm.id_pago = p.id_pago and p.rut = b.rut and 
									p.rut = pm.rut and p.borrado is null
									order by pm.pago_mes desc LIMIT 0, 1) is null ) 
        	        
        	        
        	               		
                		
							
                	 $filtro_sql "; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
                	 //echo $sql;
                	 /* (select pago_mes
	                	from pago_mes pm where pm.id_pago = p.id_pago and p.rut=b.rut order by id_pago desc, id_pago_mes desc limit 0,1) as ultima_cuota,
	                	*/

		if($orden!='')
			$sql.=" order by $orden";
			
		//echo $sql;
		$query=$this->con->query($sql);
		$result=array();
		if($query!==false)
        		$result=$query->fetchAll(PDO::FETCH_ASSOC);
        		
		return $result;
	}
	
	function eliminar_bombero_tabla_anotacion($rut){
		$sql="delete from anotacion where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function eliminar_bombero_tabla_cargo_actual($rut){
		$sql="delete from cargo_actual where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function eliminar_bombero_tabla_cargo_hst($rut){
		$sql="delete from cargo_hst where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function eliminar_bombero_tabla_curso($rut){
		$sql="delete from curso where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}

	//si solo fue asistente, se debe eliminar solo como asistente	
	function eliminar_bombero_tabla_llamado_bombero($rut){
		$sql="delete from llamado_bomberos where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		//print_r($query->errorInfo());
	}
	
	//si fue a cargo, hay que eliminar todo el llamado, en este caso habría que eliminar a todos los asistentes y luego al llamado en si.
	function eliminar_bombero_tabla_llamado($rut){
		$sql="select id_llamado from llamado where rut_acargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0])){ //si al menos hay un resultado
			foreach($result as $llam){ //rut con llamado a cargo, eliminamos los llamados
				$sql="delete from llamado_bomberos where id_llamado=?";
				$query=$this->con->prepare($sql);
				$query->execute(array($llam['id_llamado']));
			
				$sql="delete from llamado where id_llamado=?";
				$query=$this->con->prepare($sql);
				$query->execute(array($llam['id_llamado']));
				//print_r($query->errorInfo());
			}
		}
		
	}
	
	function eliminar_bombero_tabla_pago($rut){
		$sql="select id_pago from pago where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0])){ //si al menos hay un resultado
			foreach($result as $llam){ //rut con llamado a cargo, eliminamos los llamados
			
				$sql="delete from pago_mes where id_pago=?";
				$query=$this->con->prepare($sql);
				$query->execute(array($llam['id_pago']));
				//print_r($query->errorInfo());
			
				$sql="delete from pago where id_pago=?";
				$query=$this->con->prepare($sql);
				$query->execute(array($llam['id_pago']));
				//print_r($query->errorInfo());
			}
		}
		/*
		$sql="delete from pago where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));*/
		
	}
	
	function eliminar_bombero_tabla_pago_mes($rut){
		$sql="delete from pago_mes where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function eliminar_bombero_tabla_premio_anios($rut){
		$sql="delete from premio_anios where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function eliminar_bombero_tabla_usuario($rut){
		$sql="delete from usuario where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	function usuario_set_rut_null($rut){
		$sql="update usuario set rut=null, habilitado=null  where rut=?"; //para eliminar solo eliminamos el rut y lo seteamos como deshabilitado
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		// print_r($query->errorInfo());
	}
	
	
	//////////editar bomberos
	function obtener_bombero($rut) {
		$datos=array();
		/* (select YEAR(ca.fecha_inicio) from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut) as fecha_ini_cargo_a, 
				(select MONTH(ca.fecha_inicio) from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut) as fecha_ini_cargo_m, 
				(select YEAR(ca.fecha_inicio) from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut) as fecha_ini_cargo_d,
				
				
				
				(select cc.id_cod_cargo from cod_cargo cc, cargo_actual ca where cc.id_cod_cargo=ca.id_cargo_actual and ca.rut=b.rut) as id_cod_cargo
				*/ 
		$sql="select b.rut, b.rut_dv, b.nombre, b.apellido, b.sexo, b.profesion, b.id_est_civil,
				b.grup_san, b.direc_calle, b.direc_numero, b.direc_comuna, b.direc_region, b.direc_depto,
				b.email, b.email2, b.fono, b.fono2, b.fecha_nac,
				'' as fecha_ini_cargo_a, 
				'' as fecha_ini_cargo_m, 
				'' as fecha_ini_cargo_d, 
				b.fecha_ingreso,
				b.fecha_actualiza, b.id_est_bombero, b.num_registro, b.num_tib, b.num_placa, b.nombre_padre, b.nombre_madre, b.foto,
				b.comentarios,  
				'' as id_cod_cargo 
				from bombero b where b.rut=? and b.borrado is null ";
	
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0];
		else
			return false;
	}
	/////fin editar bomberos
	
	
	/////editar vida bomberos
	function obtener_cargos($rut) {
		$datos=array();
		// obtenemos los cargos
		$sql="select c.id_cod_cargo, c.id_cargo, c.descripcion, c.fecha_inicio, c.fecha_inicio, c.fecha_inicio, 
				c.fecha_termino, c.fecha_termino, c.fecha_termino, 
				cc.descripcion cod_cargo_descripcion from cargo_hst c, cod_cargo cc where c.rut=? and c.id_cod_cargo=c.id_cod_cargo";
			$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $id=>$row)
			$datos[$row['id_cargo']]=$row;
		return $datos;
	}
	
	function obtener_premios_array($rut) {
		$datos=array();
		// obtenemos los cargos
		$sql="select * from premio_anios where rut=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));

		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $id=>$row)
			$datos[$row['id_premio_anios']]=$row;
		return $datos;
	}
	
	function obtener_anotaciones_array($rut) {
		$datos=array();
		// obtenemos los cargos
		$sql="select a.*, ta.descripcion tipo_anotacion_descripcion from anotacion a, tipo_anotacion ta where a.rut=? and 
			a.id_tipo_anotacion=ta.id_tipo_anotacion";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $id=>$row)
			$datos[$row['id_anotacion']]=$row;
		return $datos;
	}
	
	function obtener_cursos_array($rut) {
		$datos=array();
		// obtenemos los cargos
		$sql="select c.*, tc.descripcion tipo_curso_descripcion from curso c, tipo_curso tc where c.rut=? and
			c.id_tipo_curso=tc.id_tipo_curso";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		foreach($query->fetchAll(PDO::FETCH_ASSOC) as $id=>$row)
			$datos[$row['id_curso']]=$row;
		return $datos;
	}
	
	function existe_anotacion($rut, $id) {
		$datos=array();
		// obtenemos los cargos
		$sql="select 1 from anotacion where rut=? and id_anotacion=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if(count($query->fetchAll(PDO::FETCH_ASSOC))>0)
			return true;
		else
			return false;
	}
	
	
	function existe_curso($rut, $id) {
		$datos=array();
		// obtenemos los cargos
		$sql="select 1 from curso where rut=? and id_curso=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if(count($query->fetchAll(PDO::FETCH_ASSOC))>0)
			return true;
		else
			return false;
	}
	
	function existe_premio_anios($rut, $id) {
		$datos=array();
		// obtenemos los cargos
		$sql="select 1 from premio_anios where rut=? and id_premio_anios=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if(count($query->fetchAll(PDO::FETCH_ASSOC))>0)
			return true;
		else
			return false;
	}
	
	function existe_cargo_hst($rut, $id) {
		$datos=array();
		// obtenemos los cargos
		$sql="select 1 from cargo_hst where rut=? and id_cargo=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut, $id));
		if(count($query->fetchAll(PDO::FETCH_ASSOC))>0)
			return true;
		else
			return false;
	}
	///fin editar vida bomberos	
	
}