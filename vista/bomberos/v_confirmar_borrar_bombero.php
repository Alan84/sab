<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Borrar bomberos') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-07T16:56:06-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar borrar datos</h1>

<form method="post" action="borrar_bombero.php">
	<p>Va a borrar un bombero, por favor, confirme la acción. 
	Esta acción, además de sus datos personales, también <strong>borrará datos vinculados a este, 
	como por ejemplo los llamados en los que estuvo a cargo</strong>, 
	si solo fue asistente, entonces solo se borrará como asistente, quedando el llamando.
	Al confirmar se realizará un <em>borrado lógico</em>, esto se puede revertir en el modulo administración.</p>
	<p>Recuerde siempre hacer respaldos, sobretodo antes de eliminar alguna información.</p>
	<table>
		<tr>
			<th>Bombero</th>
			<th>Llamados a cargo</th>
			<th>Usuario</th>
			<th>Cargo actual</th>
		</tr>
		<tr>
			<td><?php echo $bombero_datos_basicos;?></td>
			<td><?php echo $cant_llamados_acargo;?></td>
			<td><?php echo $usuario;?></td>
			<td>
			<?php if($cargos_act!=array()){ ?>
			<ul>
			<?php foreach($cargos_act as $carg){ ?>
				<li><?php echo $carg['descripcion'].' | '.$carg['fecha_inicio']; ?></li>
			<?php } ?>
			</ul>
			<?php } else { echo "<em>Sin cargo</em>"; } ?>
		</td>
		</tr>
	</table>
	
	<input type="hidden" name="rut" value="<?php echo $rut; ?>" />
	<p style="text-align: right"><input type="submit" name="confirmar" value="Borrado lógico" />  <input type="submit" name="cancelar" value="Cancelar" /></p>
</form>

</div>
</div>
</body>
</html>
