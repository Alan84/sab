<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Llamados') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:49-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">
<script>
	function resaltar_envio(){
		envio=document.getElementById('envio');
		envio.setAttribute("style","font-weight: 800");
	}
</script>
</head>
<body>
<div style="display: table; margin: 1em auto 0 auto">
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include('../../menu_admin.inc') ?>
</div>
<div style="clear: both"></div>
<div style="margin-top: 1em">
<h1 style="color: white; background-color: black">Lista de llamados</h1>
<!--
<form method="get" action="consulta_bomberos.php">


<h2 style="display: inline">Filtro</h2>
<input type="search" onchange="resaltar_envio()" name="buscar" value="<?php echo $filtro_buscar ?>" style="float:right" /><br />

<input type="submit" id="envio" value="¡filtrar!" />
</form> -->
<?php if($pagina>0){ echo '<p>'; for($x=1; $x<=ceil($total_registros/REGISTROS_POR_PAGINA); $x++){ ?>
	<a href="consulta_llamados.php?pagina=<?php echo $x; ?>&orden=<?php echo $orden;?>&tipo_orden=<?php echo $tipo_orden; ?>#historico" class="<?php echo ($x==$pagina)? 'pagina_actual': 'pagina'; ?>"><?php echo $x; ?></a>
<?php } echo '</p>'; } ?>
<form method="post" action="confirmar_borrar_llamados.php">
<table class="tabla">
<thead>
<tr>
	<!-- <th>selecionar</th> -->
	<th><a href="consulta_llamados.php?orden=id<?php echo $orden_link_id.$link_busqueda ?>" style="color: white">id<?php echo $simbolo_orden_id ?></a></th>
	<th><a href="consulta_llamados.php?orden=tipo_llamado<?php echo $orden_link_tipo.$link_busqueda ?>" style="color: white">tipo<?php echo $simbolo_orden_tipo ?></a></th>
	<th><a href="consulta_llamados.php?orden=rut<?php echo $orden_link_rut.$link_busqueda ?>" style="color: white">rut a cargo<?php echo $simbolo_orden_rut ?></a></th>
	<th colspan="2"><a href="consulta_llamados.php?orden=fecha<?php echo $orden_link_fecha.$link_busqueda ?>" style="color: white">salida y llegada al cuartel<?php echo $simbolo_orden_fecha ?></a></th>
	<th><a href="consulta_llamados.php?orden=horas<?php echo $orden_link_horas.$link_busqueda ?>" style="color: white">hrs <?php echo $simbolo_orden_horas ?></a></th>

	<th><a href="consulta_llamados.php?orden=dir<?php echo $orden_link_dir.$link_busqueda ?>" style="color: white">dirección<?php echo $simbolo_orden_dir ?></a></th>
	<th><a href="consulta_llamados.php?orden=asisten<?php echo $orden_link_asisten.$link_busqueda ?>" style="color : white">asis.<?php echo $simbolo_orden_asisten ?></a></th>
	<th><a href="consulta_llamados.php?orden=user<?php echo $orden_link_user.$link_busqueda ?>" style="color : white">usuario<?php echo $simbolo_orden_user ?></a></th>
	<th>Sel</th>
</tr>
</thead>
<tbody>
<?php $i=0; foreach($llamados as $llamado){ ?>
	<tr>
		<td style="text-align: center"><a href="ver_llamado.php?id=<?php echo $llamado['id_llamado']; ?>" ><?php echo $llamado['id_llamado']; ?></a></td>
		<td style="text-align: center"><?php echo $llamado['cod_tipo_llamado'] ?></td>
		<td style="text-align: right"><a href="../bomberos/ver_bombero.php?rut=<?php echo $llamado['rut_acargo'] ?>"><?php echo number_format($llamado['rut_acargo'], 0, ',', '.').'-'.$llamado['rut_dv'] ?></a></td>
		<td style="text-align: center"><?php echo $llamado['fecha_ini']; ?></td>
		<td style="text-align: center"><?php echo $llamado['fecha_fin']; ?></td>
		<td style="text-align: right"><?php echo $llamado['horas']; ?></td>
		<td style="text-align: left"><?php echo mostrar_num_romanos($llamado['dir_region']).' '.$llamado['dir_comuna'].' '.$llamado['dir_calle'].' '.$llamado['dir_numero']; ?></a></td>
		<td style="text-align: center"><?php echo $llamado['asistentes']; ?></td>
		<td style="text-align: center"><?php echo $llamado['usuario']; ?></td>
		<td style="text-align: center"><input type="checkbox" name="ids[<?php echo $llamado['id_llamado']; ?>]" value="<?php echo $llamado['rut_acargo']; ?>" /></td>
	</tr>
<?php } ?>
<tbody>
</table>

<p><input type="submit" name="borrar" value="Borrar" style="float: right" /></p>
</form>
</div>
</div>
</body>
</html>
