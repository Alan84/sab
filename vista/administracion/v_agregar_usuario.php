<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Agregar usuario') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T20:56:17-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>



</div>


<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">

<h1 style="color: white; background-color: black">Usuarios</h1>
	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/confirmar_usuario.php" method="post">
			<table><tr><td>nombre de usuario:</td><td><input type="text" name="uname" value="<?php echo $form['uname'] ?>" /></td></tr>
			<tr><td>password:</td><td><input type="password" name="upass" value="" /> * El password se almacenará codificado</td></tr>
			<tr><td>tipo:</td><td><select name="tipo_usuario">
			<option value=""></option>
			<?php foreach($tipos_usuarios as $tipo){ ?>
				<?php $sel=($form['tipo_usuario'] == $tipo['id'])?' selected="selected"':''; ?>
				<option value="<?php echo $tipo['id'] ?>" <?php echo $sel ?>><?php echo $tipo['descripcion'] ?></option>
			<?php } ?>
			</select></td></tr>
			<tr><td>rut bombero:</td><td><input type="text" name="rut" value="<?php echo $form['rut'] ?>" /> <span style="font-weight: bold;">-</span> <input type="text" name="rut_dv" value="<?php echo $form['rut_dv'] ?>" maxlength="2" style="width: 2em" /> * Rut al que se vinculará el usuario</td></tr>
			<tr><td>Habilitado:</td><td><input type="checkbox" name="habilitado" value="1" <?php echo $habilitado_checked ?> /> * permite que el usuario pueda ingresar.</td></tr>
			</table>			
			<input type="submit" name="enviar" value="Cancelar" /> <input type="submit" name="enviar" value="Agregar" />
	</form>
</div>
</body>
</html>