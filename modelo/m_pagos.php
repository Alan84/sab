<?php

class m_pagos{
	var $con;
	function __construct($conexion) {
		$this->con=$conexion;
	}


	function guardar_pago($rut, $fechas, $monto, $comentario, $usuario_actualiza) {
		$this->con->beginTransaction();
		$indice = $this->obtener_ultimo_indice();
		$indice+=1;
		$fecha_actual = date('Y-m-d h:i:s');
 		$sql="insert into pago (rut, id_pago, fecha_ingreso, monto, comentario, usuario)
				values (?, ?, ?, ?, ?, ?)";

		$q=$this->con->prepare($sql);
		$q->execute(array($rut, $indice, $fecha_actual, $monto, $comentario, $usuario_actualiza));
		
		
		foreach($fechas as $fecha)
			$this->guardar_fecha($indice, $fecha.'-01'); // agregamos el día para que no tega problema en almacenarlo
			
		
		$this->con->commit();
		//exit;
		return array($indice, $fecha_actual);
	}

	function obtener_ultimo_indice() {
 		$sql="select id_pago  from pago order by id_pago desc limit 0,1";
 		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['id_pago'];
		else
			return '0'; //si no hay
	}

	function obtener_ultimo_indice_pago_mes($id_pago) {
 		$sql="select id_pago_mes from pago_mes where id_pago=? order by id_pago_mes desc limit 0,1";
 		$query=$this->con->prepare($sql);
		$query->execute(array($id_pago));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['id_pago_mes'];
		else
			return '0';
	}

	function guardar_fecha($id_pago, $fecha){
		$id_pago_mes = $this->obtener_ultimo_indice_pago_mes($id_pago);
		$id_pago_mes +=1; 
 		$sql="insert into pago_mes (id_pago, id_pago_mes, pago_mes)
				values (?, ?, ?)";
 		// echo "$sql $rut, $id_pago, $indice+1, $fecha \n";

		$q=$this->con->prepare($sql);
		$q->execute(array($id_pago, $id_pago_mes, $fecha));
		
		//echo " $sql \n - $rut - $id_pago - $id_pago_mes - $fecha \n\r ";
		//print_r($q->errorInfo());
	}
	
	
	function cantidad_cuotas_impagas($rut){
			$sql="select period_diff(date_format(current_date,'%Y%m'), date_format(pm.pago_mes,'%Y%m')) as cuotas_impagas
						from pago p,pago_mes pm where p.rut=? and p.id_pago = pm.id_pago and p.borrado is null 
							order by pm.id_pago desc , pm.id_pago_mes desc limit 0,1";
			$query=$this->con->prepare($sql);
			$query->execute(array($rut));
			$result=$query->fetchAll(PDO::FETCH_ASSOC);
			
			if(isset($result[0]))
				return $result[0]['cuotas_impagas'];
			else
				return false;
		}
		
		
	function obtener_ultima_fecha_pago($rut) {
 		$sql="select pago_mes from pago_mes where rut=? order by id_pago_mes, id_pago desc limit 0,1";
 		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['pago_mes'];
		else
			return false;
	}

	function obtener_pagos_borradas($orden='', $tipo_orden='', $pagina=0, $total_por_pagina=0) {
		$inicio= $pagina*$total_por_pagina;
		$sql="select p.rut, p.id_pago, DATE_FORMAT(p.fecha_ingreso, '%d %M %Y - %T') fecha_ingreso, 
			month(p.fecha_vence) fecha_vence_m, year(p.fecha_vence) fecha_vence_a,
			monto, comentario, fecha_mail_alerta, usuario, (select count(pm.id_pago) from pago_mes pm where pm.id_pago = p.id_pago) as pago_mes
			from pago p where p.borrado=1 ";
		
		
		if($orden!='' and $tipo_orden!='')
			$sql.=" order by $orden $tipo_orden ";
		
		if($total_por_pagina>0)
			$sql.=" limit $inicio, $total_por_pagina";
			
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	
	function obtener_total_registros_borrados() {
		$sql="select count(*) total
			from pago where borrado=1 order by id_pago desc";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]['total'];
	}
	
	function obtener_pagos($orden='', $tipo_orden='', $pagina=0, $total_por_pagina=0) {
		$inicio= $pagina*$total_por_pagina;
		$sql="select p.rut, b.rut_dv, p.id_pago, DATE_FORMAT(p.fecha_ingreso, '%d %M %Y - %T') fecha_ingreso, 
			month(p.fecha_vence) fecha_vence_m, year(p.fecha_vence) fecha_vence_a,
			p.monto, p.comentario, p.fecha_mail_alerta, usuario, (select count(pm.id_pago) from pago_mes pm where pm.id_pago = p.id_pago) as pago_mes
			from pago p, bombero b where p.rut = b.rut and p.borrado is null and b.borrado is null ";
		
		
		if($orden!='' and $tipo_orden!='')
			$sql.=" order by $orden $tipo_orden ";
		
		if($total_por_pagina>0)
			$sql.=" limit $inicio, $total_por_pagina";
			
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	
	function obtener_total_registros() {
		$sql="select count(*) total
			from pago where borrado is null order by id_pago desc";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]['total'];
	}
	
	//////////ver pagos
	function obtener_pagos_bombero($rut, $orden='', $tipo_orden='', $pagina=0, $total_por_pagina=0) {
		$inicio= $pagina*$total_por_pagina;
		$sql="select p.id_pago, DATE_FORMAT(p.fecha_ingreso, '%d %M %Y - %T') fecha_ingreso, 
			month(p.fecha_vence) fecha_vence_m, year(p.fecha_vence) fecha_vence_a,
			monto, comentario, fecha_mail_alerta, usuario, (select count(pm.id_pago) from pago_mes pm where pm.id_pago = p.id_pago) as pago_mes
			from pago p where p.rut=? and p.borrado is null ";
		
		
		if($orden!='' and $tipo_orden!='')
			$sql.=" order by $orden $tipo_orden ";
		
		if($total_por_pagina>0)
			$sql.=" limit $inicio, $total_por_pagina";
			
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	
	function obtener_total_registros_por_rut($rut) {
		$sql="select count(*) total
			from pago where rut=? order by id_pago desc";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result[0]['total'];
	}
	
	function obtener_ultima_fecha_vencimiento($rut){
		$sql="select month(pm.pago_mes) fecha_vence_m, year(pm.pago_mes) fecha_vence_a,
		month(DATE_ADD(pm.pago_mes, INTERVAL 1 MONTH)) fecha_siguiente_m,
		year(DATE_ADD(pm.pago_mes, INTERVAL 1 MONTH)) fecha_siguiente_a 
		from pago_mes pm, pago p where p.rut=? and pm.id_pago= p.id_pago and borrado is null order by p.id_pago desc, pm.id_pago_mes desc limit 0,1";
		$query=$this->con->prepare($sql);
		$query->execute(array($rut));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	
	function obtener_meses($id_pago){
		$sql="select pago_mes, month(pago_mes) pago_mes_m , year(pago_mes) pago_mes_a from pago_mes where id_pago=? order by id_pago_mes asc";
		$query=$this->con->prepare($sql);
		$query->execute(array($id_pago));
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	
	//////////fin ver pagos
}