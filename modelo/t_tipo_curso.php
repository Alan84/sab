<?php

class tipo_curso{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_tipos_cursos() {
		$datos=array();
		$sql="select * from tipo_curso";
		foreach($this->con->query($sql) as $row)
			$datos[$row['id_tipo_curso']]=$row['descripcion'];

		return $datos;
	}
}
