<?php
session_start();

if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ..');
}

include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_civil.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cargo_hst.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_anotacion.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_cargo.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_anio.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_tipo_curso.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_est_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_parametro.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_premio_anios.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_anotacion.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_curso.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/m_bomberos.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$rut='';
if(isset($_GET['rut']) and is_numeric($_GET['rut']))
        $rut=$_GET['rut'];

$t_est_civil = new est_civil($con);
$t_cargo_hst = new cargo_hst($con);
$t_cod_anio = new cod_anio($con);
$t_est_bombero = new est_bombero($con);
$t_tipo_curso = new tipo_curso($con);
$t_tipo_anotacion = new tipo_anotacion($con);
$t_tipo_cargo = new tipo_cargo($con);
$t_premio_anios = new premio_anios($con);
$t_anotacion = new anotacion($con);
$t_curso = new curso($con);
$t_parametro = new parametro($con);

// obtenemos los estados civiles desde la base de datos para usarlos en el combo
$estado_civil = $t_est_civil->obtener_estados_civiles();

/* // obtenemos los estados civiles desde la base de datos para usarlos en el combo
$anios_servicio = $t_cod_anio->obtener_anios_servicios();
*/

$anios_multiplo = $t_parametro->obtener_parametro('premio_por_anios');

// obtenemos los estados bomberos desde la base de datos para usarlos en el combo
$estados_bombero = $t_est_bombero->obtener_estados_bomberos();

// obtenemos los tipos de cursos desde la base de datos para usarlos en el combo
$cursos = $t_tipo_curso->obtener_tipos_cursos();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
$tipo_anotaciones = $t_tipo_anotacion->obtener_tipos_anotaciones();

// obtenemos los tipos de cargo desde la base de datos para usarlos en el combo
$tipo_cargos = $t_tipo_cargo->obtener_tipos_cargos();


//filas a mostrar
$cantidad_anios_servicio=CANTIDAD_ANIOS_SERVICIO_INICIO;
$cantidad_anotaciones=CANTIDAD_ANOTACIONES_INICIO;
$cantidad_cursos=CANTIDAD_CURSOS_INICIO;
$cantidad_cargos=CANTIDAD_CARGOS_INICIO;

$valores_form = array();

/****
 en este punto se va a poner un poco complejo el seteo de variables, 
 la idea es setear en vacio las primeras, 
 después llenarlas con datos, si los hay, 
 finalmente se setean en vacío por n espacios más. 
 esto para que se pueden agregar mas información sin requerir tanto de javascript, quien los agragará dinámicamente  
***/
/////for($i=1; $i<=count($anios_servicio); $i++){
//$i2=1; 
//for($i=$anios_multiplo; $i<=($anios_multiplo*5); $i=$i+$anios_multiplo){ //puede hacerse más simple
for($i=1; $i<=$cantidad_anios_servicio; $i++){
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cuerpo']='';
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cia']='';
	$valores_form['bombero_premios_anios'][$i]['id_cod_anio']='';
	
} 

/*
if(isset($_POST['anio']) and count($_POST['anio'])>0){
	for($i=1;$i<count($_POST['anio']);$i++){
		if($_POST['anio'][$i]!='' and $cantidad_anios_servicio<$i){
				$cantidad_anios_servicio=$i;
		}
	}
}

if(isset($_POST['anotacion_anio']) || isset($_POST['id_anotacion']) || isset($_POST['anotacion_descripcion'])){
	for($i=0;$i<count($_POST['anotacion_anio']);$i++){
		if($i>=CANTIDAD_ANOTACIONES_INICIO)
			$cantidad_anotaciones=$i+1;
	}
}

if(isset($_POST['curso_anio']) || isset($_POST['id_curso']) || isset($_POST['curso_descripcion'])){
	for($i=0;$i<count($_POST['curso_anio']);$i++){
		if($i>=CANTIDAD_CURSOS_INICIO)
			$cantidad_cursos=$i+1;
	}
}
*/

//$cantidad_anotaciones=20;

// seteamos los meses para usarlos en el combo
$meses=array(1=> 'Enero', 2=> 'Febrero', 3=>'Marzo', 4=> 'Abril', 5=> 'Mayo', 6=> 'Junio',
	7=> 'Julio' , 8=> 'Agosto', 9=> 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');

$sql="select rut, nombre, apellido from bombero order by fecha_actualiza desc limit 0,1"; //obtenemos el ultimo usuario agregado para mostrarlo en pantalla
$query=$con->query($sql);
$result=$query->fetchAll(PDO::FETCH_ASSOC);

$ultimo['rut']=$ultimo['nombre']=$ultimo['apellido']='';
if($result!=array())
	$ultimo=$result[0];

$rut_dv=$direc_calle=$direc_numero=$direc_depto=$direc_region=$direc_comuna='';
$nombre=$apellido=$nombre_padre=$nombre_madre=$tel_movil=$tel_fijo='';
$fecha_ingreso_d=$fecha_ingreso_m=$fecha_ingreso_a=$fecha_nac_d=$fecha_nac_m=$fecha_nac_a='';
$profesion=$grup_san=$email_quinta=$email2=$sexo=$sel_sexo_m=$sel_sexo_f='';
$id_est_civil=$id_est_bombero=$num_registro=$num_tib=$num_placa='';
$curso_tipo=array();
$anotacion_descripcion=array();

/***
 for($i=1; $i<=count($anios_servicio); $i++){
	$bombero_premios_anios[$i]['anio_premio_cia']='';
	$bombero_premios_anios[$i]['anio_premio_cuerpo']='';
} */




$ultimo_id_cargo = $t_cargo_hst->obtener_ultimo_id($rut);

//if($ultimo_id_cargo > $cantidad_cargos)
	//$cantidad_cargos = $ultimo_id_cargo;
//seteamos en vacío los primeros
for($i=1; $i<=$ultimo_id_cargo; $i++){
	$valores_form['bombero_cargos'][$i]['id_cargo']='';
	$valores_form['bombero_cargos'][$i]['id_cod_cargo']='';
	$valores_form['bombero_cargos'][$i]['fecha_inicio']='';
	$valores_form['bombero_cargos'][$i]['fecha_termino']='';
	$valores_form['bombero_cargos'][$i]['descripcion']='';
} 

$ultimo_id_premio_anios = $t_premio_anios->obtener_ultimo_id($rut);
for($i=1; $i<=$ultimo_id_premio_anios; $i++){
	$valores_form['bombero_premios_anios'][$i]['anios']=$i*$anios_multiplo;
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cuerpo']='';
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cia']='';
	$valores_form['bombero_premios_anios'][$i]['id_cod_anio']='';
}

$ultimo_id_anotacion = $t_anotacion->obtener_ultimo_id($rut);
for($i=1; $i<=$ultimo_id_anotacion; $i++){
	$valores_form['bombero_anotaciones'][$i]['id_anotacion']='';
	$valores_form['bombero_anotaciones'][$i]['id_tipo_anotacion']='';
	$valores_form['bombero_anotaciones'][$i]['fecha']='';
	$valores_form['bombero_anotaciones'][$i]['descripcion']='';
}

$ultimo_id_curso = $t_curso->obtener_ultimo_id($rut);
for($i=1; $i<=$ultimo_id_curso; $i++){
	$valores_form['bombero_cursos'][$i]['id_curso']='';
	$valores_form['bombero_cursos'][$i]['id_tipo_curso']='';
	$valores_form['bombero_cursos'][$i]['anio']='';
	$valores_form['bombero_cursos'][$i]['fecha']='';
	$valores_form['bombero_cursos'][$i]['descripcion']='';
	$valores_form['bombero_cursos'][$i]['certificado']='';
}

$premio_anios=array();
$m_bomberos = new m_bomberos($con);
//$i=1;
$ultimoid=0;
foreach($m_bomberos->obtener_cargos($rut) as $row){
	$valores_form['bombero_cargos'][$row['id_cargo']]=$row;
	$ultimoid=$row['id_cargo'];
}
	
//seteamos vacío los siguientes, para que muestre mas filas, asi se pueden agregar datos mas fácilmente, sin depender mucho de javascript
for($i=$ultimoid+1; $i<=($cantidad_cargos+$ultimoid); $i++){
	$valores_form['bombero_cargos'][$i]['id_cargo']='';
	$valores_form['bombero_cargos'][$i]['id_cod_cargo']='';
	$valores_form['bombero_cargos'][$i]['fecha_inicio']='';
	$valores_form['bombero_cargos'][$i]['fecha_termino']='';
	$valores_form['bombero_cargos'][$i]['descripcion']='';
}

$ultimoid=0;
foreach($m_bomberos->obtener_premios($rut) as $i=>$row){
	$valores_form['bombero_premios_anios'][$row['id_premio_anios']]=$row;
	$ultimoid=$row['id_premio_anios'];
}
	
	
//seteamos vacío los siguientes, para que muestre mas filas, asi se pueden agregar datos mas fácilmente, sin depender mucho de javascript
for($i=$ultimoid+1; $i<=($cantidad_anios_servicio+$ultimoid); $i++){
	$valores_form['bombero_premios_anios'][$i]['anios']=$i*$anios_multiplo;
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cuerpo']='';
	$valores_form['bombero_premios_anios'][$i]['anio_premio_cia']='';
	$valores_form['bombero_premios_anios'][$i]['id_cod_anio']='';
}
//print_r($valores_form['bombero_premios_anios']);

$ultimoid=0;
foreach($m_bomberos->obtener_anotaciones($rut) as $i=>$row){
	$valores_form['bombero_anotaciones'][$row['id_anotacion']]=$row;
	$ultimoid=$row['id_anotacion'];
}

//print_r($valores_form['bombero_anotaciones']);
	
for($i=$ultimoid+1; $i<=$cantidad_anotaciones+$ultimoid; $i++){
	$valores_form['bombero_anotaciones'][$i]['id_anotacion']='';
	$valores_form['bombero_anotaciones'][$i]['id_tipo_anotacion']='';
	$valores_form['bombero_anotaciones'][$i]['fecha']='';
	$valores_form['bombero_anotaciones'][$i]['descripcion']='';
}

$ultimoid=0;
foreach($m_bomberos->obtener_cursos($rut) as $i=>$row){
	$valores_form['bombero_cursos'][$row['id_curso']]=$row;
	$ultimoid=$row['id_curso'];
}

for($i=$ultimoid+1; $i<=$cantidad_cursos+$ultimoid; $i++){
	$valores_form['bombero_cursos'][$i]['id_curso']='';
	$valores_form['bombero_cursos'][$i]['id_tipo_curso']='';
	$valores_form['bombero_cursos'][$i]['anio']='';
	$valores_form['bombero_cursos'][$i]['fecha']='';
	$valores_form['bombero_cursos'][$i]['descripcion']='';
	$valores_form['bombero_cursos'][$i]['certificado']='';
}

foreach($_POST as $indice => $dato){
	if(is_array($_POST[$indice])){
		foreach($_POST[$indice] as $indice2=>$dato2){
			if(is_array($_POST[$indice][$indice2])){
				foreach($_POST[$indice][$indice2] as $indice3=>$dato3){
					$valores_form[$indice][$indice2][$indice3]=$dato3;
				}
			}
			else {
				$valores_form[$indice][$indice2]=$dato2;
			}
			/****************
			switch($indice) {
				case 'anio': //error de php, no se puede poner el nombre en forma dinámica
					$anio[$indice2]= $dato2;
					break;
				case 'anotacion_descripcion': //error de php, no se puede poner el nombre en forma dinámica
					$anotacion_descripcion[$indice2]= $dato2;
					break;
				case 'anotacion_anio': //error de php, no se puede poner el nombre en forma dinámica
					$anotacion_anio[$indice2]=$dato2;
					break;
				case 'id_anotacion': //error de php, no se puede poner el nombre en forma dinámica
					$id_anotacion[$indice2]=$dato2;
					break;
				case 'curso_descripcion': //error de php, no se puede poner el nombre en forma dinámica
					$curso_descripcion[$indice2]= $dato2;
					break;
				case 'curso_anio': //error de php, no se puede poner el nombre en forma dinámica
					$curso_anio[$indice2]=$dato2;
					break;
				case 'id_curso': //error de php, no se puede poner el nombre en forma dinámica
					$id_curso[$indice2]=$dato2;
					break;
					
			} **/
		}
	}
	else
		$valores_form[$indice]=$dato;
}

/**
for($i=0; $i<count($bombero_premios_anios); $i++){
	if($i>CANTIDAD_ANIOS_SERVICIO_INICIO and ($bombero_premios_anios[$i]['anio_premio_cia']!='' or $bombero_premios_anios[$i]['anio_premio_cuerpo']!='')){
		$cantidad_anios_servicio=$i;
	}
}
*/

if(count($valores_form['bombero_anotaciones'])>CANTIDAD_ANOTACIONES_INICIO)
	$cantidad_anotaciones=count($valores_form['bombero_anotaciones']);

if(count($valores_form['bombero_cursos'])>CANTIDAD_CURSOS_INICIO)
	$cantidad_cursos=count($valores_form['bombero_cursos']);

if(count($valores_form['bombero_cargos'])>CANTIDAD_CARGOS_INICIO)
	$cantidad_cargos=count($valores_form['bombero_cargos']);


if($sexo=='m')
	$sel_sexo_m='checked="checked"';
else if($sexo=='f')
	$sel_sexo_f='checked="checked"';

//print_r($bombero_premios_anios);

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_editar_vida_bombero.php');