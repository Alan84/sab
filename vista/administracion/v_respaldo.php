<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Respaldo') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-02T22:23:49-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>


</div>

<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">
<h1 style="text-align: center"></h1>
<h1 style="color: white; background-color: black">Respaldos</h1>
<p style="float: right;"><a href="subir_respaldo_db.php" >Subir un respaldo de la base de datos</a></p>
<ul>
	<li><a href="descargar_respaldo_db.php" >Descargar respaldo de la base de datos</a></li>
	<li><a href="descargar_respaldo_fotos.php" >Descargar respaldo de fotos</a></li>
	<li><a href="descargar_respaldo_certificados.php" >Descargar respaldo de certificados</a></li>
</ul>	
</div>
</body>
</html>