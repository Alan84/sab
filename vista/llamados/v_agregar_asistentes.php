<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Agregar asistentes') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T22:37:24-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<!-- <h1 style="text-align: center; margin: 1em"><span style="color: red">BOMBEROS</span> <br /> <span style="border: solid yellow 1px;background-color: red; color: yellow; font-size: 14pt">* QUINTA SAN BERNARDO *</span></h1> -->
<div>
<p><?php echo $_SESSION['usr'] ?> <a href="<?php echo DIRECTORIO_WEB_SISTEMA ?>/salir.php">Salir</a></p>
<?php include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/menu_admin.inc') ?>

</div>

<div style="clear: both"></div>
<div style=" margin: 0 auto; padding: 2em 10%">

<h1 style="color: white; background-color: black">Agregar llamado</h1>
	<form action="<?php echo DIRECTORIO_WEB_SISTEMA ?>/sistema/administracion/confirmar_llamado.php" method="post">
			<table>
				<tr><td>tipo llamado:</td><td><select name="cod_tipo_llamado">
				<option value=""></option>
				<?php foreach($tipos_llamado as $tipo){ ?>
					<?php $sel=($form['cod_tipo_llamado'] == $tipo['id'])?' selected="selected"':''; ?>
					<option value="<?php echo $tipo['id'] ?>" <?php echo $sel ?>><?php echo $tipo['descripcion'] ?></option>
				<?php } ?>
				</select></td>
				</tr>
			<tr><td>fecha/hora inicio:</td><td><input type="date" name="fecha_ini" value="<?php echo $form['fecha_ini']; ?>" /> <input type="time" name="hora_ini" value="<?php echo $form['hora_ini']; ?>" /> * salida del cuartel</td></tr>
			<tr><td>fecha/hora termino:</td><td><input type="date" name="fecha_fin" value="<?php echo $form['fecha_fin']; ?>" /> <input type="time" name="hora_fin" value="<?php echo $form['hora_fin']; ?>" />	* llegada al cuartel</td></tr>
			
			<tr><td>dirección:</td><td>Región:
			<select name="dir_region">
				<option value="">-- seleccionar --</option>
				<?php foreach($regiones as $id => $region){ ($id==$form['dir_region'])?$sel=' selected="selected"':$sel='';?>
					<option value="<?php echo $id ?>"<?php echo $sel; ?>><?php echo $region; ?></option>
				<?php } ?>
			</select> <br /> 
				Comuna: <input type="text" name="dir_comuna" value="<?php echo $form['dir_comuna']; ?>" /> <br /> 
				Calle: <input type="text" name="dir_calle" value="<?php echo $form['dir_calle']; ?>" /> <br /> 
				Número: <input type="text" name="dir_numero" value="<?php echo $form['dir_numero']; ?>" /><br />
				Calle2: <input type="text" name="dir_calle" value="<?php echo $form['dir_calle']; ?>" /> *calle cercana, intersección o referencial
			</td></tr>
			<tr><td>Observaciones:</td><td><textarea name="observaciones" cols="25" rows="5"><?php echo $form['observaciones']; ?></textarea></td></tr>
			<tr><td>Rut bombero a cargo:</td><td><input type="text" name="rut" value="<?php echo $form['rut']; ?>" /> <span style="font-weight: bold;">-</span> <input type="text" name="rut_dv" value="<?php echo $form['rut_dv']; ?>" maxlength="2" style="width: 2em" /> No es necesario incluirlo en la lista de <em>bomberos asistentes</em>, se entiende que el bombero a cargo asistió, por lo que este rut se agregará a la tabla de asistentes <strong>siempre</strong>.</td></tr>
			
			<tr><td style="vertical-align: top">Bomberos asitentes:</td><td>
				<ul>
				<?php foreach($bomberos as $bom){ ?>
					<li><input type="checkbox" name="rut_asistentes[<?php echo $bom['rut']; ?>]" value="1" id="rut_asiste[<?php echo $bom['rut']; ?>]" /><label for="rut_asiste[<?php echo $bom['rut']; ?>]"><span class="bombero_asiste"><?php echo $bom['rut'].'-'.$bom['rut_dv'].' '.$bom['nombre'].' '.$bom['apellido'].' '; ?></span></li></label>
				<?php } ?>
				</ul>			
			</td></td></tr>
			</table>
			<?php foreach($_POST as $name=>$dato){ ?>
				<?php if(is_array($_POST[$name])) {?>
					<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
						<?php if(is_array($_POST[$name][$name2])) {?>
							<?php foreach($_POST[$name][$name2] as $name3=>$dato3){ ?>
								<input type="hidden" name="<?php echo $name.'['.$name2.']['.$name3.']';?>" value="<?php echo $dato3 ?>" />
							<?php } ?>
						<?php } else {?>
							<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
						<?php } ?>		
					<?php } ?>
				<?php } else {?>
					<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
				<?php } ?>
			<?php } ?>
			<input type="submit" name="enviar" value="Agregar" />
	</form>
</div>
</body>
</html>