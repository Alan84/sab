<?php

class cod_grupo_sangre{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_grupos_sangre($datos_iniciales=array()) {
		$datos=array();
		$sql="select * from cod_grupo_sangre";
		
		$datos_combo=array();
		if($datos_iniciales!=array())
			$datos_combo=$datos_iniciales;
		
		foreach($this->con->query($sql) as $row)
			$datos_combo[$row['id_cod_grupo_sangre']]=$row['descripcion'];
		return $datos_combo;
	}
}