<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}

include('../../comun.inc');

include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_llamado.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_usuario.php');

$con= new PDO($cadena_con, $usuario_bd, $clave_bd);

$t_usuario = new usuario($con);
$t_llamado = new llamado($con);
$t_bombero = new bombero($con);

$errores=check_errores($t_usuario);
$borrar=array();

// $rut=$_POST['rut']; no hay rut, se elimina solo por el id del llamado




//if($cambios==array())
//	$errores[]='No se han seleccionado cambios.';
	
	

//$estados=$t_est_bombero->obtener_estados_bomberos();
//$cargos=$t_cargo->obtener_cargos();
//$cbo_bomberos=$t_bombero->obtener_combo_bomberos(array(1), array(array('','-- seleccionar --')));
//print_r($_POST['borrar']);
if ($errores==array()){
	if(isset($_POST['confirmar'])){
		foreach($_POST['ids'] as $id_llamado=>$rut)
			$t_llamado->set_borrado($id_llamado, '1'); //borrado lógico
			
			header('location: consulta_llamados.php');
	}	
	else if(isset($_POST['borrar'])){
		$i=0;
		foreach($_POST['ids'] as $id_llamado=>$rut){
			$bombero=$t_bombero->obtener_bombero($rut);
			$llamados[$i]['bombero_datos_basicos']=$bombero['nombre'].' '.$bombero['apellido'].' '.number_format($bombero['rut'], 0, ',','.').'-'.$bombero['rut_dv'];
			$llamados[$i]['id_llamado']=$id_llamado;
			$i++;
		}
		include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/llamados/v_confirmar_borrar_llamados.php');
	}
	else {
		header('location: consulta_llamados.php');
	}	
}
else{
	$enlace_volver = DIRECTORIO_WEB_SISTEMA.'/sistema/llamados/consulta_llamados.php';
	include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/vista/bomberos/v_mostrar_errores.php');
}



function check_errores($t_usuario){
	$errores=array();
	if($t_usuario->es_administrador($_SESSION['usr'])==false)
		$errores[]='Solo puede borrar un usuario administrador';
	
	return $errores;
}