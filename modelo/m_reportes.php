<?php
class m_reportes{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_total_usuarios() {
		$sql="select count(*) as total from usuario";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return false;
	}
	
	function obtener_total_bomberos() {
		$sql="select count(*) as total from bombero where borrado is null ";
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return 0;
	}
	
	function obtener_total_bomberos_morosos() {
		$sql="select count(*) as total_bomberos, 
			COUNT((select pago_mes from pago_mes pm, pago p 
					where pm.id_pago = p.id_pago and p.rut = b.rut and 
						p.rut = pm.rut and pm.pago_mes > date_sub(CURRENT_DATE(), interval 1 month) and p.borrado is null  
							order by pm.pago_mes desc LIMIT 0, 1)) as bomberos_sin_deuda from bombero b where b.borrado is null 
		
		 ";
			/*  ver sin deuda select b.rut, 
					(select 1 from pago_mes pm, pago p 
							where pm.id_pago = p.id_pago and p.rut = b.rut and 
								pago_mes > date_sub(CURRENT_DATE(), interval 1 month)
									 order by pm.pago_mes desc limit 0,1) as sin_deuda from bombero b */		
									 
			/*** sql para morosos 
			
			select b.rut, (select pm.id_pago_mes from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and pm.pago_mes < date_sub(CURRENT_DATE(), interval 1 month) LIMIT 0, 1) as deuda from bombero b
			
			***/
			
			/*****
			
			select count((select pago_mes from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and pm.pago_mes > date_sub(CURRENT_DATE(), interval 1 month) order by pm.pago_mes desc LIMIT 0, 1)) as deuda from bombero b
			
			**/ 
			
			/*************
			
			select count(*) as total_bomberos, COUNT((select pago_mes from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut and pm.pago_mes > date_sub(CURRENT_DATE(), interval 1 month) order by pm.pago_mes desc LIMIT 0, 1)) as bomberos_sin_deuda from bombero b
			
			***/
		 
		 // echo $sql;
		 //(select pago_mes from pago_mes pm, pago p where pm.id_pago = p.id_pago and p.rut = b.rut order by pm.id_pago desc, pm.id_pago_mes desc limit 0,1)
		$query=$this->con->prepare($sql);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total_bomberos']-$result[0]['bomberos_sin_deuda'];
		else 
			return false;
	}
	
	function obtener_total_llamados($rut='') {
		
		if($rut==''){
			$sql="select count(*) as total from llamado ll, bombero b where b.borrado is null and ll.borrado is null and b.rut=ll.rut_acargo ";
			$query=$this->con->prepare($sql);
			$query->execute();
		}
		else {
			$sql="select count(*) as total from llamado ll, bombero b where ll.rut_acargo=? and b.borrado is null and ll.borrado is null and b.rut=ll.rut_acargo";
			$query=$this->con->prepare($sql);
			$query->execute(array($rut));
		}
		
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return false;
	}
	
	function obtener_total_socios() {
		return 0;
	}
	
	function obtener_total_bomberos_borrados() {
		$sql="select count(*) as total from bombero where borrado=1";
		$query=$this->con->prepare($sql);
		$query->execute();
		
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return 0;
	}
	
	function obtener_total_llamados_borrados() {
		$sql="select count(*) as total from llamado where borrado=1";
		$query=$this->con->prepare($sql);
		$query->execute();
		
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return 0;
	}
	
	function obtener_total_pagos() {
		$sql="select count(*) as total from pago where borrado is null";
		$query=$this->con->prepare($sql);
		$query->execute();
		
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return 0;
	}
	
	function obtener_total_pagos_borradas() {
		$sql="select count(*) as total from pago where borrado=1";
		$query=$this->con->prepare($sql);
		$query->execute();
		
		$result=$query->fetchAll(PDO::FETCH_ASSOC);

		if(isset($result[0]))
			return $result[0]['total'];
		else 
			return 0;
	}
}
