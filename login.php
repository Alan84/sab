<?php

$errores=array();
if(trim($_POST['usr'])=='')
	$errores[]='Campo Usuario vacío';
if(trim($_POST['clave'])=='')
	$errores[]='Campo Clave vacío';



if($errores==array()){
	$login_correcto=verificar_ingreso($_POST['usr'], $_POST['clave']);
	if($login_correcto){
		session_start();
		$_SESSION['usr']=$_POST['usr'];
		header('location: sistema/');
	}
	else{
		$errores[]='Datos incorrectos';
	}
}
if($errores!=array()){
	include('error.php');
}

function verificar_ingreso($usr, $clave){
	include('comun.inc');
	$pdo= new PDO($cadena_con, $usuario_bd, $clave_bd);
	$clave=hash('sha256',$clave);
	$sql="select habilitado from usuario where usuario=? and clave=?";
	$query=$pdo->prepare($sql);
	$query->execute(array($usr, $clave));
	$result=$query->fetchAll(PDO::FETCH_ASSOC);
	
	//$result->fetchAll();
//	$numRow = $pdo->rowCount();
	if(isset($result[0]) and $result[0]['habilitado']=='1')
		return true;
	else
		return false;
}