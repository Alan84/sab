-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `anotacion`;
CREATE TABLE `anotacion` (
  `id_anotacion` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `id_tipo_anotacion` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id_anotacion`,`rut`),
  KEY `rut` (`rut`),
  KEY `id_tipo_anotacion` (`id_tipo_anotacion`),
  CONSTRAINT `anotacion_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `anotacion_ibfk_2` FOREIGN KEY (`id_tipo_anotacion`) REFERENCES `tipo_anotacion` (`id_tipo_anotacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `anotacion` (`id_anotacion`, `descripcion`, `anio`, `rut`, `id_tipo_anotacion`, `fecha`) VALUES
(1,	'todos bien gracias a ti....',	NULL,	7031254,	1,	'2019-10-10');

DROP TABLE IF EXISTS `bombero`;
CREATE TABLE `bombero` (
  `nombre` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `rut_dv` int(11) DEFAULT NULL,
  `direc_calle` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `direc_numero` int(11) DEFAULT NULL,
  `direc_comuna` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `direc_region` int(10) DEFAULT NULL,
  `fono` int(11) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `fecha_def` date DEFAULT NULL,
  `num_registro` int(11) DEFAULT NULL,
  `num_tib` int(11) DEFAULT NULL,
  `num_placa` int(11) DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `profesion` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `grup_san` char(6) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `email2` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `fono2` int(11) DEFAULT NULL,
  `apellido` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `fecha_actualiza` datetime DEFAULT NULL,
  `direc_depto` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `nombre_padre` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `nombre_madre` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `sexo` char(1) COLLATE utf8_bin DEFAULT NULL,
  `foto` tinyint(1) DEFAULT NULL,
  `id_est_civil` int(11) DEFAULT NULL,
  `id_est_bombero` int(11) NOT NULL,
  `usuario_actualiza` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `id_cod_cargo` int(11) DEFAULT NULL,
  `fecha_ini_cargo` date DEFAULT NULL,
  `comentarios` text COLLATE utf8_bin DEFAULT NULL,
  `fecha_mail_alerta` datetime DEFAULT NULL,
  `borrado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`rut`),
  KEY `id_est_civil` (`id_est_civil`),
  KEY `id_est_bombero` (`id_est_bombero`),
  KEY `usuario_actualiza` (`usuario_actualiza`),
  KEY `id_cod_cargo` (`id_cod_cargo`),
  KEY `direc_region` (`direc_region`),
  CONSTRAINT `bombero_ibfk_1` FOREIGN KEY (`id_est_civil`) REFERENCES `est_civil` (`id_est_civil`),
  CONSTRAINT `bombero_ibfk_2` FOREIGN KEY (`id_est_bombero`) REFERENCES `est_bombero` (`id_est_bombero`),
  CONSTRAINT `bombero_ibfk_3` FOREIGN KEY (`usuario_actualiza`) REFERENCES `usuario` (`usuario`),
  CONSTRAINT `bombero_ibfk_4` FOREIGN KEY (`id_cod_cargo`) REFERENCES `cod_cargo` (`id_cod_cargo`),
  CONSTRAINT `bombero_ibfk_6` FOREIGN KEY (`direc_region`) REFERENCES `cod_region` (`id_cod_region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `bombero` (`nombre`, `rut_dv`, `direc_calle`, `direc_numero`, `direc_comuna`, `direc_region`, `fono`, `fecha_ingreso`, `fecha_nac`, `fecha_def`, `num_registro`, `num_tib`, `num_placa`, `rut`, `profesion`, `grup_san`, `email`, `email2`, `fono2`, `apellido`, `fecha_actualiza`, `direc_depto`, `nombre_padre`, `nombre_madre`, `sexo`, `foto`, `id_est_civil`, `id_est_bombero`, `usuario_actualiza`, `id_cod_cargo`, `fecha_ini_cargo`, `comentarios`, `fecha_mail_alerta`, `borrado`) VALUES
('JUAN ENRIQUE',	9,	'PABLO BURCHARD',	4801,	'SAN BERNARDO',	13,	228572402,	'1980-08-05',	'1954-08-08',	NULL,	NULL,	NULL,	NULL,	7031254,	'EMPRESARIO',	'A2 RH+',	NULL,	NULL,	991657449,	'ORTEGA VALDES',	'2019-10-03 23:20:05',	NULL,	'GUILLERMO',	'EMA',	'M',	1,	1,	1,	'admin',	NULL,	'0000-00-00',	NULL,	'2019-09-17 17:18:58',	NULL),
('ALVARO ANDRES',	7,	'PABLO BURCHARD',	NULL,	'SAN BERNARDO',	13,	NULL,	'2019-10-01',	'1984-06-11',	NULL,	NULL,	NULL,	NULL,	15622288,	NULL,	NULL,	NULL,	NULL,	NULL,	'ORTEGA VELASQUEZ',	'2019-10-06 06:04:47',	NULL,	NULL,	NULL,	'M',	NULL,	NULL,	1,	'admin',	NULL,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `cargo_actual`;
CREATE TABLE `cargo_actual` (
  `id_cargo_actual` int(11) NOT NULL,
  `rut` int(11) DEFAULT 0,
  `fecha_inicio` date DEFAULT NULL,
  PRIMARY KEY (`id_cargo_actual`),
  UNIQUE KEY `id_cargo_actual_2` (`id_cargo_actual`),
  UNIQUE KEY `id_cargo_actual_4` (`id_cargo_actual`),
  UNIQUE KEY `id_cargo_actual_3` (`id_cargo_actual`,`rut`),
  KEY `rut` (`rut`),
  CONSTRAINT `cargo_actual_ibfk_1` FOREIGN KEY (`id_cargo_actual`) REFERENCES `cod_cargo` (`id_cod_cargo`),
  CONSTRAINT `cargo_actual_ibfk_2` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cargo_actual` (`id_cargo_actual`, `rut`, `fecha_inicio`) VALUES
(2,	7031254,	'2019-08-26'),
(4,	7031254,	'2019-08-25'),
(5,	7031254,	'2019-08-27');

DROP TABLE IF EXISTS `cargo_hst`;
CREATE TABLE `cargo_hst` (
  `id_cargo` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `anio` date DEFAULT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_termino` date NOT NULL,
  `id_cod_cargo` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_cargo`,`rut`),
  KEY `rut` (`rut`),
  KEY `id_cod_cargo` (`id_cod_cargo`),
  CONSTRAINT `cargo_hst_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `cargo_hst_ibfk_2` FOREIGN KEY (`id_cod_cargo`) REFERENCES `cod_cargo` (`id_cod_cargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cargo_hst` (`id_cargo`, `rut`, `anio`, `fecha_inicio`, `fecha_termino`, `id_cod_cargo`, `descripcion`) VALUES
(1,	7031254,	NULL,	'2019-08-25',	'2019-08-27',	3,	NULL),
(2,	7031254,	NULL,	'2019-08-25',	'2019-08-27',	5,	NULL);

DROP TABLE IF EXISTS `cod_anio`;
CREATE TABLE `cod_anio` (
  `cant_anios` int(11) DEFAULT NULL,
  `id_cod_anio` int(11) NOT NULL,
  PRIMARY KEY (`id_cod_anio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cod_anio` (`cant_anios`, `id_cod_anio`) VALUES
(5,	1),
(10,	2),
(15,	3),
(20,	4),
(25,	5),
(30,	6),
(35,	7),
(40,	8),
(45,	9),
(50,	10),
(55,	11),
(60,	12),
(65,	13),
(70,	14),
(75,	15),
(80,	16),
(85,	17),
(90,	18),
(95,	19),
(100,	20),
(105,	21),
(110,	22),
(115,	23),
(120,	24),
(125,	25);

DROP TABLE IF EXISTS `cod_cargo`;
CREATE TABLE `cod_cargo` (
  `id_cod_cargo` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_cod_cargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cod_cargo` (`id_cod_cargo`, `descripcion`) VALUES
(1,	'DIRECTOR'),
(2,	'CAPITAN'),
(3,	'TENIENTE'),
(4,	'TENIENTE 2do'),
(5,	'TENIENTE 3ro'),
(6,	'TESORERO'),
(7,	'CONSEJERO'),
(8,	'CONSEJERO'),
(9,	'CONSEJERO'),
(10,	'CONSEJERO');

DROP TABLE IF EXISTS `cod_region`;
CREATE TABLE `cod_region` (
  `id_cod_region` int(11) NOT NULL,
  `descripcion` varchar(25) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_cod_region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cod_region` (`id_cod_region`, `descripcion`) VALUES
(1,	'Tarapaca'),
(2,	'Antofagasta'),
(3,	'Atacama'),
(4,	'Coquimbo'),
(5,	'Valparaiso'),
(6,	'O\'Higgins'),
(7,	'Maule'),
(8,	'Biobio'),
(9,	'La araucania'),
(10,	'Los lagos'),
(11,	'Aysen'),
(12,	'Magallanes'),
(13,	'Region metropolitana'),
(14,	'Los rios'),
(15,	'Arica y parinacota');

DROP TABLE IF EXISTS `curso`;
CREATE TABLE `curso` (
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  `anio` smallint(6) DEFAULT NULL,
  `fecha` date NOT NULL,
  `id_curso` int(11) NOT NULL,
  `certificado` char(4) COLLATE utf8_bin DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `id_tipo_curso` int(11) NOT NULL,
  PRIMARY KEY (`id_curso`,`rut`),
  KEY `rut` (`rut`),
  KEY `id_tipo_curso` (`id_tipo_curso`),
  CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `curso_ibfk_2` FOREIGN KEY (`id_tipo_curso`) REFERENCES `tipo_curso` (`id_tipo_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `curso` (`descripcion`, `anio`, `fecha`, `id_curso`, `certificado`, `rut`, `id_tipo_curso`) VALUES
('todo ok',	1911,	'2019-01-01',	1,	'PNG',	7031254,	1),
('ok',	NULL,	'2019-10-06',	2,	NULL,	7031254,	2),
('okkk',	NULL,	'2019-10-06',	3,	NULL,	7031254,	2),
('KJKLNLNL',	NULL,	'2019-10-01',	4,	NULL,	7031254,	2),
('pkkjoaskd',	NULL,	'2019-10-09',	5,	NULL,	7031254,	1);

DROP TABLE IF EXISTS `est_bombero`;
CREATE TABLE `est_bombero` (
  `id_est_bombero` int(11) NOT NULL,
  `descripcion` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_est_bombero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `est_bombero` (`id_est_bombero`, `descripcion`) VALUES
(1,	'activo'),
(2,	'con licencia'),
(3,	'suspendido'),
(4,	'retirado'),
(5,	'expulsado'),
(6,	'fallecido');

DROP TABLE IF EXISTS `est_civil`;
CREATE TABLE `est_civil` (
  `id_est_civil` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_est_civil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `est_civil` (`id_est_civil`, `descripcion`) VALUES
(1,	'casado'),
(2,	'soltero');

DROP TABLE IF EXISTS `llamado`;
CREATE TABLE `llamado` (
  `id_llamado` int(11) NOT NULL,
  `cod_tipo_llamado` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `direccion` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fue_acargo` tinyint(1) DEFAULT NULL,
  `rut_acargo` int(11) NOT NULL,
  `fecha_ini` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `dir_region` int(11) DEFAULT NULL,
  `dir_comuna` varchar(50) COLLATE utf8_bin NOT NULL,
  `dir_calle` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `dir_numero` int(11) DEFAULT NULL,
  `dir_calle2` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `usuario` varchar(16) COLLATE utf8_bin NOT NULL,
  `borrado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_llamado`,`rut_acargo`),
  KEY `rut` (`rut_acargo`),
  KEY `cod_tipo_llamado` (`cod_tipo_llamado`),
  CONSTRAINT `llamado_ibfk_1` FOREIGN KEY (`rut_acargo`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `llamado_ibfk_2` FOREIGN KEY (`cod_tipo_llamado`) REFERENCES `tipo_llamado` (`cod_tipo_llamado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `llamado` (`id_llamado`, `cod_tipo_llamado`, `direccion`, `observaciones`, `fecha`, `fue_acargo`, `rut_acargo`, `fecha_ini`, `fecha_fin`, `dir_region`, `dir_comuna`, `dir_calle`, `dir_numero`, `dir_calle2`, `usuario`, `borrado`) VALUES
(3,	NULL,	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-08-21 14:31:00',	'2019-08-22 12:29:00',	13,	'SAN BERNARDO',	'camino internacional norte',	NULL,	NULL,	'admin',	NULL),
(7,	NULL,	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-08-28 11:02:00',	'2019-08-28 13:01:00',	13,	'SAN BDO',	'ines de suarez',	NULL,	NULL,	'admin',	NULL),
(8,	'10-1',	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-08-28 12:20:00',	'2021-09-30 13:19:00',	13,	'SAN BERNARDO',	'saf',	NULL,	NULL,	'admin',	NULL),
(9,	NULL,	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-08-28 12:05:00',	'2019-08-28 15:06:00',	13,	'SAN BERNARDO',	'OK',	NULL,	NULL,	'admin',	NULL),
(10,	NULL,	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-10-03 01:20:00',	'2019-10-03 01:22:00',	13,	'SAN BERNARDO',	'OK',	NULL,	NULL,	'admin',	NULL),
(11,	NULL,	NULL,	'TODO BIEN',	NULL,	NULL,	7031254,	'2019-10-03 22:22:00',	'2019-10-03 23:00:00',	13,	'SAN BERNARDO',	'OKKKKK',	NULL,	NULL,	'admin',	NULL),
(12,	'10',	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-10-03 00:27:00',	'2019-10-04 00:27:00',	13,	'SAN BERNARDO',	'ZSFDS',	NULL,	NULL,	'admin',	NULL),
(13,	NULL,	NULL,	'OKASSSSSSSSSSSSSSSSSS',	NULL,	NULL,	7031254,	'2019-10-03 22:28:00',	'2019-10-03 22:29:00',	13,	'SAN BERNARDO',	'OKASSS',	NULL,	NULL,	'admin',	NULL),
(14,	'10-3',	NULL,	'SASDFSDA',	NULL,	NULL,	7031254,	'2019-10-03 00:31:00',	'2019-10-03 00:32:00',	13,	'SAN BERNARDO',	'SSSS',	NULL,	NULL,	'admin',	NULL),
(15,	NULL,	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-10-04 12:06:00',	'2019-11-04 10:04:00',	13,	'SAN BERNARDO',	'ines de suarez',	NULL,	NULL,	'admin',	NULL),
(16,	'10-3',	NULL,	NULL,	NULL,	NULL,	7031254,	'2019-10-04 12:08:00',	'2019-10-05 12:08:00',	13,	'SAN BERNARDO',	'santa marta',	NULL,	NULL,	'admin',	NULL);

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `llamado_bomberos`;
CREATE TABLE `llamado_bomberos` (
  `id_llamado` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `comentario` text DEFAULT NULL,
  PRIMARY KEY (`id_llamado`,`rut`),
  KEY `rut` (`rut`),
  CONSTRAINT `llamado_bomberos_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `llamado_bomberos_ibfk_2` FOREIGN KEY (`id_llamado`) REFERENCES `llamado` (`id_llamado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `pago`;
CREATE TABLE `pago` (
  `id_pago` int(11) NOT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  `fecha_vence` date DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `comentario` text COLLATE utf8_bin DEFAULT NULL,
  `fecha_mail_alerta` date DEFAULT NULL,
  `rut` int(11) NOT NULL,
  `usuario` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `borrado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_pago`,`rut`),
  KEY `rut` (`rut`),
  KEY `usuario` (`usuario`),
  CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`),
  CONSTRAINT `pago_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `pago` (`id_pago`, `fecha_ingreso`, `fecha_vence`, `monto`, `comentario`, `fecha_mail_alerta`, `rut`, `usuario`, `borrado`) VALUES
(1,	'2019-09-17 05:57:35',	NULL,	9000,	'',	NULL,	7031254,	NULL,	NULL),
(1,	'2019-10-06 06:05:14',	NULL,	3000,	'',	NULL,	15622288,	'admin',	NULL),
(3,	'2019-09-22 06:27:22',	NULL,	6000,	'',	NULL,	7031254,	'admin',	NULL),
(4,	'2019-09-22 06:28:08',	NULL,	3000,	'',	NULL,	7031254,	'admin',	NULL),
(5,	'2019-09-22 06:29:50',	NULL,	3000,	'',	NULL,	7031254,	'admin',	NULL),
(6,	'2019-09-22 06:43:18',	NULL,	3000,	'',	NULL,	7031254,	'admin',	NULL),
(7,	'2019-09-22 06:45:06',	NULL,	3000,	'',	NULL,	7031254,	'admin',	NULL),
(8,	'2019-10-02 04:05:28',	NULL,	5000,	'todo bien, paga menos',	NULL,	7031254,	'admin',	NULL);

DROP TABLE IF EXISTS `pago_mes`;
CREATE TABLE `pago_mes` (
  `id_pago_mes` int(11) NOT NULL,
  `id_pago` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `pago_mes` date NOT NULL,
  PRIMARY KEY (`id_pago_mes`,`id_pago`,`rut`),
  KEY `id_pago` (`id_pago`),
  CONSTRAINT `pago_mes_ibfk_2` FOREIGN KEY (`id_pago`) REFERENCES `pago` (`id_pago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `pago_mes` (`id_pago_mes`, `id_pago`, `rut`, `pago_mes`) VALUES
(1,	1,	7031254,	'2019-01-01'),
(1,	1,	15622288,	'2019-10-01'),
(1,	3,	7031254,	'2019-04-01'),
(1,	4,	7031254,	'2019-06-01'),
(1,	5,	7031254,	'2019-07-01'),
(1,	6,	7031254,	'2019-08-01'),
(1,	7,	7031254,	'2019-09-01'),
(1,	8,	7031254,	'2019-10-01'),
(2,	1,	7031254,	'2019-02-01'),
(2,	3,	7031254,	'2019-05-01'),
(2,	8,	7031254,	'2019-11-01'),
(3,	1,	7031254,	'2019-03-01'),
(3,	8,	7031254,	'2019-12-01'),
(4,	8,	7031254,	'2020-01-01');

DROP TABLE IF EXISTS `parametro`;
CREATE TABLE `parametro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8_bin NOT NULL,
  `valor` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `comentario` varchar(200) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `parametro` (`id`, `nombre`, `valor`, `comentario`) VALUES
(1,	'comuna_por_defecto',	'SAN BERNARDO',	'Util para agregar llamado, aparecerá esta comuna por defecto, pudiendo ser cambiada en caso necesario'),
(4,	'email_copias_de_pagos',	'alvaro@quintinos.cl',	'Copia de los pagos realizados. Se usa un servidor de gmail para el envío, esta cuenta soporta máximo 100 envíos por día, por lo que no es recomendado agregar más de un correo aquí'),
(6,	'morosidad_limite',	'3',	'Permite agregar un texto extra indicando que se está cayendo en falta. 0 para desactivar'),
(3,	'premio_por_anios',	'5',	'Este valor se usa para listar los premio por años , por ejemplo un valor 5 para listar premios multiplos de 5: 5, 10, 15...'),
(2,	'region_por_defecto',	'13',	'numero de la region mostrada por defecto. util para agregar llamado'),
(5,	'valor_cuota',	'3000',	'');

DROP TABLE IF EXISTS `premio_anios`;
CREATE TABLE `premio_anios` (
  `id_premio_anios` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `anio_premio_cia` int(11) DEFAULT NULL,
  `anio_premio_cuerpo` int(11) NOT NULL,
  `id_cod_anio` int(11) DEFAULT NULL,
  `anios` int(3) NOT NULL,
  PRIMARY KEY (`id_premio_anios`,`rut`),
  KEY `rut` (`rut`),
  KEY `id_cod_anio` (`id_cod_anio`),
  CONSTRAINT `premio_anios_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `premio_anios` (`id_premio_anios`, `rut`, `anio_premio_cia`, `anio_premio_cuerpo`, `id_cod_anio`, `anios`) VALUES
(1,	7031254,	2019,	2019,	NULL,	5);

DROP TABLE IF EXISTS `tipo_anotacion`;
CREATE TABLE `tipo_anotacion` (
  `id_tipo_anotacion` int(11) NOT NULL,
  `descripcion` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_tipo_anotacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `tipo_anotacion` (`id_tipo_anotacion`, `descripcion`) VALUES
(1,	'Positiva'),
(2,	'Amonestacion'),
(3,	'Otro');

DROP TABLE IF EXISTS `tipo_curso`;
CREATE TABLE `tipo_curso` (
  `descripcion` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `id_tipo_curso` int(11) NOT NULL,
  PRIMARY KEY (`id_tipo_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `tipo_curso` (`descripcion`, `id_tipo_curso`) VALUES
('ANB',	1),
('CBSB',	2),
('INTERNO',	3),
('OTRO',	4);

DROP TABLE IF EXISTS `tipo_llamado`;
CREATE TABLE `tipo_llamado` (
  `cod_tipo_llamado` varchar(10) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_tipo_llamado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tipo_llamado` (`cod_tipo_llamado`, `descripcion`) VALUES
('10',	''),
('10-1',	''),
('10-2',	''),
('10-3',	'');

DROP TABLE IF EXISTS `tipo_usuario`;
CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tipo_usuario` (`id`, `descripcion`) VALUES
(1,	'Administrador'),
(2,	'Normal');

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `usuario` varchar(16) COLLATE utf8_bin NOT NULL,
  `clave` char(64) COLLATE utf8_bin DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `rut` int(11) DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`usuario`),
  KEY `rut` (`rut`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rut`) REFERENCES `bombero` (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `usuario` (`usuario`, `clave`, `tipo`, `rut`, `habilitado`) VALUES
('admin',	'b33cfad024e7044966dc1b8dcf29e44a40775474be3aa21089fa02e99d773bd3',	1,	NULL,	1),
('enrique',	'9b6753ec65850f2ae84aa23ee6eb107ba282f171e242822e0ed28a82ed4db5d4',	2,	7031254,	1);

-- 2019-10-06 09:08:50
