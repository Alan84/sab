<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Revisar pago') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-10-06T05:08:09-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">GUARDAR PAGO</h1>
<form method="post" action="guardar_pago.php">
	Va a ingresar un pago, por favor revise la información antes de confirmar el pago.
	<table>
		<tr>
			<td>Indice de pago</td>
			<td><?php echo $ultimo_indice+1?></td>
		</tr>
		<tr>
			<td>RUT</td>
			<td><?php echo $_POST['rut'].'-'.$bombero['rut_dv'];?></td>
		</tr>
		<tr>
			<td>Nombre</td>
			<td><?php echo $bombero['nombre'].' '.$bombero['apellido'];?></td>
		</tr>
		<tr>
			<td>Monto</td>
			<td>$<?php echo number_format($monto, 0,',','.');?></td>
		</tr>
		<tr>
			<td style="vertical-align: top">Pagando</td>
			<td>
				<ol>
					<?php foreach($fechas_pago as $fecha){ ?>
						<li><?php echo $fecha;?></li>
					<?php } ?>
				</ol>
			</td>
		</tr>
		<tr>
			<td>Fecha de ingreso</td>
			<td><?php echo date('Y-m-d H:i:s')?> *Referencial, varia al confirmar.</td>
		</tr>
		<tr>
			<td>Comentario</td>
			<td><?php echo $_POST['comentario']?></td>
		</tr>
		<tr>
			<td>Enviar notificación a</td>
			<td><?php echo $email;?></td>
		</tr>
	</table>
	<ul>
	<?php foreach($errores as $error){ ?>
		<li><?php echo $error ?></li>
	<?php } ?>
	</ul>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>
	<input type="hidden" name="monto" value="<?php echo $monto; ?>" />
	<input type="submit" name="confirmar" value="Confirmar" />
</form>
<form method="post" action="ver_pagos.php?rut=<?php echo $_POST['rut']; ?>">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<input type="submit" name="cancelar" value="Cancelar" />
</form>
</div>
</div>
</body>
</html>
