<?php
session_start();
if(!isset($_SESSION['usr']) or $_SESSION['usr']==""){
	//si no hay usuario, no hay ingreso. Se envía al login.
	header('location: ../..');
}
include('../../comun.inc');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_cod_region.php');
include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_bombero.php');
//include(DIRECTORIO_WEB.DIRECTORIO_WEB_SISTEMA.'/modelo/t_llamado.php');
$con= new PDO($cadena_con, $usuario_bd, $clave_bd);
$t_cod_region = new cod_region($con);
$t_bombero = new bombero($con);
//$t_llamado = new llamado($con);

$bomberos = $t_bombero->obtener_checkbox_bomberos(array(1));
$form['fecha_ini'] = date('Y-m-d');
$form['hora_ini'] = '';
$form['fecha_fin'] = date('Y-m-d');
$form['hora_fin'] = '';
$form['dir_region'] = '13';
$form['dir_comuna'] = '';
$form['dir_calle'] = '';
$form['dir_numero'] = '';
$form['observaciones'] = '';
$form['rut'] = '';
$form['rut_dv'] = '';

//$t_llamado->guardar_llamado($_POST['cod_tipo_llamado'], $_POST['fecha_ini'], $_POST['hora_ini'], $_POST['fecha_fin'], $_POST['hora_fin'], $_POST['dir_region'], $_POST['dir_comuna'], $_POST['dir_calle'], $_POST['dir_numero'], $_POST['dir_calle2'], $_POST['obervaciones'], $_POST['rut_acargo']);

//foreach($_POST['rut_asistentes'] as $rut){
	//$t_llamado_bomberos->guardar_bombero($rut, $id_llamado, $comentario);
//}
$errores = check_errores();

// obtenemos los tipos de anotación desde la base de datos para usarlos en el combo
//$regiones = $t_cod_region->obtener_regiones();
if($errores==array()) {
	include('../../vista/llamados/v_confirmar_llamado.php');
}
else {
	$enlace_volver = 'agregar_llamado.php';
	include('../../vista/v_mostrar_errores.php');
}	


function check_errores($errores=array()) {
	if(trim($_POST['fecha_ini'])=='' or trim($_POST['hora_ini'])=='' )
		$errores[]='Fecha de inicio requerida';
	if(trim($_POST['fecha_fin'])=='' or trim($_POST['hora_fin'])=='' ){
		$errores[]='Fecha de termino requerida';
	}
	else {
		if(strtotime($_POST['fecha_ini'].' '.$_POST['hora_ini'].':00')>=strtotime($_POST['fecha_fin'].' '.$_POST['hora_fin']).':00')
			$errores[]='La <span style="text-decoration: underline;">fecha de inicio</span> debe ser menor que <span style="text-decoration: underline;">fecha de termino</span>';
	}
		
	if(trim($_POST['dir_region'])=='' )
		$errores[]='Campo region requerido';
	if(trim($_POST['dir_comuna'])=='' )
		$errores[]='Campo comuna requerido';
	if(trim($_POST['dir_calle'])=='' )
		$errores[]='Campo calle requerido';
		
		
	if(trim($_POST['rut_acargo'])=='' )
		$errores[]='Campo bombero a cargo requerido';
	
	return $errores;
}