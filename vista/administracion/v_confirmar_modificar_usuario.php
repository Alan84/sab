<!DOCTYPE html>
<html>
<head>
<title><?php echo sprintf(TITULO_HTML, 'Restablecer bombero') ?></title>
<meta name="generator" content="Bluefish 2.2.10" >
<meta name="author" content="Álvaro Andrés Ortega Velásquez" >
<meta name="date" content="2019-09-30T20:59:35-0300" >
<meta name="copyright" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
 <link rel="stylesheet" type="text/css" href="<?php echo DIRECTORIO_WEB_SISTEMA?>/main.css">

</head>
<body>
<div style="display: table; margin: 5em auto">
<div class="tabla">
<h1 style="color: white; background-color: black">Confirmar cambios</h1>
<form method="post" action="guardar_usuario.php">
	<p>Confirme la modificación</p>
	<table>
		<tr>
			<th>nombre usuario</th><td><?php echo $uname ?></td>
		</tr>
		<tr>
			<th>tipo</th><td><?php echo $descripcion_tipo_usuario_bd; if($hay_cambio_tipo_usuario)  echo ' <strong>-> '.$descripcion_tipo_usuario.'</strong>'; ?></td>
		</tr>
		<tr>
			<th>modificación de clave</th><td><?php echo ($hay_cambio_password)?'<strong>Sí</strong>':'No'; ?></td>
		</tr>
		<tr>
			<th>habilitado</th><td><?php echo ($habilitado_db)?'Sí':'No'; if($hay_cambio_habilitado){ echo '<strong> -> '; echo ($habilitado_db)?'No</strong>':'Sí</strong>'; } ?></td>
		</tr>
		<tr>
			<th>rut</th><td><?php echo $rutcompleto ?></td>
		</tr>
		<tr>
			<th>nombre</th><td><?php echo $nombre ?></td>
		</tr>
	</table>
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])){ ?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<p><input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" /></p>
		<?php } ?>
	<?php } ?>
	
	<p style="text-align: right"><input type="submit" name="confirmar" value="Actualizar" /></p>
</form>
<form method="post" action="modificar_usuario.php?uname=<?php echo $uname; ?>">
	<?php foreach($_POST as $name=>$dato){ ?>
		<?php if(is_array($_POST[$name])) {?>
			<?php foreach($_POST[$name] as $name2=>$dato2){ ?>
				<input type="hidden" name="<?php echo $name.'['.$name2.']';?>" value="<?php echo $dato2 ?>" />
			<?php } ?>
		<?php } else {?>
			<input type="hidden" name="<?php echo $name?>" value="<?php echo $dato ?>" />
		<?php } ?>
	<?php } ?>

	<p style="text-align: right"><input type="submit" name="cancelar" value="Cancelar" /></p>
</form>
</div>
</div>
</body>
</html>
