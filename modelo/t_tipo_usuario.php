<?php

class tipo_usuario{
	var $con;
	function __construct($conexion) {
       $this->con=$conexion;
   }
	
	function obtener_tipos_usuarios() {
		$sql="select * from tipo_usuario";
		$query=$this->con->prepare($sql);
		$query->execute();
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
	
	function obtener_descripcion($tipo) {
		$sql="select descripcion from tipo_usuario where id=?";
		$query=$this->con->prepare($sql);
		$query->execute(array($tipo));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		if(isset($result[0]))
			return $result[0]['descripcion'];
		else 
			return '';
	}
	
}